import { EventEmitter, ChangeDetectorRef, AfterViewInit, ElementRef, Renderer } from "@angular/core";
import { FileItem } from "../ng2-file-uploader/index";
import { CropBoundaryComponent } from "./boundary/index";
export declare class Ng2ImageCropComponent implements AfterViewInit {
    private element;
    private renderer;
    private changeDetectionRef;
    data: {
        bound?: boolean;
        points?: any;
        url?: string;
    };
    minZoom: number;
    maxZoom: number;
    boundaryWidth: number;
    boundaryHeight: number;
    options: {
        image: {};
        viewport: {
            width: number;
            height: number;
        };
        mouseWheelZoom: boolean;
        update: Function;
    };
    private zoomValue;
    private loaded;
    fileItem: FileItem;
    cropChanged: EventEmitter<string>;
    boundaryComponent: CropBoundaryComponent;
    constructor(element: ElementRef, renderer: Renderer, changeDetectionRef: ChangeDetectorRef);
    ngAfterViewInit(): void;
    zoomDecrement(): void;
    increaseZoom(): void;
    onZoomSliderChanged(): void;
    onZoomImageChanged(scale: number): void;
    _updatePropertiesFromImage(): void;
    getResult(): string;
    static _getCanvasResult(img: any, data: any): string;
}
