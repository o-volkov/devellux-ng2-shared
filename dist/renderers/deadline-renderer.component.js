import { Component, Input } from '@angular/core';
import * as moment from 'moment';
export var DeadlineRendererComponent = (function () {
    function DeadlineRendererComponent() {
    }
    DeadlineRendererComponent.prototype.ngOnInit = function () {
        this.isFutute = moment(moment.utc(this.deadline).toDate()).diff(moment(new Date())) < 0;
    };
    ;
    DeadlineRendererComponent.prototype.setClasses = function () {
        var classes = {
            'text-color__danger': this.highlighted,
            'weight-400': this.highlighted
        };
        return classes;
    };
    DeadlineRendererComponent.decorators = [
        { type: Component, args: [{
                    selector: 'deadline-renderer',
                    template: "<span *ngIf=\"!isFutute\">+ {{deadline | toNow}}</span>\n             <span *ngIf=\"isFutute\" [ngClass] = \"setClasses()\">{{deadline | toNow}} ago</span>",
                },] },
    ];
    DeadlineRendererComponent.ctorParameters = [];
    DeadlineRendererComponent.propDecorators = {
        'deadline': [{ type: Input },],
        'highlighted': [{ type: Input },],
    };
    return DeadlineRendererComponent;
}());
//# sourceMappingURL=deadline-renderer.component.js.map