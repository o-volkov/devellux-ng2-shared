import { Directive, Input, Renderer, ElementRef, OnInit, OnDestroy } from '@angular/core';
import { PresenceService } from './presence.service';

/**
 * Навешивает класс в зависимости от присутствия пользователя на сайте.
 * Классы по умолчанию 'status__online'  и 'status__offline'.
 * Если мы сразу можем передать состояние пользователя через атрибут presenceDefault.
 *
 * ### Example
 *
 * ```html
 * <div [presence]="someUser._id"></div>
 *
 * <div [presence]="someOtherUser._id"
 *      presenceOnline="custom_online"
 *      presenceOffline="mycustom_offline"
 *      [presenceDefault]="someOtherUser.currentStatus"></div>
 * ```
 *
 */
@Directive({
  selector: '[presence]'
})
export class PresenceDirective implements OnInit, OnDestroy {
  @Input() presence;
  @Input() presenceOnline: string = 'status__online';
  @Input() presenceOffline: string = 'status__offline';
  @Input() presenceDefault: boolean = false;

  private getStateSub;
  private obsStateSub;

  constructor(private service: PresenceService, private el: ElementRef, private renderer: Renderer) {
  }


  ngOnInit() {
    this.setStatus(this.presenceDefault);
    this.getStateSub = this.service.isUserOnline(this.presence)
      .subscribe(status => this.setStatus(status));

    this.obsStateSub = this.service.userStatusChange$
      .subscribe(obj => this.updatesListener(obj));
  }

  ngOnDestroy() {
    this.getStateSub.unsubscribe();
    this.obsStateSub.unsubscribe();
  }

  setStatus(status) {
    this.renderer.setElementClass(this.el.nativeElement, this.presenceOnline, status);
    this.renderer.setElementClass(this.el.nativeElement, this.presenceOffline, !status);
  }


  private updatesListener(obj: {id: string, status: boolean}|any) {
    if (this.presence === obj.id) {
      this.setStatus(obj.status);
    }
  }

}
