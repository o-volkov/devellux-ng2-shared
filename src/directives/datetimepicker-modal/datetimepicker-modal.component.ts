import {Component, HostListener, forwardRef} from "@angular/core";
import {NG_VALUE_ACCESSOR, ControlValueAccessor} from "@angular/forms";
import * as moment from "moment";
import {DeadlineDialogComponent} from "./deadline.dialog";
import {CustomNgInput} from "../../components/custom_inputs/custom-ng2-input";
import {DialogService} from "../../modules/dialog-module/dialog.module";


/* tslint:disable */
const noop = (...args) => {
};


const CUSTOM_INPUT_CONTROL_VALUE_ACCESSOR = {provide: NG_VALUE_ACCESSOR, useExisting: forwardRef(() => DatetimePickerModalComponent), multi: true};
/* tslint:enable */


@Component({
  selector: 'datetimepicker-modal',
  template: `<input type="text" class="form-input datepicker-required" placeholder="Deadline" [(ngModel)]="valueView" readonly="readonly" />`,
  styles: [`


    /*:host.ng-invalid .datepicker-required {
      border-color: #ef4773 !important;
    }
    
    :host.ng-pristine.ng-invalid .datepicker-required {
      border-color: #cad3d1 !important;
    }
    
    :host.ng-dirty.ng-invalid .datepicker-required {
      border-color: #ef4773 !important;
    }
    
    :host.ng-dirty.ng-valid .datepicker-required {
      border-color: #04be5b !important;
    }
    
    .datetimepicker-modal.ng-untouched.ng-pristine.ng-invalid .form-input.datepicker-required.ng-untouched.ng-pristine.ng-valid {
      border-color: #ef4773 !important;
    }*/
    
    
  `],
  providers: [CUSTOM_INPUT_CONTROL_VALUE_ACCESSOR],
})
export class DatetimePickerModalComponent extends  CustomNgInput implements ControlValueAccessor {


  constructor(private modal:DialogService) {
    super();
  }

  valueView: string = '';
  updateViewValue(v){
    if (v) {
      this.valueView = moment(v).format('dddd, DD MMM, h:mm a');
    }
  }

  @HostListener('mousedown')
  onDeadlineClick() {
    this.touched();
    this.modal.open(DeadlineDialogComponent, {deadline: this.value})
      .then((deadline?:any) => {
          if (deadline) {
            this.value = moment(moment.utc(deadline)).format();
          }
        })
      .catch(err => {
      });
  }
}
