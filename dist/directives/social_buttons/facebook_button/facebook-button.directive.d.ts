import { EventEmitter } from "@angular/core";
import { FacebookService } from "./facebook.service";
export declare class FacebookButtonDirective {
    private service;
    token: EventEmitter<any>;
    constructor(service: FacebookService);
    getFacebookToken(): void;
}
