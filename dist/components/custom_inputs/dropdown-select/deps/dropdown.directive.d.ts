import { OnInit, OnDestroy, EventEmitter, ElementRef } from '@angular/core';
export declare class DropdownDirective implements OnInit, OnDestroy {
    isOpen: boolean;
    autoClose: string;
    keyboardNav: boolean;
    appendToBody: boolean;
    onToggle: EventEmitter<any>;
    isOpenChange: EventEmitter<any>;
    addClass: boolean;
    selectedOption: number;
    menuEl: ElementRef;
    toggleEl: ElementRef;
    el: ElementRef;
    private iisOpen;
    constructor(el: ElementRef);
    ngOnInit(): void;
    ngOnDestroy(): void;
    dropDownMenu: {
        el: ElementRef;
    };
    dropDownToggle: {
        el: ElementRef;
    };
    toggle(open?: boolean): boolean;
    focusDropdownEntry(keyCode: number): void;
    focusToggleElement(): void;
}
