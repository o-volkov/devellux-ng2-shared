import { Pipe } from '@angular/core';
import { EssayConstantsService } from '../services/essay-constants.service';
export var EssayConstantsPipe = (function () {
    function EssayConstantsPipe(constants) {
        var _this = this;
        this.constants = constants;
        this.constants.getConstants().subscribe(function (data) { return _this.data = data; });
    }
    EssayConstantsPipe.prototype.transform = function (value, type) {
        if (type && this.data && this.data[type]) {
            var essayConst = this.data[type].filter(function (item) { return item.value == value; }).pop();
            if (essayConst) {
                return essayConst.description;
            }
        }
    };
    EssayConstantsPipe.decorators = [
        { type: Pipe, args: [{
                    name: 'essayConstants',
                    pure: false,
                },] },
    ];
    EssayConstantsPipe.ctorParameters = [
        { type: EssayConstantsService, },
    ];
    return EssayConstantsPipe;
}());
//# sourceMappingURL=essay-constants.pipe.js.map