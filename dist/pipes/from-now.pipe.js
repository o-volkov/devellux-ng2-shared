import { Pipe } from '@angular/core';
import * as moment from 'moment';
export var FromNowPipe = (function () {
    function FromNowPipe() {
    }
    FromNowPipe.prototype.transform = function (date) {
        return moment(moment.utc(date).toDate()).fromNow();
    };
    FromNowPipe.decorators = [
        { type: Pipe, args: [{ name: 'fromNow' },] },
    ];
    FromNowPipe.ctorParameters = [];
    return FromNowPipe;
}());
//# sourceMappingURL=from-now.pipe.js.map