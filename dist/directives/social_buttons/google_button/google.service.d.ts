import { OpaqueToken } from '@angular/core';
export declare let GOOGLE_APP_ID: OpaqueToken;
export declare class GoogleService {
    private appId;
    private googleAccessToken;
    private googleAuthInstance;
    constructor(appId: string);
    setGoogleToken(token: any): void;
    getToken(): Promise<any>;
}
