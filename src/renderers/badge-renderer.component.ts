import { Component, Input } from '@angular/core';

/*
* По умолчанию показываем бейж только если есть новые  (badgeMode='new')
*
* <badge-renderer [badge]="{total:0, new:0}"></badge-renderer>
* <badge-renderer [badge]="{total:0, new:0}" badgeMode="total"></badge-renderer>
* */
@Component({
  selector: 'badge-renderer',
  template: `
  <span *ngIf="badgeMode === 'new'">
    <span class="badge new" *ngIf="badge?.new">{{badge.new}}</span>
  </span>

  <span *ngIf="badgeMode === 'total'">
    <span class="badge" [class.new]="badge.new" *ngIf="badge?.total">
      <span *ngIf="!badge.new">{{badge.total}}</span>
      <span *ngIf="badge.new>0">+{{badge.new}}</span>
    </span>
  </span>`,
})
export class BadgeRendererComponent {
  @Input() badge = {total: 0, new: 0};
  @Input() badgeMode: 'total'|'new' = 'new';
}
