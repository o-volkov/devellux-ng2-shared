export declare class TransactionStateRendererComponent {
    states: {
        new: {
            color: string;
            text: string;
        };
        closed: {
            color: string;
            text: string;
        };
        finished: {
            color: string;
            text: string;
        };
        success: {
            color: string;
            text: string;
        };
        pending: {
            color: string;
            text: string;
        };
        in_progress: {
            color: string;
            text: string;
        };
        progress: {
            color: string;
            text: string;
        };
        declined: {
            color: string;
            text: string;
        };
        canceled: {
            color: string;
            text: string;
        };
    };
    constructor();
    state: string;
}
