import {ModuleWithProviders, NgModule} from "@angular/core";
import {EssayConstantsService} from "./services/essay-constants.service";
import {EssayConstantsPipe} from "./pipes/essay-constants.pipe";

@NgModule({
  declarations: [
    EssayConstantsPipe,
  ],
  exports: [
    EssayConstantsPipe,
  ]
})
export class DevelluxEssayModule {
  static forRoot(): ModuleWithProviders {
    return {
      ngModule: DevelluxEssayModule,
      providers: [EssayConstantsService]
    };
  }

}


export  * from "./pipes/essay-constants.pipe";
export  * from "./services/essay-constants.service";
