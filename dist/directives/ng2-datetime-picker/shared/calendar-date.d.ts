export declare const MIN_YEAR: number;
export declare const MAX_YEAR: number;
export declare class CalendarDate {
    year: number;
    month: number;
    day: number;
    weekDay: number;
    _hours: number;
    _minutes: number;
    seconds: number;
    ampm: string;
    disabled: boolean;
    hours: number;
    minutes: number;
    constructor(year?: number, month?: number, day?: number, weekDay?: number, _hours?: number, _minutes?: number, seconds?: number, ampm?: string, disabled?: boolean);
    static fromDate(date: Date): CalendarDate;
    equalDate(cdt: CalendarDate): boolean;
    clone(): CalendarDate;
    toDate(): Date;
    readonly lastWeekDayOfMonth: number;
    readonly lastDayOfMonth: number;
    readonly firstWeekDay: number;
    readonly now: boolean;
    nextHour(): void;
    prevHour(): void;
    nextMinute(): void;
    prevMinute(): void;
}
