import {Component, Input, Output, EventEmitter, AfterViewInit, ViewEncapsulation, forwardRef} from "@angular/core";
import {ControlValueAccessor, NG_VALUE_ACCESSOR} from "@angular/forms";
import * as moment from "moment";
import {CalendarDate, MIN_YEAR, MAX_YEAR} from "./shared/calendar-date";
import {CustomNgInput} from "../../components/custom_inputs/custom-ng2-input";


export const DATETIME_PICKER_VALUE_ACCESSOR:any = {
  provide: NG_VALUE_ACCESSOR,
  useExisting: forwardRef(() => DatetimePickerComponent),
  multi: true
};

@Component({
  selector: 'ng2-datetime-picker',
  template: `<div>
<div class="ng-datetimepicker" *ngIf="selectedDate">
  <div class="ng-datepicker" *ngIf="state === 'date'">
    <div class="controls">
      <div class="left" (click)="prevMonth()"></div>
          <span class="date">
            {{ dateTitle }}
          </span>
      <div class="right" (click)="nextMonth()"></div>
    </div>
    <div class="day-names">
          <span *ngFor="let cdn of calendarDayNames">
            <span>{{ cdn }}</span>
          </span>
    </div>
    <div class="calendar">
      <div class="calendar-container">
        <div class="day-container" *ngFor="let cd of calendarDays">
            <button type="button" class="day"
              [disabled]="cd.disabled"
              [class.disabled]="!isCurrentMonth(cd)"
              [class.now]="cd.now"
              [class.deadline]="isSelectedDay(cd)"
              (click)="selectValue(cd)">
              {{ cd.day }}
            </button>
        </div>
      </div>
    </div>
    <button type="button" class="to-date" (click)="state = 'time'">
      {{ timeTitle }}
    </button>
  </div>
  <div class="ng-timepicker" *ngIf="state === 'time'">
    <div class="controls">
      <button type="button" class="date" (click)="state = 'date'">
        {{ datetimeTitle }}
      </button>
    </div>
    <div class="time">
      <div class="time-container">
        <div class="hours">
          <button type="button" class="prev-time" (click)="nextHour()"></button>
          <input min="1" max="12" maxlength="2" minlength="1" type="number" [(ngModel)]="selectedDate.hours" (ngModelChange)="onTimeChanged()" />
          <button type="button" class="next-time" (click)="prevHour()"></button>
        </div>

        <div class="space">:</div>

        <div class="minutes">
          <button type="button" class="prev-time" (click)="nextMinute()"></button>
          <input min="0" max="59" maxlength="2" minlength="1" type="number" [(ngModel)]="selectedDate.minutes" (ngModelChange)="onTimeChanged()" />
          <button type="button" class="next-time" (click)="prevMinute()"></button>
        </div>

        <div class="space"></div>

        <div *ngIf="selectedDate.ampm == 'AM'" class="am-pm">
          <label for="ampm_am">AM</label>
          <input id="ampm_am" type="checkbox" [(ngModel)]="selectedDate.ampm"
                 [value]="AM" (click)="changeAmpm()" />
        </div>
        <div *ngIf="selectedDate.ampm == 'PM'" class="am-pm">
          <label for="ampm_pm">PM</label>
          <input id="ampm_pm" type="checkbox" [(ngModel)]="selectedDate.ampm"
                 [value]="PM" (click)="changeAmpm()" />
        </div>
      </div>
    </div>
  </div>
</div>
</div>
`,
  styles: [`.ng-datetimepicker {
	border-top-left-radius: 5px;
	border-top-right-radius: 5px;
	overflow: hidden;

	color: #fff;
	font-size: 15px;
	font-weight: 400;
	line-height: 18px;
	background: #5c6d85;
}

.ng-datepicker,
.ng-timepicker {
	border: none;
	outline: none;
	width: 320px;
	height: 410px;
}

.controls {
	display: flex;
	align-items: stretch;

	height: 55px;
	border-bottom: 1px solid rgba(255, 255, 255, 0.3);
}

.controls .date,
.controls .left,
.controls .right {
	display: flex;
	flex: 1;
	width: 56px;
	transition: background-color 0.2s;

	background-position: center;
	cursor: pointer;
}

.controls .date:hover,
.controls .left:hover,
.controls .right:hover {
	background-color: #4b5c74;
}



.controls .left {
	background-image: url('data:image/svg+xml;utf8,<svg width="18px" height="15px" viewBox="19 21 18 15" version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink"><path d="M23.4214354,29.0042976 L27.6947341,33.2732551 C28.0900642,33.6684919 28.0900642,34.3083356 27.6947341,34.7035724 C27.2994039,35.0988092 26.6587425,35.0988092 26.2634123,34.7035724 L20.2947941,28.7410286 C20.0921291,28.5384114 19.9947965,28.2718099 20.0001298,28.0058748 C19.9947965,27.7386068 20.0921291,27.4713387 20.2947941,27.2687215 L26.2634123,21.3061777 C26.6587425,20.9116074 27.2994039,20.9116074 27.6947341,21.3061777 C28.0900642,21.7014145 28.0900642,22.3419248 27.6947341,22.7364951 L23.4214354,27.006119 L35.0000081,27.006119 C35.5526703,27.006119 36,27.4533431 36,28.0052083 C36,28.556407 35.5526703,29.0042976 35.0000081,29.0042976 L23.4214354,29.0042976 Z" id="Arrow-Left-Icon" stroke="none" fill="#FFFFFF" fill-rule="evenodd"></path></svg>');
	background-repeat: no-repeat;

}

.controls .right {
	background-image: url('data:image/svg+xml;utf8,<svg width="18px" height="15px" viewBox="283 21 18 15" version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink"><path d="M284.999914,29.0042976 L296.578487,29.0042976 L292.305188,33.2732551 C291.909858,33.6684919 291.909858,34.3083356 292.305188,34.7035724 C292.700518,35.0988092 293.34118,35.0988092 293.73651,34.7035724 L299.705128,28.7410286 C299.907793,28.5384114 300.005126,28.2718099 299.999792,28.0058748 L299.999792,28.0052083 L299.999792,28.0045418 C300.005126,27.7386068 299.907793,27.4713387 299.705128,27.2687215 L293.73651,21.3061777 C293.34118,20.9116074 292.700518,20.9116074 292.305188,21.3061777 C291.909858,21.7014145 291.909858,22.3419248 292.305188,22.7364951 L296.578487,27.006119 L284.999914,27.006119 C284.447252,27.006119 283.999922,27.4533431 283.999922,28.0052083 C283.999922,28.556407 284.447252,29.0042976 284.999914,29.0042976 L284.999914,29.0042976 L284.999914,29.0042976 Z" id="Arrow-Right-Icon" stroke="none" fill="#FFFFFF" fill-rule="evenodd"></path></svg>');
	background-repeat: no-repeat;
}

.controls .date {
	display: block;
	flex: 2;
	padding: 10px 0;


	background: transparent;
	color: #fff;
	border: none;
	outline: none;

	font-size: 20px;
	font-family: "Roboto Condensed", sans-serif;
	line-height: 35px;
	text-align: center;
	text-transform: uppercase;

}

.day-names {
	display: flex;
	justify-content: space-between;
	padding: 5px 25px;
	border-bottom: 1px solid rgba(255, 255, 255, 0.3);
}

.day-names span {
	display: block;
	width: 26px;
}

.day-names span span {
	color: #fff;
	cursor: default;
	font-size: 12px;
	text-transform: uppercase;
}

.calendar{
	min-height: 240px;
	margin: 10px 20px;
}

.calendar-container {
	display: flex;
	flex-wrap: wrap;
	align-items: flex-start;

}

.day-container {
	box-sizing: border-box;
	display: flex;
	justify-content: center;
	align-self: flex-start;
	align-items: center;

	width: 40px;
	height: 40px;
	cursor: pointer;
}

.day {
	display: block;
	width: 34px;
	height: 34px;
	padding: 0;

	border: 1px solid transparent;
	border-radius: 50%;
	outline: none;
	font-family: 'Roboto', sans-serif;
	text-align: center;
	transition: background-color 0.2s;
	vertical-align: middle;

	color: #fff;
	background: none;
}

.day:enabled:hover {
	background-color: #54657e;
}

.day.now {
	border-color: #fff;
}

.day.deadline,
.day.deadline:hover {
	border-color: #04be5b;
	background-color: #04be5b;
}

.day.disabled,
.day:disabled {
	opacity: 0.3;
}

.time {
	min-height: 355px;
	padding: 118px 60px;
	border-bottom: 1px solid rgba(255, 255, 255, 0.3);
}

.time-container {
	display: flex;
	justify-content: space-between;
	align-items: stretch;
}

.time-container .space {
	width: 20px;
	text-align: center;
	line-height: 115px;
	font-size: 20px;
	font-weight: 500;
	color: #fff;
}

.time-container input {
	width: 100%;
	padding: 10px;

	border-radius: 5px;
	border: 1px solid #cad3df;
	color: #8493a8;
	font-size: 20px;
	font-weight: 500;
	font-family: Roboto, sans-serif;
	line-height: 24px;
	text-align: center;
	-webkit-appearance: none;
}

.time-container input[type='number'] {
	-moz-appearance:textfield;
}

.time-container input::-webkit-outer-spin-button,
.time-container input::-webkit-inner-spin-button {
	-webkit-appearance: none;
}

.time-container .hours,
.time-container .minutes {
	display: flex;
	flex-direction: column;
	justify-content: space-between;
	align-items: stretch;

	width: 60px;
}

.prev-time,
.next-time {
	display: block;
	height: 22px;
	width: 100%;

	background: transparent;
	border: none;
	outline: none;

	background-position: center;
	background-size: 28px 16px;
	background-repeat: no-repeat;
}

.prev-time {
	background-image: url(data:image/svg+xml;base64,PD94bWwgdmVyc2lvbj0iMS4wIiBzdGFuZGFsb25lPSJubyI/PjwhRE9DVFlQRSBzdmcgUFVCTElDICItLy9XM0MvL0RURCBTVkcgMS4xLy9FTiIgImh0dHA6Ly93d3cudzMub3JnL0dyYXBoaWNzL1NWRy8xLjEvRFREL3N2ZzExLmR0ZCI+PHN2ZyB3aWR0aD0iMTAwJSIgaGVpZ2h0PSIxMDAlIiB2aWV3Qm94PSIwIDAgMjggMTYiIHZlcnNpb249IjEuMSIgeG1sbnM9Imh0dHA6Ly93d3cudzMub3JnLzIwMDAvc3ZnIiB4bWxuczp4bGluaz0iaHR0cDovL3d3dy53My5vcmcvMTk5OS94bGluayIgeG1sOnNwYWNlPSJwcmVzZXJ2ZSIgc3R5bGU9ImZpbGwtcnVsZTpldmVub2RkO2NsaXAtcnVsZTpldmVub2RkO3N0cm9rZS1saW5lam9pbjpyb3VuZDtzdHJva2UtbWl0ZXJsaW1pdDoxLjQxNDIxOyI+PGcgaWQ9IkxheWVyMSI+PGNsaXBQYXRoIGlkPSJfY2xpcDEiPjxwYXRoIGQ9Ik0yNC41NDUsMTUuNDA2bC0xMC41NDUsLTEwLjU1OWwtMTAuNTQ0LDEwLjU1OWMtMC43OTEsMC43OTIgLTIuMDczLDAuNzkyIC0yLjg2MywwYy0wLjc5MSwtMC43OTIgLTAuNzkxLC0yLjA3NSAwLC0yLjg2OGwxMS45MzQsLTExLjk0OWMwLjQwNiwtMC40MDYgMC45NDEsLTAuNiAxLjQ3MywtMC41ODhjMC41MzIsLTAuMDEyIDEuMDY3LDAuMTgyIDEuNDc0LDAuNTg4bDExLjkzNCwxMS45NDljMC43ODksMC43OTMgMC43ODksMi4wNzYgMCwyLjg2OGMtMC43OTEsMC43OTIgLTIuMDczLDAuNzkyIC0yLjg2MywwWiIvPjwvY2xpcFBhdGg+PGcgY2xpcC1wYXRoPSJ1cmwoI19jbGlwMSkiPjxyZWN0IHg9IjAiIHk9IjAiIHdpZHRoPSIyOCIgaGVpZ2h0PSIxNiIgc3R5bGU9ImZpbGw6I2VjZWZmNDtmaWxsLXJ1bGU6bm9uemVybzsiLz48L2c+PC9nPjwvc3ZnPg==);
}

.next-time {
	background-image: url(data:image/svg+xml;base64,PD94bWwgdmVyc2lvbj0iMS4wIiBzdGFuZGFsb25lPSJubyI/PjwhRE9DVFlQRSBzdmcgUFVCTElDICItLy9XM0MvL0RURCBTVkcgMS4xLy9FTiIgImh0dHA6Ly93d3cudzMub3JnL0dyYXBoaWNzL1NWRy8xLjEvRFREL3N2ZzExLmR0ZCI+PHN2ZyB3aWR0aD0iMTAwJSIgaGVpZ2h0PSIxMDAlIiB2aWV3Qm94PSIwIDAgMjggMTYiIHZlcnNpb249IjEuMSIgeG1sbnM9Imh0dHA6Ly93d3cudzMub3JnLzIwMDAvc3ZnIiB4bWxuczp4bGluaz0iaHR0cDovL3d3dy53My5vcmcvMTk5OS94bGluayIgeG1sOnNwYWNlPSJwcmVzZXJ2ZSIgc3R5bGU9ImZpbGwtcnVsZTpldmVub2RkO2NsaXAtcnVsZTpldmVub2RkO3N0cm9rZS1saW5lam9pbjpyb3VuZDtzdHJva2UtbWl0ZXJsaW1pdDoxLjQxNDIxOyI+PGcgaWQ9IkxheWVyMSI+PGNsaXBQYXRoIGlkPSJfY2xpcDEiPjxwYXRoIGQ9Ik0zLjQ1OCwwLjU4OWwxMC41NDEsMTAuNTY0bDEwLjU0OCwtMTAuNTU1YzAuNzkxLC0wLjc5MSAyLjA3MywtMC43OTEgMi44NjMsMC4wMDFjMC43OSwwLjc5MyAwLjc5LDIuMDc2IC0wLjAwMSwyLjg2OWwtMTEuOTM5LDExLjk0M2MtMC40MDYsMC40MDYgLTAuOTQxLDAuNjAxIC0xLjQ3MywwLjU4OGMtMC41MzIsMC4wMTIgLTEuMDY3LC0wLjE4MyAtMS40NzQsLTAuNTg5bC0xMS45MjksLTExLjk1M2MtMC43ODksLTAuNzk0IC0wLjc4OSwtMi4wNzYgMC4wMDEsLTIuODY5YzAuNzkyLC0wLjc5MSAyLjA3NCwtMC43OTEgMi44NjMsMC4wMDFaIi8+PC9jbGlwUGF0aD48ZyBjbGlwLXBhdGg9InVybCgjX2NsaXAxKSI+PHBhdGggZD0iTTI4LjAwMywwLjAwNmwtMjgsLTAuMDEybC0wLjAwNiwxNmwyOCwwLjAxMmwwLjAwNiwtMTZaIiBzdHlsZT0iZmlsbDojZWNlZmY0O2ZpbGwtcnVsZTpub256ZXJvOyIvPjwvZz48L2c+PC9zdmc+);
}

.am-pm {
	font-size: 20px;
	line-height: 115px;
}

.am-pm label {
	cursor: pointer;
}

.am-pm input {
	display: none;
}

.to-date {
	display: block;
	width: 100%;
	height: 66px;
	padding: 0;

	background: transparent;
	border-top: 1px solid rgba(255, 255, 255, 0.3);
	border-right: none;
	border-left: none;
	border-bottom: 1px solid rgba(255, 255, 255, 0.3);
	outline: none;

	cursor: pointer;
	font-family: "Roboto Condensed", sans-serif;
	font-size: 20px;
	line-height: 66px;
	text-align: center;
	transition: background-color 0.2s;


}

.to-date:hover {
	background-color: #4a5c75;
}

.after-datepicker {
	display: flex;
	justify-content: space-between;
	line-height: 60px;
	width: 100%;

	border-bottom-right-radius: 5px;
	border-bottom-left-radius: 5px;
	overflow: hidden;

}

.after-datepicker button {
	flex: 1;

	background: #5c6d85;
	border: none;
	outline: none;
	color: #fff;

	font-weight: 500;
	font-size: 15px;
	font-family: Roboto, sans-serif;
	line-height: 60px;
	text-transform: uppercase;
	text-align: center;
	transition: background-color 0.2s;
}

.after-datepicker button:not(:last-child) {
	border-right: 1px solid rgba(255, 255, 255, 0.3);
}

.after-datepicker button:hover {
	background-color: #4a5c75;
}
`],
  providers: [DATETIME_PICKER_VALUE_ACCESSOR],
  encapsulation: ViewEncapsulation.None
})
export class DatetimePickerComponent extends CustomNgInput implements ControlValueAccessor, AfterViewInit {
  private state:string;

  private calendarDays:CalendarDate[];
  public calendarDayNames:string[] = moment.weekdaysShort();

  private calendarView:CalendarDate;

  private selectedDate:CalendarDate;

  private openedDate:Date;

  private minDate:Date;
  private maxDate:Date;
  @Input() min:string;
  @Input() max:string;

  @Output() changed:EventEmitter<Date> = new EventEmitter<Date>();

  constructor(){
    super();
  }

  ngAfterViewInit() {
    setTimeout(() => {
      if (this.min == 'now') {
        this.minDate = new Date();
      } else if (this.min) {
        this.minDate = new Date(this.min);
      }

      if (this.max == 'now') {
        this.maxDate = new Date();
      } else if (this.max) {
        this.maxDate = new Date(this.max);
      }


      if (this.value) {
        this.openedDate = this.value;
      } else {
        this.openedDate = new Date();
      }

      this.selectedDate = CalendarDate.fromDate(this.openedDate);

      this.generateCalendar(this.selectedDate);

      this.state = 'date';
    });
  }

  onTimeChanged() {
    this.onValueChanged(this.selectedDate);
  }

  nextHour() {
    this.selectedDate.nextHour();
    this.onTimeChanged();
  }

  prevHour() {
    this.selectedDate.prevHour();
    this.onTimeChanged();
  }

  nextMinute() {
    this.selectedDate.nextMinute();
    this.onTimeChanged();
  }

  prevMinute() {
    this.selectedDate.prevMinute();
    this.onTimeChanged();
  }

  changeAmpm() {
    this.selectedDate.ampm = (this.selectedDate.ampm == 'AM' ? 'PM' : 'AM');
    this.onTimeChanged();
  }


  public prevYear():void {
    if (this.selectedDate.year <= MIN_YEAR) {
      return;
    }

    let cdt = this.selectedDate.clone();
    cdt.year = cdt.year - 1;
    this.generateCalendar(cdt);
  }

  public nextYear():void {
    if (this.selectedDate.year >= MAX_YEAR) {
      return;
    }

    let cdt = this.selectedDate.clone();
    cdt.year = cdt.year + 1;

    this.generateCalendar(cdt);
  }

  public prevMonth():void {
    let cdt = this.calendarView.clone();

    if (cdt.month > 0) {
      cdt.month -= 1;
    } else {
      cdt.month = 11;
      cdt.year -= 1;
    }

    this.generateCalendar(cdt);
  }

  public nextMonth():void {
    let cdt = this.calendarView.clone();

    if (cdt.month < 11) {
      cdt.month += 1;
    } else {
      cdt.month = 0;
      cdt.year += 1;
    }

    this.generateCalendar(cdt);
  }

  public onValueChanged(cd:CalendarDate) {
    let dt = cd.toDate();
    this.value = dt;
    this.changed.emit(dt);
  }

  public selectValue(cd:CalendarDate):void {
    cd.hours = this.selectedDate.hours;
    cd.minutes = this.selectedDate.minutes;
    cd.seconds = this.selectedDate.seconds;
    cd.ampm = this.selectedDate.ampm;
    this.selectedDate = cd;
    this.onValueChanged(cd);

    this.generateCalendar(cd);
  }

  get dateTitle():string {
    return moment(this.calendarView.toDate()).format('MMMM YYYY');
  }

  get timeTitle():string {
    return moment(this.selectedDate.toDate()).format('h:mm A');
  }

  get datetimeTitle():string {
    return moment(this.selectedDate.toDate()).format('dddd, MMM D, YYYY');
  }

  private generateCalendar(calendarDate:CalendarDate):void {
    this.calendarView = calendarDate;
    let prevMonth:Date = moment(calendarDate.toDate()).subtract(1, 'month')
      .endOf('month').toDate();
    let nextMonth:Date = moment(calendarDate.toDate()).add(1, 'month')
      .startOf('month').toDate();
    let n = 1;

    this.calendarDays = [];

    let firstWeekDay = calendarDate.firstWeekDay;

    if (firstWeekDay !== 1) {
      n -= (firstWeekDay + 6) % 7;
    }

    let prevDays = [];
    for (let i = n; i <= calendarDate.lastDayOfMonth; i += 1) {
      if (i > 0) {
        let cd = calendarDate.clone();
        cd.day = i;

        if (this.minDate && cd.toDate() < this.minDate) {
          cd.disabled = true;
        } else if (this.maxDate && cd.toDate() > this.maxDate) {
          cd.disabled = true;
        }

        this.calendarDays.push(cd);
      } else {
        let cd = CalendarDate.fromDate(prevMonth);
        if (this.minDate && cd.toDate() < this.minDate) {
          cd.disabled = true;
        } else if (this.maxDate && cd.toDate() > this.maxDate) {
          cd.disabled = true;
        }
        prevDays.push(cd);
        prevMonth.setDate(prevMonth.getDate() - 1);
      }
    }

    prevDays = prevDays.reverse();
    this.calendarDays.unshift(...prevDays);

    if (calendarDate.lastWeekDayOfMonth > 0) {
      let nextDays = [];
      for (let i = calendarDate.lastWeekDayOfMonth; i < 7; i += 1) {
        let cd = CalendarDate.fromDate(nextMonth);
        if (this.minDate && cd.toDate() < this.minDate) {
          cd.disabled = true;
        } else if (this.maxDate && cd.toDate() > this.maxDate) {
          cd.disabled = true;
        }
        nextDays.push(cd);
        nextMonth.setDate(nextMonth.getDate() + 1);
      }

      this.calendarDays.push(...nextDays);
    }
  }

  public isCurrentMonth(cd:CalendarDate):boolean {
    return cd.month === this.calendarView.month;
  }

  public isSelectedDay(cd:CalendarDate):boolean {
    return this.selectedDate.equalDate(cd);
  }

  updateViewValue(value) {
    this.openedDate = value;
  }
}
