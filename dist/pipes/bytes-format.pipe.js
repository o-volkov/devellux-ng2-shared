import { Pipe } from '@angular/core';
export var BytesFormatPipe = (function () {
    function BytesFormatPipe() {
    }
    BytesFormatPipe.prototype.transform = function (bytes, precision) {
        if (precision === void 0) { precision = 1; }
        if (isNaN(parseFloat(bytes)) || !isFinite(bytes)) {
            return '-';
        }
        var units = ['bytes', 'kB', 'MB', 'GB', 'TB', 'PB'], number = Math.floor(Math.log(bytes) / Math.log(1024));
        return (bytes / Math.pow(1024, Math.floor(number))).toFixed(precision) + ' ' + units[number];
    };
    BytesFormatPipe.decorators = [
        { type: Pipe, args: [{
                    name: 'bytesFormat',
                    pure: false
                },] },
    ];
    BytesFormatPipe.ctorParameters = [];
    return BytesFormatPipe;
}());
//# sourceMappingURL=bytes-format.pipe.js.map