import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'cutPipe'
})
export class CutPipe implements PipeTransform {

  transform(value: any, arg1: number): any {

    if(arg1 &&  arg1 > 3 && value.length  > arg1) {
      return value = value.slice(0, arg1 - 3) + '...'
    };

    return value;
  }

}
