import {Component} from "@angular/core";
import * as moment from "moment";
import {FormControl} from "@angular/forms";
import {ModalComponent, DialogRef} from "../../modules/dialog-module/components/angular2-modal/index";


@Component({
  selector: 'dialog-custom',
  template: `
    <ng2-datetime-picker *ngIf="deadline" [formControl]="deadlineControl" [min]="min"></ng2-datetime-picker>
    <div class="after-datepicker">
        <button type="button" (click)="onDeadlineSelected($event)">Select</button>
        <button type="button" (click)="dialog.dismiss($event)">Close</button>
    </div>
  `
})
export class DeadlineDialogComponent implements ModalComponent<any> {
  context: any;
  deadline: Date;
  deadlineControl:FormControl;

  constructor(public dialog: DialogRef<any>) {
    if (dialog.context.deadline) {
      this.deadline = moment.utc(dialog.context.deadline).toDate();
    } else {
      let deadline = moment(moment.utc(new Date()));
      this.deadline = deadline.add(2, 'days').toDate();
    }

    this.deadlineControl = new FormControl(this.deadline);
  }

  get min() {
    if (moment().add({minutes:30}) < moment().endOf('day')) {
      return moment().toDate().toString();
    } else {
      return moment().add(1, 'day').startOf('day').toDate().toString();
    }
  }

  onDeadlineSelected($event: Event) {
    $event.preventDefault();
    this.dialog.close(this.deadlineControl.value);
  }
}
