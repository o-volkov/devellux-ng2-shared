import { EventEmitter } from '@angular/core';
import { FormControl } from '@angular/forms';
import { CustomNgInput } from '../../components/custom_inputs/custom-ng2-input';
export declare class ExpandingSearchComponent extends CustomNgInput {
    delay: number;
    valueChanged: EventEmitter<any>;
    input: FormControl;
    ngOnInit(): void;
    updateViewValue(value: any): void;
    clearInput(): void;
}
