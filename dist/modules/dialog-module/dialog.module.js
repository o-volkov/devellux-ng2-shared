import { NgModule, Injectable } from "@angular/core";
import { Modal, ModalModule, Overlay } from "./components/angular2-modal/index";
import { BSModalContextBuilder, BSModalContext } from "./components/angular2-modal/plugins/bootstrap/modal-context";
import { BootstrapModalModule } from "./components/angular2-modal/plugins/bootstrap/bootstrap.module";
import { DialogRef } from "./components/angular2-modal/models/dialog-ref";
import { OverlayRenderer } from "./components/angular2-modal/index";
import { DOMOverlayRenderer } from "./components/angular2-modal/index";
import { EVENT_MANAGER_PLUGINS } from "@angular/platform-browser";
import { DOMOutsideEventPlugin } from "./components/angular2-modal/providers/outside-event-plugin";
export var DialogService = (function () {
    function DialogService(modal) {
        this.modal = modal;
    }
    DialogService.prototype.open = function (component, data) {
        var builder = new BSModalContextBuilder(data, undefined, BSModalContext);
        var overlayConfig = {
            context: builder.toJSON()
        };
        return this.modal.open(component, overlayConfig).then(function (ref) {
            return ref.result;
        });
    };
    DialogService.decorators = [
        { type: Injectable },
    ];
    DialogService.ctorParameters = [
        { type: Modal, },
    ];
    return DialogService;
}());
export function bootstrapDialogModule() {
    return BootstrapModalModule.getProviders();
}
export var DialogModule = (function () {
    function DialogModule() {
    }
    DialogModule.forRoot = function () {
        return {
            ngModule: DialogModule,
            providers: [
                Overlay,
                { provide: OverlayRenderer, useClass: DOMOverlayRenderer },
                { provide: EVENT_MANAGER_PLUGINS, useClass: DOMOutsideEventPlugin, multi: true },
            ]
        };
    };
    DialogModule.decorators = [
        { type: NgModule, args: [{
                    imports: [
                        ModalModule,
                        BootstrapModalModule
                    ],
                    providers: [
                        DialogService,
                        bootstrapDialogModule()
                    ],
                },] },
    ];
    DialogModule.ctorParameters = [];
    return DialogModule;
}());
export { ModalModule, BootstrapModalModule, DialogRef, Overlay };
//# sourceMappingURL=dialog.module.js.map