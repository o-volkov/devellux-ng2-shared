import { Pipe } from '@angular/core';
import * as moment from 'moment';
export var ToNowPipe = (function () {
    function ToNowPipe() {
    }
    ToNowPipe.prototype.transform = function (date) {
        return moment(moment.utc(date).toDate()).toNow(true);
    };
    ToNowPipe.decorators = [
        { type: Pipe, args: [{ name: 'toNow' },] },
    ];
    ToNowPipe.ctorParameters = [];
    return ToNowPipe;
}());
//# sourceMappingURL=to-now.pipe.js.map