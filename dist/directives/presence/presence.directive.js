import { Directive, Input, Renderer, ElementRef } from '@angular/core';
import { PresenceService } from './presence.service';
export var PresenceDirective = (function () {
    function PresenceDirective(service, el, renderer) {
        this.service = service;
        this.el = el;
        this.renderer = renderer;
        this.presenceOnline = 'status__online';
        this.presenceOffline = 'status__offline';
        this.presenceDefault = false;
    }
    PresenceDirective.prototype.ngOnInit = function () {
        var _this = this;
        this.setStatus(this.presenceDefault);
        this.getStateSub = this.service.isUserOnline(this.presence)
            .subscribe(function (status) { return _this.setStatus(status); });
        this.obsStateSub = this.service.userStatusChange$
            .subscribe(function (obj) { return _this.updatesListener(obj); });
    };
    PresenceDirective.prototype.ngOnDestroy = function () {
        this.getStateSub.unsubscribe();
        this.obsStateSub.unsubscribe();
    };
    PresenceDirective.prototype.setStatus = function (status) {
        this.renderer.setElementClass(this.el.nativeElement, this.presenceOnline, status);
        this.renderer.setElementClass(this.el.nativeElement, this.presenceOffline, !status);
    };
    PresenceDirective.prototype.updatesListener = function (obj) {
        if (this.presence === obj.id) {
            this.setStatus(obj.status);
        }
    };
    PresenceDirective.decorators = [
        { type: Directive, args: [{
                    selector: '[presence]'
                },] },
    ];
    PresenceDirective.ctorParameters = [
        { type: PresenceService, },
        { type: ElementRef, },
        { type: Renderer, },
    ];
    PresenceDirective.propDecorators = {
        'presence': [{ type: Input },],
        'presenceOnline': [{ type: Input },],
        'presenceOffline': [{ type: Input },],
        'presenceDefault': [{ type: Input },],
    };
    return PresenceDirective;
}());
//# sourceMappingURL=presence.directive.js.map