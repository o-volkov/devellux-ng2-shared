import { Directive, Output, EventEmitter, HostListener } from "@angular/core";
import { GoogleService } from "./google.service";
export var GoogleButtonDirective = (function () {
    function GoogleButtonDirective(service) {
        this.service = service;
        this.token = new EventEmitter();
    }
    GoogleButtonDirective.prototype.getFacebookToken = function () {
        var _this = this;
        this.service.getToken().then(function (token) {
            _this.token.emit(token);
        })
            .catch(function () { return ''; });
    };
    GoogleButtonDirective.decorators = [
        { type: Directive, args: [{
                    selector: '[GoogleButton]',
                    providers: [GoogleService]
                },] },
    ];
    GoogleButtonDirective.ctorParameters = [
        { type: GoogleService, },
    ];
    GoogleButtonDirective.propDecorators = {
        'token': [{ type: Output },],
        'getFacebookToken': [{ type: HostListener, args: ['click',] },],
    };
    return GoogleButtonDirective;
}());
//# sourceMappingURL=google-button.directive.js.map