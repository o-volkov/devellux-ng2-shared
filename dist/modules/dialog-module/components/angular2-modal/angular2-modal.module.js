import { ANALYZE_FOR_ENTRY_COMPONENTS, NgModule } from '@angular/core';
import { EVENT_MANAGER_PLUGINS } from '@angular/platform-browser';
import { DOMOutsideEventPlugin, DOMOverlayRenderer } from './providers/index';
import { OverlayRenderer } from './models/tokens';
import { SwapComponentDirective, CSSBackdrop, CSSDialogContainer } from './components/index';
import { Overlay, ModalOverlay, OverlayDialogBoundary, OverlayTarget, DefaultOverlayTarget } from './overlay/index';
export var ModalModule = (function () {
    function ModalModule() {
    }
    ModalModule.withComponents = function (entryComponents) {
        return {
            ngModule: ModalModule,
            providers: [
                { provide: ANALYZE_FOR_ENTRY_COMPONENTS, useValue: entryComponents, multi: true }
            ]
        };
    };
    ModalModule.forRoot = function (entryComponents) {
        return {
            ngModule: ModalModule,
            providers: [
                Overlay,
                { provide: OverlayRenderer, useClass: DOMOverlayRenderer },
                { provide: EVENT_MANAGER_PLUGINS, useClass: DOMOutsideEventPlugin, multi: true },
                { provide: ANALYZE_FOR_ENTRY_COMPONENTS, useValue: entryComponents || [], multi: true }
            ]
        };
    };
    ModalModule.decorators = [
        { type: NgModule, args: [{
                    declarations: [
                        ModalOverlay,
                        SwapComponentDirective,
                        CSSBackdrop,
                        CSSDialogContainer,
                        OverlayDialogBoundary,
                        OverlayTarget,
                        DefaultOverlayTarget
                    ],
                    exports: [
                        CSSBackdrop,
                        CSSDialogContainer,
                        SwapComponentDirective,
                        OverlayDialogBoundary,
                        OverlayTarget,
                        DefaultOverlayTarget
                    ],
                    entryComponents: [
                        ModalOverlay,
                        CSSBackdrop,
                        CSSDialogContainer
                    ]
                },] },
    ];
    ModalModule.ctorParameters = [];
    return ModalModule;
}());
//# sourceMappingURL=angular2-modal.module.js.map