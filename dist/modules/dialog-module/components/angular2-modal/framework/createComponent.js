import { ReflectiveInjector } from '@angular/core';
export function createComponent(cfr, type, vcr, bindings, projectableNodes) {
    return vcr.createComponent(cfr.resolveComponentFactory(type), vcr.length, getInjector(vcr, bindings), projectableNodes);
}
function getInjector(viewContainer, bindings) {
    var ctxInjector = viewContainer.parentInjector;
    return Array.isArray(bindings) && bindings.length > 0 ?
        ReflectiveInjector.fromResolvedProviders(bindings, ctxInjector) : ctxInjector;
}
export default createComponent;
//# sourceMappingURL=createComponent.js.map