import * as Utils from './utils';
export var ImageTransform = (function () {
    function ImageTransform(x, y, scale) {
        this.x = x;
        this.y = y;
        this.scale = scale;
    }
    ImageTransform.parse = function (v) {
        if (v.style) {
            return ImageTransform.parse(v.style[Utils.CSS_TRANSFORM]);
        }
        else {
            return ImageTransform.fromString(v);
        }
    };
    ImageTransform.fromString = function (v) {
        var values = v.split(') ');
        var translate = values[0].substring(12).split(',');
        var scale = values.length > 1 ? parseInt(values[1].substring(6)) : 1;
        var x = translate.length > 1 ? parseInt(translate[0].replace('px', '')) : 0;
        var y = translate.length > 1 ? parseInt(translate[1].replace('px', '')) : 0;
        return new ImageTransform(x, y, scale);
    };
    ;
    ImageTransform.prototype.toString = function () {
        return "translate3d(" + this.x + "px, " + this.y + "px, 0px) scale(" + this.scale + ")";
    };
    ;
    return ImageTransform;
}());
//# sourceMappingURL=image-transform.js.map