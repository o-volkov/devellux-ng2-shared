import {URLSearchParams} from "@angular/http";
import {Subject} from "rxjs/Subject";
import {Observable} from "rxjs/Observable";

// todo <T>  ,  func:(p:URLSearchParams)=>Observable<T|any> ,  подумать еще про механизм unsubscribe
export function makeInfinite<T>(func: (p: URLSearchParams)=>any, opts: any): InfiniteList<T> {
  let params: URLSearchParams = new URLSearchParams();
  Object.keys(opts).forEach(key => {
    if (opts[key]) {
      params.set(key, opts[key]);
    }
  });

  let skip = 0;
  let hasMore = true;
  let result = new Subject();
  let isFirstRun = true;

  function loadMore() {
    if (isFirstRun && this) {
      if (Array.isArray(this.objects)) {
        this.objects.length = 0;
      } else {
        this.objects = [];
      }
      isFirstRun = false;
    }

    if (!hasMore) {
      return;
    }

    params.set('skip', skip.toString());
    return func(params)
      .subscribe(res => {
        skip = skip + res.objects.length;
        hasMore = skip < res.total;

        if(this){
          this.hasMore = hasMore;
          this.objects.push.apply(this.objects, res.objects);
        }

        // for result deprecated
        result.next(res.objects);
        if (!hasMore) {
          result.complete();
        }
        // end for result
      });
  }

  return {result, loadMore, hasMore: true};
}

export interface InfiniteList<T> {
  result: Observable<any>,
  loadMore: Function,
  hasMore: boolean,
  objects?: any[]
}
