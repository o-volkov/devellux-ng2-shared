export declare function privateKey(name: string): string;
export declare function setAssignMethod<T>(obj: T, propertyName: string, writeOnce?: boolean): void;
export declare function setAssignAlias<T>(obj: T, propertyName: string, srcPropertyName: string, hard?: boolean): void;
export interface FluentAssignMethod<T, Z> {
    (value: T): Z;
}
export interface IFluentAssignFactory<Z> {
    fluentAssign: Z;
    setMethod(name: string, defaultValue?: any): IFluentAssignFactory<Z>;
}
export declare class FluentAssignFactory<T> {
    private _fluentAssign;
    constructor(fluentAssign?: FluentAssign<T>);
    setMethod(name: string, defaultValue?: any): FluentAssignFactory<T>;
    readonly fluentAssign: FluentAssign<T>;
}
export declare class FluentAssign<T> {
    private __fluent$base__;
    static compose<T>(defaultValues?: T, initialSetters?: string[]): FluentAssignFactory<T>;
    static composeWith<Z>(fluentAssign: Z): IFluentAssignFactory<Z>;
    constructor(defaultValues?: T | T[], initialSetters?: string[], baseType?: new () => T);
    toJSON(): T;
}
