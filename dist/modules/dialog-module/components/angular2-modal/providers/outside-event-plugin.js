import { Injectable } from '@angular/core';
import { noop } from '../framework/utils';
var eventMap = {
    clickOutside: 'click',
    mousedownOutside: 'mousedown',
    mouseupOutside: 'mouseup',
    mousemoveOutside: 'mousemove'
};
function bubbleNonAncestorHandlerFactory(element, handler) {
    return function (event) {
        var current = event.target;
        do {
            if (current === element) {
                return;
            }
        } while (current.parentNode && (current = current.parentNode));
        handler(event);
    };
}
export var DOMOutsideEventPlugin = (function () {
    function DOMOutsideEventPlugin() {
        if (!document || typeof document.addEventListener !== 'function') {
            this.addEventListener = noop;
        }
    }
    DOMOutsideEventPlugin.prototype.supports = function (eventName) {
        return eventMap.hasOwnProperty(eventName);
    };
    DOMOutsideEventPlugin.prototype.addEventListener = function (element, eventName, handler) {
        var zone = this.manager.getZone();
        var onceOnOutside = function () {
            var listener = bubbleNonAncestorHandlerFactory(element, function (evt) { return zone.runGuarded(function () { return handler(evt); }); });
            document.addEventListener(eventMap[eventName], listener, false);
            return function () { return document.removeEventListener(eventMap[eventName], listener, false); };
        };
        return zone.runOutsideAngular(function () {
            var fn;
            setTimeout(function () { return fn = onceOnOutside(); }, 0);
            return function () { return fn(); };
        });
    };
    DOMOutsideEventPlugin.prototype.addGlobalEventListener = function (target, eventName, handler) {
        throw 'not supported';
    };
    DOMOutsideEventPlugin.decorators = [
        { type: Injectable },
    ];
    DOMOutsideEventPlugin.ctorParameters = [];
    return DOMOutsideEventPlugin;
}());
//# sourceMappingURL=outside-event-plugin.js.map