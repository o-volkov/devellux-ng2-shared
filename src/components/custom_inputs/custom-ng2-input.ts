import {ControlValueAccessor} from "@angular/forms";

export abstract class CustomNgInput implements ControlValueAccessor {
  // The internal data model
  /* tslint:disable */
  private _value;
  /* tslint:enable */

  // Placeholders for the callbacks
  private onTouchedCallback:() => void = (...args) => {
  };
  private onChangeCallback:(value:any) => void = (...args) => {
  };

  protected updateViewValue(v) {
    throw `not implemented updateViewValue(v) in ${this}`
  };


  touched() {
    this.onTouchedCallback();
  }

  // get accessor
  get value():any {
    return this._value;
  };

  // set accessor including call the onchange callback
  set value(v:any) {
    if (v !== this._value) {
      this._value = v;
      this.onChangeCallback(v);
      this.updateViewValue(v);
    }
  }

  // From ControlValueAccessor interface
  writeValue(value:any) {
    this.value = value;
  }

  // From ControlValueAccessor interface
  registerOnChange(fn:any) {
    this.onChangeCallback = fn;
  }

  // From ControlValueAccessor interface
  registerOnTouched(fn:any) {
    this.onTouchedCallback = fn;
  }
}
