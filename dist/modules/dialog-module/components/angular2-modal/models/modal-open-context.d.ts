import { ResolvedReflectiveProvider } from '@angular/core';
import { FluentAssignMethod } from '../framework/fluent-assign';
import { ModalComponent, WideVCRef } from './tokens';
import { Modal } from '../providers/index';
import { DialogRef } from './dialog-ref';
import { ModalContext, ModalContextBuilder } from './modal-context';
import { ModalControllingContextBuilder } from './overlay-context';
export declare class ModalOpenContext extends ModalContext {
    component: any;
    modal: Modal;
}
export declare abstract class ModalOpenContextBuilder<T extends ModalOpenContext> extends ModalContextBuilder<T> implements ModalControllingContextBuilder<T> {
    component: FluentAssignMethod<ModalComponent<T>, this>;
    constructor(defaultValues?: T, initialSetters?: string[], baseType?: new () => T);
    protected $$beforeOpen(config: T): ResolvedReflectiveProvider[];
    open(viewContainer?: WideVCRef): Promise<DialogRef<T>>;
}
