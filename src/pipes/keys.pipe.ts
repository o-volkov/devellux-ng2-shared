import { Pipe, PipeTransform  } from '@angular/core';


@Pipe({ name: 'keys' })
export class KeysPipe implements PipeTransform {

    transform (input: any): any {

        if (typeof input !== 'object') {
            return input;
        }

        return Object.keys(input);
    }
}
