import { Component, Input } from '@angular/core';
export var BadgeRendererComponent = (function () {
    function BadgeRendererComponent() {
        this.badge = { total: 0, new: 0 };
        this.badgeMode = 'new';
    }
    BadgeRendererComponent.decorators = [
        { type: Component, args: [{
                    selector: 'badge-renderer',
                    template: "\n  <span *ngIf=\"badgeMode === 'new'\">\n    <span class=\"badge new\" *ngIf=\"badge?.new\">{{badge.new}}</span>\n  </span>\n\n  <span *ngIf=\"badgeMode === 'total'\">\n    <span class=\"badge\" [class.new]=\"badge.new\" *ngIf=\"badge?.total\">\n      <span *ngIf=\"!badge.new\">{{badge.total}}</span>\n      <span *ngIf=\"badge.new>0\">+{{badge.new}}</span>\n    </span>\n  </span>",
                },] },
    ];
    BadgeRendererComponent.ctorParameters = [];
    BadgeRendererComponent.propDecorators = {
        'badge': [{ type: Input },],
        'badgeMode': [{ type: Input },],
    };
    return BadgeRendererComponent;
}());
//# sourceMappingURL=badge-renderer.component.js.map