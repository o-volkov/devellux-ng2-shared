import { FluentAssignMethod } from '../../../../../components/angular2-modal';
import { BSMessageModalButtonConfig, BSMessageModalButtonHandler } from '../message-modal.component';
import { BSModalContext, BSModalContextBuilder } from '../modal-context';
export interface MessageModalPreset extends BSModalContext {
    headerClass: string;
    title: string;
    titleHtml: string;
    message: string;
    body: string;
    bodyClass: string;
    footerClass: string;
    buttons: BSMessageModalButtonConfig[];
}
export declare abstract class MessageModalPresetBuilder<T extends MessageModalPreset> extends BSModalContextBuilder<T> {
    headerClass: FluentAssignMethod<string, this>;
    title: FluentAssignMethod<string, this>;
    titleHtml: FluentAssignMethod<string, this>;
    message: FluentAssignMethod<string, this>;
    body: FluentAssignMethod<string, this>;
    bodyClass: FluentAssignMethod<string, this>;
    footerClass: FluentAssignMethod<string, this>;
    constructor(defaultValues?: T, initialSetters?: string[], baseType?: new () => T);
    addButton(css: string, caption: string, onClick: BSMessageModalButtonHandler): this;
}
