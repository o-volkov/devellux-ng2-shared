export function extend(m1, m2) {
    var m = {};
    for (var attr in m1) {
        if (m1.hasOwnProperty(attr)) {
            m[attr] = m1[attr];
        }
    }
    for (var attr in m2) {
        if (m2.hasOwnProperty(attr)) {
            m[attr] = m2[attr];
        }
    }
    return m;
}
export function arrayUnion(arr1, arr2) {
    return arr1
        .concat(arr2.filter(function (v) { return arr1.indexOf(v) === -1; }));
}
export function supportsKey(keyCode, config) {
    if (!Array.isArray(config))
        return config === null ? false : true;
    return config.indexOf(keyCode) > -1;
}
export function toStyleString(obj) {
    return Object.getOwnPropertyNames(obj)
        .map(function (k) { return (k + ":" + obj[k]); })
        .join(';');
}
export var PromiseCompleter = (function () {
    function PromiseCompleter() {
        var _this = this;
        this.promise = new Promise(function (res, rej) {
            _this.resolve = res;
            _this.reject = rej;
        });
    }
    return PromiseCompleter;
}());
export function noop() { }
//# sourceMappingURL=utils.js.map