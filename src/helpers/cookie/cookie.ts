// возвращает cookie с именем name, если есть, если нет, то undefined
export function getCookie(name) {
  var matches = document.cookie.match(new RegExp(
    '(?:^|; )' + name.replace(/([\.$?*|{}\(\)\[\]\\\/\+^])/g, '\\$1') + '=([^;]*)'
  ));
  return matches ? decodeURIComponent(matches[1]) : undefined;
}

// устанавливает cookie с именем name и значением value
// options - объект с свойствами cookie (expires, path, domain, secure)
export function setCookie(name, value, options) {
  options = options || {expires: 14 * 24 * 3600};

  var expires = options.expires;

  if (typeof expires == 'number' && expires) {
    var d = new Date();
    d.setTime(d.getTime() + expires * 1000);
    expires = options.expires = d;
  }
  if (expires && expires.toUTCString) {
    options.expires = expires.toUTCString();
  }
  if (!options.path) {
    options.path = '/';
  }
  value = encodeURIComponent(value);
  var updatedCookie = name + '=' + value;

  for (var propName in options) {
    updatedCookie += '; ' + propName;
    var propValue = options[propName];
    if (propValue !== true) {
      updatedCookie += '=' + propValue;
    }
  }

  document.cookie = updatedCookie;
}

// удаляет cookie с именем name
export function deleteCookie(name) {
  setCookie(name, '', {
    expires: -1
  });
}

export function moveFromUriToCookies(urlKey: string, cookieKey: string = urlKey) {
  let search = window.location.search;
  if (search) {
    let obj = {};
    search
      .replace('?', '')
      .split('&')
      .forEach(part => {
        let tmp = part.split('=');
        if (tmp[0]) {
          obj[tmp[0]] = tmp[1];
        }
      });

    if (obj[urlKey]) {
      setCookie(cookieKey, obj[urlKey], {});
    }
    delete obj[urlKey];

    if (Object.keys(obj).length) {
      search = '?';
      Object.keys(obj).forEach(key => {
        search += key + '=' + obj[key] + '&';
      });
      search = search.slice(0, -1);
    } else {
      search = '';
    }
  }


  let url = window.location.pathname + search + window.location.hash;
  window.history.replaceState({}, document.title, url);
}
