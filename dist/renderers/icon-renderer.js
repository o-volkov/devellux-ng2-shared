import { Component, Input, ViewEncapsulation } from '@angular/core';
export var IconRendererComponent = (function () {
    function IconRendererComponent() {
    }
    IconRendererComponent.decorators = [
        { type: Component, args: [{
                    selector: 'icon-renderer',
                    template: "\n    <svg width=\"100%\" height=\"100%\">\n      <use [attr.xlink:href]=\"'/assets/symbols.svg#'+name\" x=\"0\" y=\"0\" width=\"100%\" height=\"100%\"></use>\n    </svg>\n  ",
                    styles: ["\n    svg {\n      fill: currentColor;\n    }\n  "],
                    encapsulation: ViewEncapsulation.None,
                },] },
    ];
    IconRendererComponent.ctorParameters = [];
    IconRendererComponent.propDecorators = {
        'name': [{ type: Input },],
    };
    return IconRendererComponent;
}());
//# sourceMappingURL=icon-renderer.js.map