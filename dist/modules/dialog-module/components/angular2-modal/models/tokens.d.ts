import { ComponentRef, ViewContainerRef, TemplateRef, Type, ResolvedReflectiveProvider } from '@angular/core';
import { ModalOverlay } from '../overlay/index';
import { DialogRef } from './dialog-ref';
import { OverlayContext } from '../models/overlay-context';
import { Maybe } from '../framework/utils';
export declare enum DROP_IN_TYPE {
    alert = 0,
    prompt = 1,
    confirm = 2,
}
export declare type WideVCRef = ViewContainerRef | string;
export declare type ContainerContent = string | TemplateRef<any> | Type<any>;
export interface OverlayPlugin extends Function {
    <T>(component: any, dialogRef: DialogRef<T>, config: OverlayConfig): Maybe<DialogRef<any>>;
}
export interface OverlayConfig {
    context?: OverlayContext;
    bindings?: ResolvedReflectiveProvider[];
    viewContainer?: WideVCRef;
    renderer?: OverlayRenderer;
    overlayPlugins?: OverlayPlugin | Array<OverlayPlugin>;
}
export interface ModalComponent<T> {
    dialog: DialogRef<T>;
}
export interface CloseGuard {
    beforeDismiss?(): boolean | Promise<boolean>;
    beforeClose?(): boolean | Promise<boolean>;
}
export declare abstract class OverlayRenderer {
    abstract render(dialogRef: DialogRef<any>, vcRef: ViewContainerRef): ComponentRef<ModalOverlay>;
}
