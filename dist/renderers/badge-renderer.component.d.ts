export declare class BadgeRendererComponent {
    badge: {
        total: number;
        new: number;
    };
    badgeMode: 'total' | 'new';
}
