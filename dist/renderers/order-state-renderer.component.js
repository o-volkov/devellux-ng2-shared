import { Component, Input } from '@angular/core';
export var OrderStateRendererComponent = (function () {
    function OrderStateRendererComponent() {
    }
    Object.defineProperty(OrderStateRendererComponent.prototype, "orderState", {
        get: function () {
            if (!this.state) {
                return '';
            }
            switch (this.state) {
                case 'available':
                    return 'Available';
                case 'bidding':
                    return 'Bidding';
                case 'in_progress':
                    return 'In Progress';
                case 'finished':
                    return 'Finished';
                case 'cancelled':
                    return 'Canceled';
                case 'canceled':
                    return 'Canceled';
            }
        },
        set: function (state) {
            this.state = state;
        },
        enumerable: true,
        configurable: true
    });
    OrderStateRendererComponent.decorators = [
        { type: Component, args: [{
                    selector: 'order-state-renderer',
                    template: "\n    <span \n      [class.available]=\"orderState == 'Available'\"\n      [class.bidding]=\"orderState == 'Bidding'\"\n      [class.in_progress]=\"orderState == 'In Progress'\"\n      [class.finished]=\"orderState == 'Finished'\"\n      [class.cancelled]=\"orderState == 'Canceled'\"\n    >{{orderState}}</span>",
                    styles: [".bidding{\n    color: #0082D5;\n}\n.in_progress{\n    color: #ff9948;\n}\n.finished{\n    color: #039547;\n}\n.cancelled{\n    color: #d2335c;\n}\n    /* LIGHT */\n:host(.light) .bidding{\n    color: #81BBFF;\n}\n:host(.light) .in_progress{\n    color: #FFAA59;\n}\n:host(.light) .finished{\n    color: #04BE5B;\n}\n:host(.light) .cancelled{\n    color: #F56185;\n}"]
                },] },
    ];
    OrderStateRendererComponent.ctorParameters = [];
    OrderStateRendererComponent.propDecorators = {
        'orderState': [{ type: Input },],
    };
    return OrderStateRendererComponent;
}());
//# sourceMappingURL=order-state-renderer.component.js.map