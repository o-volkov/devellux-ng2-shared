import { ModuleWithProviders, Type } from '@angular/core';
export declare class ModalModule {
    static withComponents(entryComponents: Array<Type<any> | any[]>): ModuleWithProviders;
    static forRoot(entryComponents?: Array<Type<any> | any[]>): ModuleWithProviders;
}
