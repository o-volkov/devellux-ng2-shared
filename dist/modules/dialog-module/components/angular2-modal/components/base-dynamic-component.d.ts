import { ComponentRef, ElementRef, ResolvedReflectiveProvider, OnDestroy, ViewContainerRef, Renderer } from '@angular/core';
import { Observable } from 'rxjs/Observable';
import { Subject } from 'rxjs/Subject';
import 'rxjs/add/operator/filter';
export declare class BaseDynamicComponent implements OnDestroy {
    protected el: ElementRef;
    protected renderer: Renderer;
    animationEnd$: Observable<TransitionEvent | AnimationEvent>;
    protected animationEnd: Subject<TransitionEvent | AnimationEvent>;
    constructor(el: ElementRef, renderer: Renderer);
    activateAnimationListener(): void;
    setStyle(prop: string, value: string): this;
    forceReflow(): void;
    addClass(css: string, forceReflow?: boolean): void;
    removeClass(css: string, forceReflow?: boolean): void;
    ngOnDestroy(): void;
    myAnimationEnd$(): Observable<AnimationEvent | TransitionEvent>;
    protected _addComponent<T>(type: any, vcRef: ViewContainerRef, bindings?: ResolvedReflectiveProvider[], projectableNodes?: any[][]): ComponentRef<T>;
    private onEnd(event);
}
