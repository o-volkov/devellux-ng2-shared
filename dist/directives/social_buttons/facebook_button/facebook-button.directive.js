import { Directive, Output, EventEmitter, HostListener } from "@angular/core";
import { FacebookService } from "./facebook.service";
export var FacebookButtonDirective = (function () {
    function FacebookButtonDirective(service) {
        this.service = service;
        this.token = new EventEmitter();
    }
    FacebookButtonDirective.prototype.getFacebookToken = function () {
        var _this = this;
        this.service.getToken().then(function (token) {
            _this.token.emit(token);
        })
            .catch(function () { return ''; });
    };
    FacebookButtonDirective.decorators = [
        { type: Directive, args: [{
                    selector: '[FacebookButton]',
                    providers: [FacebookService]
                },] },
    ];
    FacebookButtonDirective.ctorParameters = [
        { type: FacebookService, },
    ];
    FacebookButtonDirective.propDecorators = {
        'token': [{ type: Output },],
        'getFacebookToken': [{ type: HostListener, args: ['click',] },],
    };
    return FacebookButtonDirective;
}());
//# sourceMappingURL=facebook-button.directive.js.map