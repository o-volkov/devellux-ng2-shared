import { Directive, ElementRef, Input, HostListener } from '@angular/core';
export var FileSelectDirective = (function () {
    function FileSelectDirective(element) {
        this.element = element;
    }
    FileSelectDirective.prototype.getOptions = function () {
        return this.uploader.options;
    };
    FileSelectDirective.prototype.getFilters = function () {
        return void 0;
    };
    FileSelectDirective.prototype.isEmptyAfterSelection = function () {
        return !!this.element.nativeElement.attributes.multiple;
    };
    FileSelectDirective.prototype.onChange = function () {
        var files = this.element.nativeElement.files;
        var options = this.getOptions();
        var filters = this.getFilters();
        this.uploader.addToQueue(files, options, filters);
        if (this.isEmptyAfterSelection()) {
        }
    };
    FileSelectDirective.decorators = [
        { type: Directive, args: [{ selector: '[ng2FileSelect]' },] },
    ];
    FileSelectDirective.ctorParameters = [
        { type: ElementRef, },
    ];
    FileSelectDirective.propDecorators = {
        'uploader': [{ type: Input },],
        'onChange': [{ type: HostListener, args: ['change',] },],
    };
    return FileSelectDirective;
}());
//# sourceMappingURL=file-select.directive.js.map