var __extends = (this && this.__extends) || function (d, b) {
    for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p];
    function __() { this.constructor = d; }
    d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
};
import { Component, Output, EventEmitter, forwardRef } from '@angular/core';
import { FormControl, NG_VALUE_ACCESSOR } from '@angular/forms';
import { CustomNgInput } from '../../components/custom_inputs/custom-ng2-input';
export var ExpandingSearchComponent = (function (_super) {
    __extends(ExpandingSearchComponent, _super);
    function ExpandingSearchComponent() {
        _super.apply(this, arguments);
        this.delay = 500;
        this.valueChanged = new EventEmitter();
        this.input = new FormControl('');
    }
    ExpandingSearchComponent.prototype.ngOnInit = function () {
        var _this = this;
        this.input.valueChanges
            .do(function () { return _this.touched(); })
            .debounceTime(this.delay)
            .distinctUntilChanged()
            .subscribe(function (val) {
            _this.value = val;
            _this.valueChanged.emit(val);
        });
    };
    ExpandingSearchComponent.prototype.updateViewValue = function (value) {
        if (this.input) {
            this.input.patchValue(value, { onlySelf: true });
        }
    };
    ExpandingSearchComponent.prototype.clearInput = function () {
        this.input.reset('', { onlySelf: true });
    };
    ExpandingSearchComponent.decorators = [
        { type: Component, args: [{
                    selector: 'expanding-search-component',
                    template: "<div class=\"search__container\">\n    <input type=\"text\" class=\"form-input type__search\" placeholder=\"Search\"\n     [formControl]=\"input\" [class.not-empty]=\"input.value\">\n    \n    <button class=\"search__clear button empty\" type=\"button\"\n      *ngIf=\"input.value\" (click)=\"clearInput()\">\n      <icon-renderer class=\"icon close-thin\" name=\"close-thin\"></icon-renderer>\n    </button>\n  </div>",
                    styles: ["\n  \n  :host {\n    position: relative;\n    display: inline-block;\n    width: auto;\n  }\n  \n  .form-input.ng-valid { \n    border-color: #cad3df;\n  }\n  \n  .form-input.ng-valid:focus { \n    border-color: #54657e;\n  }\n  \n  .seach__container {\n    position: relative;\n    width: 170px !important;\n  }\n  \n  .form-input {\n     width: 170px !important;\n     transition: all .3s ease;\n     z-index: 3;\n  }\n  \n  .form-input:focus,\n  .form-input.not-empty {\n    width: 360px !important;\n  }\n  \n  .form-input + .search__clear {\n    position: absolute;\n    right: 15px;\n    top: calc(50% - 12px);\n    z-index: 4;\n    \n    width: 20px;\n    height: 20px;\n    \n    color: #fff;\n    background-color: #cad3df;\n    border-radius: 10px;\t\n  }\n  \n  .form-input + .search__clear:hover,\n  .form-input + .search__clear:focus {\n    background-color: #8493A8;\n  }\n  \n  .icon.close-thin {\n    padding: 5px;\n  }\n  \n    \n  "],
                    providers: [
                        { provide: NG_VALUE_ACCESSOR, useExisting: forwardRef(function () { return ExpandingSearchComponent; }), multi: true }
                    ]
                },] },
    ];
    ExpandingSearchComponent.ctorParameters = [];
    ExpandingSearchComponent.propDecorators = {
        'valueChanged': [{ type: Output },],
    };
    return ExpandingSearchComponent;
}(CustomNgInput));
//# sourceMappingURL=expanding-search.component.js.map