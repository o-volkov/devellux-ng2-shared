import { ComponentRef, ComponentFactoryResolver, ViewContainerRef, ResolvedReflectiveProvider } from '@angular/core';
export declare function createComponent(cfr: ComponentFactoryResolver, type: any, vcr: ViewContainerRef, bindings: ResolvedReflectiveProvider[], projectableNodes?: any[][]): ComponentRef<any>;
export default createComponent;
