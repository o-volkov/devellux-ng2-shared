import { Headers, RequestOptions, RequestOptionsArgs } from "@angular/http";
export declare class AppRequestOptions extends RequestOptions {
    headers: Headers;
    withCredentials: boolean;
    baseUrl: string;
    private static isAbsoluteUrl(url);
    private prepareUrl(url);
    merge(options?: RequestOptionsArgs): AppRequestOptions;
}
