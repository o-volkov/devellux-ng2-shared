import { NgModule } from "@angular/core";
import { CommonModule } from "@angular/common";
import { HttpModule, RequestOptions } from "@angular/http";
import { TransactionStateRendererComponent } from "../renderers/transaction-state-renderer.component";
import { CutPipe } from "../pipes/cut.pipe";
import { ToNowPipe } from "../pipes/to-now.pipe";
import { FromNowPipe } from "../pipes/from-now.pipe";
import { OrderStateRendererComponent } from "../renderers/order-state-renderer.component";
import { DeadlineRendererComponent } from "../renderers/deadline-renderer.component";
import { FacebookButtonDirective } from "../directives/social_buttons/facebook_button/facebook-button.directive";
import { GoogleButtonDirective } from "../directives/social_buttons/google_button/google-button.directive";
import { FormErrorsComponent } from "../helpers/form/form-errors.component";
import { KeysPipe } from "../pipes/keys.pipe";
import { ToMoneyPipe } from "../pipes/to-money.pipe";
import { HttpClient, API_URL_TOKEN } from "../helpers/http/client";
import { AppRequestOptions } from "../helpers/http/request-options";
import { ReactiveFormsModule, FormsModule } from "@angular/forms";
import { DropdownSelectorComponent } from "../components/custom_inputs/dropdown-select/dropdown.component";
import { ButtonSpinnerComponent } from "../directives/button-spinner.component";
import { LoadingIndicatorComponent } from "../directives/loading-indicator";
import { FILE_UPLOAD_DIRECTIVES } from "../directives/ng2-file-uploader/shared/index";
import { CropBoundaryViewportDirective } from "../directives/ng2-image-crop/boundary/viewport/crop-viewport.directive";
import { CropBoundaryOverlayDirective } from "../directives/ng2-image-crop/boundary/overlay/crop-overlay.directive";
import { CropBoundaryImageDirective } from "../directives/ng2-image-crop/boundary/image/crop-image.directive";
import { DatetimePickerComponent } from "../directives/ng2-datetime-picker/ng2-datetime-picker.component";
import { CropBoundaryComponent } from "../directives/ng2-image-crop/boundary/crop-boundary.component";
import { DROPDOWN_DIRECTIVES } from "../components/custom_inputs/dropdown-select/deps/index";
import { SelectComponent } from "../components/custom_inputs/select2/deps/select";
import { OffClickDirective } from "../components/custom_inputs/select2/deps/off-click";
import { HighlightPipe } from "../components/custom_inputs/select2/deps/select-pipes";
import { SSE_URL_TOKEN } from "../services/see.service";
import { TimezoneTimePipe } from "../pipes/timezone-time.pipe";
import { BadgeRendererComponent } from "../renderers/badge-renderer.component";
import { IconRendererComponent } from "../renderers/icon-renderer";
import { OrderRateComponent } from "../directives/order-rate.component";
import { DeadlineDialogComponent } from "../directives/datetimepicker-modal/deadline.dialog";
import { DatetimePickerModalComponent } from "../directives/datetimepicker-modal/datetimepicker-modal.component";
import { Ng2FileUploaderComponent } from "../directives/ng2-file-uploader/ng2-file-uploader.component";
import { Ng2ImageCropComponent } from "../directives/ng2-image-crop/ng2-image-crop.component";
import { CountryComponent } from "../directives/country.component";
import { Select2Component } from "../components/custom_inputs/select2/select2.component";
import { ExpandingSearchComponent } from '../directives/searchers/expanding-search.component';
var arr = [
    TransactionStateRendererComponent,
    CutPipe,
    ToNowPipe,
    FromNowPipe,
    OrderStateRendererComponent,
    DeadlineRendererComponent,
    FacebookButtonDirective,
    LoadingIndicatorComponent,
    GoogleButtonDirective,
    FormErrorsComponent,
    KeysPipe,
    ToMoneyPipe,
    TimezoneTimePipe,
    DropdownSelectorComponent,
    BadgeRendererComponent,
    ButtonSpinnerComponent,
    IconRendererComponent,
    OrderRateComponent,
    DeadlineDialogComponent,
    DatetimePickerModalComponent,
    Ng2FileUploaderComponent,
    Ng2ImageCropComponent,
    CountryComponent,
    Select2Component,
    ExpandingSearchComponent,
    FILE_UPLOAD_DIRECTIVES,
    CropBoundaryImageDirective,
    CropBoundaryOverlayDirective,
    CropBoundaryViewportDirective,
    DatetimePickerComponent,
    CropBoundaryComponent,
    DROPDOWN_DIRECTIVES,
    SelectComponent,
    OffClickDirective,
    HighlightPipe,
];
export var DevelluxSharedModule = (function () {
    function DevelluxSharedModule() {
    }
    DevelluxSharedModule.forRoot = function (config) {
        if (config === void 0) { config = { apiUrl: '', sseUrl: '' }; }
        return {
            ngModule: DevelluxSharedModule,
            providers: [
                HttpClient,
                {
                    provide: RequestOptions,
                    useClass: AppRequestOptions,
                },
                {
                    provide: API_URL_TOKEN,
                    useValue: config.apiUrl
                },
                {
                    provide: SSE_URL_TOKEN,
                    useValue: config.sseUrl
                },
            ],
        };
    };
    DevelluxSharedModule.decorators = [
        { type: NgModule, args: [{
                    imports: [CommonModule, HttpModule, ReactiveFormsModule, FormsModule],
                    declarations: arr,
                    exports: arr,
                    entryComponents: [DeadlineDialogComponent],
                },] },
    ];
    DevelluxSharedModule.ctorParameters = [];
    return DevelluxSharedModule;
}());
//# sourceMappingURL=devellux-shared.module.js.map