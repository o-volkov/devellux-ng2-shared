import {Injectable, EventEmitter} from "@angular/core";
import {JwtHelper} from "../helpers/jwt/jwt";
import {getCookie, setCookie, deleteCookie} from "../helpers/cookie/cookie";

@Injectable()
export class User {
  private token_;
  private refreshToken_;
  private data_;
  status$:EventEmitter<boolean> = new EventEmitter<boolean>();
  data$:EventEmitter<any> = new EventEmitter();

  constructor() {
    window.addEventListener('storage', (event) => {
      if (event.key === 'data' && event.newValue === null) {
        console.log('succesful logout');
        this.logout();
      }
    });
  }

  isAuthenticated():boolean {
    return !!this.token;
  }

  getId():string {
    if (this.data) {
      return this.data._id;
    }
    return '';
  }

  get data() {
    if (!this.isAuthenticated() && this.data_) {
      Object.keys(this.data_).forEach(key => {
        delete this.data_[key];
      });
      this.data_ = null;
    }
    if (this.data_) {
      return this.data_;
    }
    if (localStorage.getItem('data')) {
      this.data_ = JSON.parse(localStorage.getItem('data'));
      return this.data_;
    }
    if (this.token) {
      this.data_ = JwtHelper.decodeToken(this.token_);
      return this.data_;
    }
    return {};
  }

  set data(value) {
    // TODO rewrite to assign
    // Object.assign(this.data_, value);
    if (!this.isAuthenticated()) {
      return;
    }
    if (value !== undefined && value !== null) {
      for (var nextKey in value) {
        if (value.hasOwnProperty(nextKey)) {
          this.data_[nextKey] = value[nextKey];
        }
      }
    }

    try {
      localStorage.setItem('data', JSON.stringify(this.data_));
    } catch (er) {
      console.error('To hell with him', er);
    }
    this.data$.emit(this.data_);
  }

  get token() {
    if (!this.token_) {
      this.token_ = getCookie('token');
    }
    return this.token_;
  }

  set token(value) {
    this.token_ = value;
    try {
      setCookie('token', value, {expires: 3600 * 24 * 30});
    } catch (er) {
      console.error('To hell with him', er);
    }
    this.status$.emit(this.isAuthenticated());
  }


  get refreshToken() {
    if (!this.refreshToken_) {

    }
    return this.refreshToken_;
  }

  logout() {
    deleteCookie('token');
    localStorage.removeItem('data');
    this.token_ = null;
    delete this.token_;
    this.data_ = null;
    delete this.data_;
    this.status$.emit(false);
  }
}
