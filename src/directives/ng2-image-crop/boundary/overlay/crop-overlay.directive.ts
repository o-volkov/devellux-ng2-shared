import { Directive, Input, ElementRef, Renderer } from '@angular/core';


@Directive({
  selector: '[crop-boundary-overlay]',
})
export class CropBoundaryOverlayDirective {
  // @Input() options: {};

  constructor(public element: ElementRef, public renderer: Renderer) {
  }
}
