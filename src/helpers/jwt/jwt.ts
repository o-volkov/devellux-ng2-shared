import { Injectable } from '@angular/core';

declare var escape: any;

@Injectable()
export class JwtHelper {

  public static urlBase64Decode(str: string) {
    let output = str.replace(/-/g, '+').replace(/_/g, '/');
    switch (output.length % 4) {
      case 0:
      {
        break;
      }
      case 2:
      {
        output += '==';
        break;
      }
      case 3:
      {
        output += '=';
        break;
      }
      default:
      {
        throw 'Illegal base64url string!';
      }
    }

    return decodeURIComponent(escape(window.atob(output)));
  }

  public static decodeToken(token: string) {
    let parts = token.split('.');

    if (parts.length !== 3) {
      throw new Error('JWT must have 3 parts');
    }

    let decoded = JwtHelper.urlBase64Decode(parts[1]);
    if (!decoded) {
      throw new Error('Cannot decode the token');
    }

    return JSON.parse(decoded);
  }


  public static getTokenExpirationDate(token: string) {
    let decoded: any;
    decoded = JwtHelper.decodeToken(token);

    if (typeof decoded.exp === 'undefined') {
      return null;
    }

    let date = new Date(0);
    date.setUTCSeconds(decoded.exp);

    return date;
  }

  public static isTokenExpired(token: string, offsetSeconds?: number) {
    let date = JwtHelper.getTokenExpirationDate(token);
    offsetSeconds = offsetSeconds || 0;
    if (date === null) {
      return false;
    }

    return !(date.valueOf() > (new Date().valueOf() + (offsetSeconds * 1000)));
  }
}
