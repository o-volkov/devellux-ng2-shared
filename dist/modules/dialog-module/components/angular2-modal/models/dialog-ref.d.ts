import { ComponentRef } from '@angular/core';
import { Observable } from 'rxjs/Observable';
import { Overlay, ModalOverlay } from '../overlay/index';
import { CloseGuard } from '../models/tokens';
export declare class DialogRef<T> {
    overlay: Overlay;
    context: T;
    overlayRef: ComponentRef<ModalOverlay>;
    inElement: boolean;
    destroyed: boolean;
    onDestroy: Observable<void>;
    private _resultDeferred;
    private _onDestroy;
    private closeGuard;
    constructor(overlay: Overlay, context?: T);
    readonly result: Promise<any>;
    setCloseGuard(guard: CloseGuard): void;
    close(result?: any): void;
    dismiss(): void;
    bailOut(): void;
    destroy(): void;
    private _destroy();
    private _fireHook<T>(name);
}
