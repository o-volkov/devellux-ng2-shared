import { OnInit } from '@angular/core';
export declare class DeadlineRendererComponent implements OnInit {
    deadline: any;
    highlighted: any;
    isFutute: any;
    ngOnInit(): void;
    setClasses(): {
        'text-color__danger': any;
        'weight-400': any;
    };
}
