import {Component, Input} from "@angular/core";


@Component({
  selector: 'country-cmp',
  template: `<span *ngIf="short" class="{{'flag-icon flag-icon-' + short.toLowerCase()}}" style="margin-right: 5px">
              </span><span class="country-name weight-400">{{short || '-'}}</span>`
})

export class CountryComponent {
  @Input() short;
}
