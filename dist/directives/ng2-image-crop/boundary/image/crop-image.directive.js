import { Directive, HostBinding, ElementRef, Renderer } from '@angular/core';
export var CropBoundaryImageDirective = (function () {
    function CropBoundaryImageDirective(element, renderer) {
        this.element = element;
        this.renderer = renderer;
    }
    CropBoundaryImageDirective.prototype.getBoundingClientRect = function () {
        return this.element.nativeElement.getBoundingClientRect();
    };
    CropBoundaryImageDirective.decorators = [
        { type: Directive, args: [{
                    selector: '[crop-boundary-image]',
                },] },
    ];
    CropBoundaryImageDirective.ctorParameters = [
        { type: ElementRef, },
        { type: Renderer, },
    ];
    CropBoundaryImageDirective.propDecorators = {
        'src': [{ type: HostBinding, args: ['attr.src',] },],
        'opacity': [{ type: HostBinding, args: ['style.opacity',] },],
    };
    return CropBoundaryImageDirective;
}());
//# sourceMappingURL=crop-image.directive.js.map