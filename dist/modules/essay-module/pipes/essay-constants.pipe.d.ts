import { PipeTransform } from '@angular/core';
import { EssayConstantsService, Constants } from '../services/essay-constants.service';
export declare class EssayConstantsPipe implements PipeTransform {
    private constants;
    data: Constants;
    constructor(constants: EssayConstantsService);
    transform(value: any, type: 'services' | 'styles' | 'levels' | 'subjects' | 'types'): any;
}
