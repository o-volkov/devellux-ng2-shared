import { Injectable, Optional, Inject } from '@angular/core';
import { RequestMethod, RequestOptionsArgs, Request, Http, Response } from '@angular/http';
import { Observable } from 'rxjs/Observable';
import { AppRequestOptions } from './request-options';
import { OpaqueToken } from '@angular/core';

export const API_URL_TOKEN = new OpaqueToken('apiUrl');


@Injectable()
export class HttpClient {
  private successIntercept = [];
  private errorIntercept = [];

  constructor(private http: Http, @Optional() @Inject(API_URL_TOKEN) private baseUrl) {
  }

  public addSuccessIntercept(intercept: (val)=>any) {
    this.successIntercept.push(intercept);
  }

  public addErrorIntercept(intercept: (er)=>Observable<any>) {
    this.errorIntercept.push(intercept);
  }

  private prepareRequest(requestArgs: RequestOptionsArgs, additionalOptions: RequestOptionsArgs): Observable<Response> {
    let options = new AppRequestOptions();
    options.baseUrl = this.baseUrl;

    if (additionalOptions) {
      requestArgs = Object.assign(requestArgs, additionalOptions);
    }

    options = options.merge(requestArgs);

    if (typeof options.body !== 'string') {
      options.body = JSON.stringify(options.body);
    }

    let req = new Request(options);

    return this.intercept(this.http.request(req));
  }

  get(url: string, options?: RequestOptionsArgs): Observable<Response> {
    return this.prepareRequest({url: url, method: RequestMethod.Get}, options);
  }

  post(url: string, body?: any, options?: RequestOptionsArgs): Observable<Response> {
    return this.prepareRequest({url: url, body: body, method: RequestMethod.Post}, options);
  }

  put(url: string, body?: any, options ?: RequestOptionsArgs): Observable<Response> {
    return this.prepareRequest({url: url, body: body, method: RequestMethod.Put}, options);
  }

  delete(url: string, body?: any, options ?: RequestOptionsArgs): Observable<Response> {
    return this.prepareRequest({url: url, body: body, method: RequestMethod.Delete}, options);
  }

  patch(url: string, body?: any, options?: RequestOptionsArgs): Observable<Response> {
    return this.prepareRequest({url: url, body: body, method: RequestMethod.Patch}, options);
  }

  intercept(observable: Observable<Response>): Observable<Response> {
    observable = this.successIntercept.reduce((pv, cv) => pv.map(cv), observable);
    observable = this.errorIntercept.reduce((pv, cv) => pv.catch(cv), observable);

    return observable;
  }

}
