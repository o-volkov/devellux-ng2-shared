import { NgModule } from "@angular/core";
import { EssayConstantsService } from "./services/essay-constants.service";
import { EssayConstantsPipe } from "./pipes/essay-constants.pipe";
export var DevelluxEssayModule = (function () {
    function DevelluxEssayModule() {
    }
    DevelluxEssayModule.forRoot = function () {
        return {
            ngModule: DevelluxEssayModule,
            providers: [EssayConstantsService]
        };
    };
    DevelluxEssayModule.decorators = [
        { type: NgModule, args: [{
                    declarations: [
                        EssayConstantsPipe,
                    ],
                    exports: [
                        EssayConstantsPipe,
                    ]
                },] },
    ];
    DevelluxEssayModule.ctorParameters = [];
    return DevelluxEssayModule;
}());
export * from "./pipes/essay-constants.pipe";
export * from "./services/essay-constants.service";
//# sourceMappingURL=essay.module.js.map