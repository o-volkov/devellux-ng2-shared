import { Injectable } from '@angular/core';
export var JwtHelper = (function () {
    function JwtHelper() {
    }
    JwtHelper.urlBase64Decode = function (str) {
        var output = str.replace(/-/g, '+').replace(/_/g, '/');
        switch (output.length % 4) {
            case 0:
                {
                    break;
                }
            case 2:
                {
                    output += '==';
                    break;
                }
            case 3:
                {
                    output += '=';
                    break;
                }
            default:
                {
                    throw 'Illegal base64url string!';
                }
        }
        return decodeURIComponent(escape(window.atob(output)));
    };
    JwtHelper.decodeToken = function (token) {
        var parts = token.split('.');
        if (parts.length !== 3) {
            throw new Error('JWT must have 3 parts');
        }
        var decoded = JwtHelper.urlBase64Decode(parts[1]);
        if (!decoded) {
            throw new Error('Cannot decode the token');
        }
        return JSON.parse(decoded);
    };
    JwtHelper.getTokenExpirationDate = function (token) {
        var decoded;
        decoded = JwtHelper.decodeToken(token);
        if (typeof decoded.exp === 'undefined') {
            return null;
        }
        var date = new Date(0);
        date.setUTCSeconds(decoded.exp);
        return date;
    };
    JwtHelper.isTokenExpired = function (token, offsetSeconds) {
        var date = JwtHelper.getTokenExpirationDate(token);
        offsetSeconds = offsetSeconds || 0;
        if (date === null) {
            return false;
        }
        return !(date.valueOf() > (new Date().valueOf() + (offsetSeconds * 1000)));
    };
    JwtHelper.decorators = [
        { type: Injectable },
    ];
    JwtHelper.ctorParameters = [];
    return JwtHelper;
}());
//# sourceMappingURL=jwt.js.map