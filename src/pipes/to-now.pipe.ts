import { Pipe, PipeTransform } from '@angular/core';
import * as moment from 'moment';

@Pipe({name: 'toNow'})
export class ToNowPipe implements PipeTransform {
  transform(date) {
    // noinspection TypeScriptValidateTypes
    return moment(moment.utc(date).toDate()).toNow(true);
  }
}
