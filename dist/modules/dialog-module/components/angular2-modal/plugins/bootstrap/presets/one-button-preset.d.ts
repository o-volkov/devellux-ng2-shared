import { ResolvedReflectiveProvider } from '@angular/core';
import { Modal, FluentAssignMethod } from '../../../../../components/angular2-modal';
import { MessageModalPresetBuilder, MessageModalPreset } from './message-modal-preset';
export interface OneButtonPreset extends MessageModalPreset {
    okBtn: string;
    okBtnClass: string;
}
export declare class OneButtonPresetBuilder extends MessageModalPresetBuilder<OneButtonPreset> {
    okBtn: FluentAssignMethod<string, this>;
    okBtnClass: FluentAssignMethod<string, this>;
    constructor(modal: Modal, defaultValues?: OneButtonPreset);
    $$beforeOpen(config: OneButtonPreset): ResolvedReflectiveProvider[];
}
