var __extends = (this && this.__extends) || function (d, b) {
    for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p];
    function __() { this.constructor = d; }
    d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
};
import { Component, Input, forwardRef } from "@angular/core";
import { NG_VALUE_ACCESSOR } from "@angular/forms";
import { CustomNgInput } from "../custom-ng2-input";
var styles = ".dropdown {\n  display: flex;\n  flex-direction: column;\n  width: 100%;\n  position: relative;\n}\n.dropdown .dropdown__menu {\n  display: none;\n  width: 100%;;\n  overflow-y: scroll;\n  margin: 0;\n  padding: 0;\n  position: absolute;\n  top: 43px;\n  left: 0;\n  z-index: 1000;\n  content: \"\";\n  background-color: #eceff4;\n  border-color: #8493a8;\n  border-style: solid;\n  border-width: 0 1px 1px 1px;\n  border-radius: 0 0 5px 5px;\n}\n.dropdown .dropdown-toggle,\n.dropdown .dropdown__item {\n  display: block;\n  width: 100%;\n  padding: 12px 30px 12px 15px;\n  font-size: 15px;\n  line-height: 18px;\n  font-weight: 300;\n  cursor: pointer;\n}\n.dropdown .dropdown-toggle {\n  color: #8493A8;\n  background-color: #eceff4;\n  background-repeat: no-repeat;\n  background-size: 12px 8px;\n  background-position: right 13px top 50%;\n  border: 1px solid #cad3df;\n  border-radius: 5px;\n  outline: none;\n  text-align: left;\n  white-space: nowrap;\n  overflow: hidden;\n  text-overflow: ellipsis;\n}\n.dropdown .dropdown-toggle.default {\n  color: #8493a8;\n}\n.dropdown .dropdown__item {\n  color: #677991;\n  transition: color 0.2s, background-color 0.2s;\n  overflow: hidden;\n  text-overflow: ellipsis;\n  white-space: nowrap;\n}\n.dropdown.isopen .dropdown-toggle, .dropdown.open .dropdown-toggle {\n  border-color: #8493a8;\n  border-bottom-color: #eceff4;\n  border-bottom-right-radius: 0;\n  border-bottom-left-radius: 0;\n}\n.dropdown.isopen .dropdown__menu, .dropdown.open .dropdown__menu {\n  display: block;\n  list-style: none;\n}\n.dropdown.isopen .dropdown__item:hover, .dropdown.isopen .dropdown__item.active, .dropdown.open .dropdown__item:hover, .dropdown.open .dropdown__item.active {\n  color: #fff;\n  background-color: #8493a8;\n}\n.dropdown.required:after {\n  position: absolute;\n  top: 6px;\n  right: 6px;\n  z-index: 1000;\n  display: block;\n  width: 7px;\n  height: 7px;\n  background-size: 7px 7px;\n  background-repeat: no-repeat;\n  content: \"\";\n}\n\n:host.ng-dirty.ng-valid:not(.no-validation) .dropdown-toggle {\n\tborder-color: #04be5b !important;\n}\n\n:host.ng-dirty.ng-invalid:not(.no-validation) .dropdown-toggle {\n\tborder-color: #ef4773 !important;\n}\n\n\n:host-context(.dropdown-file-uploaded) .dropdown .dropdown-toggle,\n:host-context(.dropdown-file-uploaded.ng-dirty.ng-invalid:not(.no-validation)) .dropdown .dropdown-toggle,\n:host-context(.dropdown-file-uploaded.ng-dirty.ng-valid:not(.no-validation)) .dropdown .dropdown-toggle {\n  background-color: #fff;\n  border-color: #fff !important;\n  border-radius: 0;\n}\n\n:host-context(.dropdown-file-uploaded) .dropdown.open .dropdown-toggle,\n:host-context(.dropdown-file-uploaded.ng-dirty.ng-invalid:not(.no-validation)) .dropdown.open .dropdown-toggle,\n:host-context(.dropdown-file-uploaded.ng-dirty.ng-valid:not(.no-validation)) .dropdown.open .dropdown-toggle {\n  border: 1px solid #cad3df !important;\n  border-bottom-color: #fff !important;\n}\n\n:host-context(.dropdown-file-uploaded) .dropdown.open .dropdown-toggle {\n  border-color: #cad3df;\n}\n\n:host-context(.dropdown-file-uploaded) .dropdown.open .dropdown__menu {\n  border-radius: 0;\n\tborder-color: #cad3df;\n}\n\n:host-context(.dropdown-file-uploaded) .dropdown.open .dropdown__item {\n  background-color: #fff;\n  color: #627289;\n}\n\n:host-context(.dropdown-file-uploaded) .dropdown.open .dropdown__item:hover {\n  background-color: #eceff4;\n  color: #627289;\n}\n\n";
var CUSTOM_INPUT_CONTROL_VALUE_ACCESSOR = {
    provide: NG_VALUE_ACCESSOR,
    useExisting: forwardRef(function () { return DropdownSelectorComponent; }),
    multi: true
};
export var DropdownSelectorComponent = (function (_super) {
    __extends(DropdownSelectorComponent, _super);
    function DropdownSelectorComponent() {
        _super.apply(this, arguments);
        this.options = [];
        this.disabled = false;
        this.titleSelected = false;
    }
    DropdownSelectorComponent.prototype.updateViewValue = function (v) {
        var item = this.options.filter(function (option) { return option.value === v; }).pop();
        if (item) {
            this.valueTitle = item.title;
            this.titleSelected = true;
        }
    };
    DropdownSelectorComponent.prototype.selectOption = function (option) {
        this.value = option.value;
    };
    DropdownSelectorComponent.prototype.onTouched = function () {
        this.touched();
    };
    DropdownSelectorComponent.decorators = [
        { type: Component, args: [{
                    selector: 'dropdown-select',
                    template: "<div class=\"dropdown\" dropdown (onToggle)=\"onTouched($event)\" >\n    <button type=\"button\" class=\"dropdown-toggle\" dropdownToggle [disabled]=\"disabled\" [class.selected]=\"titleSelected\"> \n    <ng-content *ngIf=\"!valueTitle\"></ng-content>\n      {{valueTitle}}\n    </button>\n    <ul class=\"dropdown__menu\" dropdownMenu>\n      <li  *ngFor=\"let option of options\" (click)=\"selectOption(option)\">\n        <span class=\"dropdown__item\">{{option.title}}</span>\n      </li>\n    </ul>\n </div>",
                    providers: [CUSTOM_INPUT_CONTROL_VALUE_ACCESSOR],
                    styles: ["\n    :host{\n      flex: 1;\n      margin: 0;\n    }\n    :host:first-child {\n      margin-right: 15px;\n    }\n  ", styles],
                },] },
    ];
    DropdownSelectorComponent.ctorParameters = [];
    DropdownSelectorComponent.propDecorators = {
        'options': [{ type: Input },],
        'disabled': [{ type: Input },],
    };
    return DropdownSelectorComponent;
}(CustomNgInput));
//# sourceMappingURL=dropdown.component.js.map