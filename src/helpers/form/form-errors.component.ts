import {Component, OnInit, Input, SkipSelf, Host, Optional} from "@angular/core";
import {FormControl, ControlContainer} from "@angular/forms";

/**
 * <form-errors [control]="form.controls['email']" [errors]="{required: 'Email is required'}"></form-errors>
 * <form-errors control="email" [errors]="{required: 'Email is required'}"></form-errors>
 */
@Component({
  selector: 'form-errors',
  template: `
    <div *ngIf="!control.pristine && !control.valid && control.touched">
      <template ngFor let-key [ngForOf]="control.errors | keys">
          <div class="auth-cmp__input-error" role="alert">
              {{ERRORS[key] || control.errors[key]}}
          </div>
      </template>
    </div>
  `,
})
export class FormErrorsComponent implements OnInit {
  @Input() control; // FormControl|string
  @Input() errors:any = {};

  ERRORS = {
    'AUTH.EMAIL_ALREADY_TAKEN': 'EMAIL ALREADY TAKEN',
    'AUTH.EMAIL_NOT_FOUND': 'EMAIL IS NOT REGISTERED',
    'AUTH.WRONG_PASSWORD': 'PASSWORD DOES NOT MATCH',
    isEmail: 'INVALID EMAIL ADDRESS',
    required: 'REQUIRED',
    minlength: 'PASSWORD IS TOO SHORT',
    isMatch: 'PASSWORD DO NOT MATCH',
    pattern: 'NOT ALLOWED SYMBOLS',
  };

  constructor(@Optional() @Host() @SkipSelf() private parent:ControlContainer) {
  }

  ngOnInit() {
    if (typeof this.errors === "object") {
      Object.assign(this.ERRORS, this.errors);
    }
    if (!(this.control instanceof FormControl)) {
      this.control = this.parent['form'].controls[this.control];
    }
  }
}
