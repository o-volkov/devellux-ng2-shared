import { PipeTransform } from '@angular/core';
export declare class UtcToLocal implements PipeTransform {
    transform(date: any): Date;
}
