import { Pipe, PipeTransform } from '@angular/core';
import * as moment from 'moment';

@Pipe({
  name: 'uploadDate'
})

export class UploadDatePipe implements PipeTransform {
  transform(date): any {

    return moment(date).format('D MMM h:mm A');
  }
}
