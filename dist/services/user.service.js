import { Injectable, EventEmitter } from "@angular/core";
import { JwtHelper } from "../helpers/jwt/jwt";
import { getCookie, setCookie, deleteCookie } from "../helpers/cookie/cookie";
export var User = (function () {
    function User() {
        var _this = this;
        this.status$ = new EventEmitter();
        this.data$ = new EventEmitter();
        window.addEventListener('storage', function (event) {
            if (event.key === 'data' && event.newValue === null) {
                console.log('succesful logout');
                _this.logout();
            }
        });
    }
    User.prototype.isAuthenticated = function () {
        return !!this.token;
    };
    User.prototype.getId = function () {
        if (this.data) {
            return this.data._id;
        }
        return '';
    };
    Object.defineProperty(User.prototype, "data", {
        get: function () {
            var _this = this;
            if (!this.isAuthenticated() && this.data_) {
                Object.keys(this.data_).forEach(function (key) {
                    delete _this.data_[key];
                });
                this.data_ = null;
            }
            if (this.data_) {
                return this.data_;
            }
            if (localStorage.getItem('data')) {
                this.data_ = JSON.parse(localStorage.getItem('data'));
                return this.data_;
            }
            if (this.token) {
                this.data_ = JwtHelper.decodeToken(this.token_);
                return this.data_;
            }
            return {};
        },
        set: function (value) {
            if (!this.isAuthenticated()) {
                return;
            }
            if (value !== undefined && value !== null) {
                for (var nextKey in value) {
                    if (value.hasOwnProperty(nextKey)) {
                        this.data_[nextKey] = value[nextKey];
                    }
                }
            }
            try {
                localStorage.setItem('data', JSON.stringify(this.data_));
            }
            catch (er) {
                console.error('To hell with him', er);
            }
            this.data$.emit(this.data_);
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(User.prototype, "token", {
        get: function () {
            if (!this.token_) {
                this.token_ = getCookie('token');
            }
            return this.token_;
        },
        set: function (value) {
            this.token_ = value;
            try {
                setCookie('token', value, { expires: 3600 * 24 * 30 });
            }
            catch (er) {
                console.error('To hell with him', er);
            }
            this.status$.emit(this.isAuthenticated());
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(User.prototype, "refreshToken", {
        get: function () {
            if (!this.refreshToken_) {
            }
            return this.refreshToken_;
        },
        enumerable: true,
        configurable: true
    });
    User.prototype.logout = function () {
        deleteCookie('token');
        localStorage.removeItem('data');
        this.token_ = null;
        delete this.token_;
        this.data_ = null;
        delete this.data_;
        this.status$.emit(false);
    };
    User.decorators = [
        { type: Injectable },
    ];
    User.ctorParameters = [];
    return User;
}());
//# sourceMappingURL=user.service.js.map