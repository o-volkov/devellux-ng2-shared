import * as moment from 'moment';

export const MIN_YEAR = 1970;
export const MAX_YEAR = 2100;

export class CalendarDate {
  get hours() {
    return this._hours;
  }

  set hours(hours) {
    if (hours > 0) {
      this._hours = hours;
    } else {
      this._hours = 1;
    }
  }

  get minutes() {
    return this._minutes;
  }

  set minutes(minutes) {
    if (minutes <= 59 && minutes > 0) {
      this._minutes = minutes;
    } else {
      this._minutes = 0;
    }
  }

  constructor(
    public year?: number,
    public month?: number,
    public day?: number,
    public weekDay?: number,
    public _hours?: number,
    public _minutes?: number,
    public seconds?: number,
    public ampm?: string,
    public disabled?: boolean
  ) {}

  static fromDate(date: Date): CalendarDate {
    let cdt = new CalendarDate(
      date.getFullYear(),
      date.getMonth(),
      date.getDate(),
      date.getDay(),
      date.getHours() > 12 ? date.getHours() - 12 : date.getHours(),
      date.getMinutes(),
      date.getSeconds(),
      date.getHours() > 12 ? 'PM' : 'AM'
    );

    cdt.disabled = cdt.year < MIN_YEAR || cdt.year > MAX_YEAR;

    return cdt;
  }

  public equalDate(cdt: CalendarDate) {
    return this.year === cdt.year &&
      this.month === cdt.month &&
      this.day === cdt.day;
  }

  public clone(): CalendarDate {
    return new CalendarDate(
      this.year,
      this.month,
      this.day,
      this.weekDay,
      this.hours,
      this.minutes,
      this.seconds,
      this.ampm,
      this.disabled
    );
  }

  public toDate(): Date {
    return moment(this.year + '-' + (this.month + 1) + '-' + this.day + ' ' +
      this.hours + ':' + this.minutes + ':' + this.seconds + this.ampm,
      'YYYY-M-D HH:mm:ssA').toDate();
  }

  get lastWeekDayOfMonth(): number {
    return (new Date(this.year, this.month + 1, 0)).getDay();
  }

  get lastDayOfMonth(): number {
    return (new Date(this.year, this.month + 1, 0)).getDate();
  }

  get firstWeekDay(): number {
    return (new Date(this.year, this.month, 1)).getDay();
  }

  get now(): boolean {
    let now = new Date();

    return this.year == now.getFullYear() &&
      this.month == now.getMonth() &&
      this.day == now.getDate();
  }

  public nextHour() {
    if (this.hours < 12) {
      this.hours++;
    } else {
      this.hours = 1;
      if (this.ampm === 'AM') {
        this.ampm = 'PM';
      } else {
        this.ampm = 'AM';
      }
    }
  }

  public prevHour() {
    if (this.hours > 1) {
      this.hours--;
    } else {
      this.hours = 12;
      if (this.ampm === 'PM') {
        this.ampm = 'AM';
      } else {
        this.ampm = 'PM';
      }
    }
  }

  public nextMinute() {
    if (this.minutes < 59) {
      this.minutes++;
    } else {
      this.minutes = 0;
      this.nextHour();
    }
  }

  public prevMinute() {
    if (this.minutes > 0) {
      this.minutes--;
    } else {
      this.minutes = 59;
      this.prevHour();
    }
  }
}
