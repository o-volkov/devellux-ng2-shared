import { Component, Input } from "@angular/core";
export var CountryComponent = (function () {
    function CountryComponent() {
    }
    CountryComponent.decorators = [
        { type: Component, args: [{
                    selector: 'country-cmp',
                    template: "<span *ngIf=\"short\" class=\"{{'flag-icon flag-icon-' + short.toLowerCase()}}\" style=\"margin-right: 5px\">\n              </span><span class=\"country-name weight-400\">{{short || '-'}}</span>"
                },] },
    ];
    CountryComponent.ctorParameters = [];
    CountryComponent.propDecorators = {
        'short': [{ type: Input },],
    };
    return CountryComponent;
}());
//# sourceMappingURL=country.component.js.map