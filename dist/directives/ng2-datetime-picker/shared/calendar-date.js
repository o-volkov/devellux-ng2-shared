import * as moment from 'moment';
export var MIN_YEAR = 1970;
export var MAX_YEAR = 2100;
export var CalendarDate = (function () {
    function CalendarDate(year, month, day, weekDay, _hours, _minutes, seconds, ampm, disabled) {
        this.year = year;
        this.month = month;
        this.day = day;
        this.weekDay = weekDay;
        this._hours = _hours;
        this._minutes = _minutes;
        this.seconds = seconds;
        this.ampm = ampm;
        this.disabled = disabled;
    }
    Object.defineProperty(CalendarDate.prototype, "hours", {
        get: function () {
            return this._hours;
        },
        set: function (hours) {
            if (hours > 0) {
                this._hours = hours;
            }
            else {
                this._hours = 1;
            }
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(CalendarDate.prototype, "minutes", {
        get: function () {
            return this._minutes;
        },
        set: function (minutes) {
            if (minutes <= 59 && minutes > 0) {
                this._minutes = minutes;
            }
            else {
                this._minutes = 0;
            }
        },
        enumerable: true,
        configurable: true
    });
    CalendarDate.fromDate = function (date) {
        var cdt = new CalendarDate(date.getFullYear(), date.getMonth(), date.getDate(), date.getDay(), date.getHours() > 12 ? date.getHours() - 12 : date.getHours(), date.getMinutes(), date.getSeconds(), date.getHours() > 12 ? 'PM' : 'AM');
        cdt.disabled = cdt.year < MIN_YEAR || cdt.year > MAX_YEAR;
        return cdt;
    };
    CalendarDate.prototype.equalDate = function (cdt) {
        return this.year === cdt.year &&
            this.month === cdt.month &&
            this.day === cdt.day;
    };
    CalendarDate.prototype.clone = function () {
        return new CalendarDate(this.year, this.month, this.day, this.weekDay, this.hours, this.minutes, this.seconds, this.ampm, this.disabled);
    };
    CalendarDate.prototype.toDate = function () {
        return moment(this.year + '-' + (this.month + 1) + '-' + this.day + ' ' +
            this.hours + ':' + this.minutes + ':' + this.seconds + this.ampm, 'YYYY-M-D HH:mm:ssA').toDate();
    };
    Object.defineProperty(CalendarDate.prototype, "lastWeekDayOfMonth", {
        get: function () {
            return (new Date(this.year, this.month + 1, 0)).getDay();
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(CalendarDate.prototype, "lastDayOfMonth", {
        get: function () {
            return (new Date(this.year, this.month + 1, 0)).getDate();
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(CalendarDate.prototype, "firstWeekDay", {
        get: function () {
            return (new Date(this.year, this.month, 1)).getDay();
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(CalendarDate.prototype, "now", {
        get: function () {
            var now = new Date();
            return this.year == now.getFullYear() &&
                this.month == now.getMonth() &&
                this.day == now.getDate();
        },
        enumerable: true,
        configurable: true
    });
    CalendarDate.prototype.nextHour = function () {
        if (this.hours < 12) {
            this.hours++;
        }
        else {
            this.hours = 1;
            if (this.ampm === 'AM') {
                this.ampm = 'PM';
            }
            else {
                this.ampm = 'AM';
            }
        }
    };
    CalendarDate.prototype.prevHour = function () {
        if (this.hours > 1) {
            this.hours--;
        }
        else {
            this.hours = 12;
            if (this.ampm === 'PM') {
                this.ampm = 'AM';
            }
            else {
                this.ampm = 'PM';
            }
        }
    };
    CalendarDate.prototype.nextMinute = function () {
        if (this.minutes < 59) {
            this.minutes++;
        }
        else {
            this.minutes = 0;
            this.nextHour();
        }
    };
    CalendarDate.prototype.prevMinute = function () {
        if (this.minutes > 0) {
            this.minutes--;
        }
        else {
            this.minutes = 59;
            this.prevHour();
        }
    };
    return CalendarDate;
}());
//# sourceMappingURL=calendar-date.js.map