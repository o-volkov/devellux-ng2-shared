export declare function extend<T>(m1: any, m2: any): T;
export declare function arrayUnion<T>(arr1: any[], arr2: any[]): T[];
export declare function supportsKey(keyCode: number, config: Array<number>): boolean;
export declare function toStyleString(obj: any | CSSStyleDeclaration): string;
export declare class PromiseCompleter<R> {
    promise: Promise<R>;
    resolve: (value?: R | PromiseLike<R>) => void;
    reject: (error?: any, stackTrace?: string) => void;
    constructor();
}
export interface Class<T> {
    new (...args: any[]): T;
}
export declare type Maybe<T> = T | Promise<T>;
export declare function noop(): void;
