import { Pipe } from '@angular/core';
export var CutPipe = (function () {
    function CutPipe() {
    }
    CutPipe.prototype.transform = function (value, arg1) {
        if (arg1 && arg1 > 3 && value.length > arg1) {
            return value = value.slice(0, arg1 - 3) + '...';
        }
        ;
        return value;
    };
    CutPipe.decorators = [
        { type: Pipe, args: [{
                    name: 'cutPipe'
                },] },
    ];
    CutPipe.ctorParameters = [];
    return CutPipe;
}());
//# sourceMappingURL=cut.pipe.js.map