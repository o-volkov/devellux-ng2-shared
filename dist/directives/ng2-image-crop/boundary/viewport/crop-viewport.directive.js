import { Directive, Input, HostBinding, ElementRef, Renderer } from '@angular/core';
export var CropBoundaryViewportDirective = (function () {
    function CropBoundaryViewportDirective(element, renderer) {
        this.element = element;
        this.renderer = renderer;
    }
    CropBoundaryViewportDirective.prototype.getBoundingClientRect = function () {
        return this.element.nativeElement.getBoundingClientRect();
    };
    CropBoundaryViewportDirective.decorators = [
        { type: Directive, args: [{
                    selector: '[crop-boundary-viewport]',
                },] },
    ];
    CropBoundaryViewportDirective.ctorParameters = [
        { type: ElementRef, },
        { type: Renderer, },
    ];
    CropBoundaryViewportDirective.propDecorators = {
        'width': [{ type: HostBinding, args: ['style.width.px',] }, { type: Input },],
        'height': [{ type: HostBinding, args: ['style.height.px',] }, { type: Input },],
    };
    return CropBoundaryViewportDirective;
}());
//# sourceMappingURL=crop-viewport.directive.js.map