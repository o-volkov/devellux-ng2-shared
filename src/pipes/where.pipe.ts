import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'where'
})
export class WherePipe implements PipeTransform {


  /**
   * Support a function or a value or the shorthand ['key', value] like the lodash shorthand.
   */
  transform(input: any, fn: any): any {

    if (!Array.isArray(input)) {
      return input;
    }

    if (typeof fn === 'function') {
      return input.filter(fn);
    } else if (Array.isArray((fn))) {
      const [key, value] = fn;
      return input.filter((item: any) => getProperty(item, key) === value);
    } else if (fn) {
      return input.filter((item: any) => item === fn);
    } else {
      return input;
    }

  }
}

export function getProperty(value: Object, key: string): Array<any> {

  const keys = key.split('.');
  let result = value[keys.shift()];
  while (keys.length && (result = result[keys.shift()])) {
  }
  return result;

}
