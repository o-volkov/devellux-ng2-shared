import { Injectable, EventEmitter } from '@angular/core';
import { Observable } from 'rxjs/Observable';
import { SSEService } from '../../services/see.service';
import { HttpClient } from "../../helpers/http/client";
import { commonResponseHandler } from "../../helpers/common/common";
export var PresenceService = (function () {
    function PresenceService(sse, http) {
        var _this = this;
        this.sse = sse;
        this.http = http;
        this.cache = {};
        this.userStatusChangeEE = new EventEmitter();
        this.userStatusChange$ = this.userStatusChangeEE.asObservable();
        this.sse.connect('status')
            .subscribe(function (res) {
            _this.changeUserStatus(res._id, res.online);
        });
    }
    PresenceService.prototype.isUserOnline = function (userId) {
        return this.getUserStatus(userId);
    };
    PresenceService.prototype.changeUserStatus = function (id, status) {
        this.cache[id] = status;
        this.userStatusChangeEE.emit({
            id: id, status: status
        });
    };
    PresenceService.prototype.getUserStatus = function (userId) {
        var _this = this;
        if (userId in this.cache) {
            return Observable.of(this.cache[userId]);
        }
        return this.http.post('/status/subscribe', { _id: userId })
            .map(commonResponseHandler)
            .map(function (res) { return res.online; })
            .do(function (status) { return _this.changeUserStatus(userId, status); });
    };
    PresenceService.prototype.randomStatusChange = function () {
        var id = Object.keys(this.cache)[Math.floor(Math.random() * Object.keys(this.cache).length)];
        if (id) {
            var status_1 = Math.random() >= 0.5;
            this.changeUserStatus(id, status_1);
        }
    };
    PresenceService.decorators = [
        { type: Injectable },
    ];
    PresenceService.ctorParameters = [
        { type: SSEService, },
        { type: HttpClient, },
    ];
    return PresenceService;
}());
//# sourceMappingURL=presence.service.js.map