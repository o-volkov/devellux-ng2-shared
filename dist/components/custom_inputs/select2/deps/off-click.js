import { Directive, HostListener, Input } from '@angular/core';
export var OffClickDirective = (function () {
    function OffClickDirective() {
    }
    OffClickDirective.prototype.onClick = function ($event) {
        $event.stopPropagation();
    };
    OffClickDirective.prototype.ngOnInit = function () {
        var _this = this;
        setTimeout(function () {
            document.addEventListener('click', _this.offClickHandler);
        }, 0);
    };
    OffClickDirective.prototype.ngOnDestroy = function () {
        document.removeEventListener('click', this.offClickHandler);
    };
    OffClickDirective.decorators = [
        { type: Directive, args: [{
                    selector: '[offClick]'
                },] },
    ];
    OffClickDirective.ctorParameters = [];
    OffClickDirective.propDecorators = {
        'offClickHandler': [{ type: Input, args: ['offClick',] },],
        'onClick': [{ type: HostListener, args: ['click', ['$event'],] },],
    };
    return OffClickDirective;
}());
//# sourceMappingURL=off-click.js.map