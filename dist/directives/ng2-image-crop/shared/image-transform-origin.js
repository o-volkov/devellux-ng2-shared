import * as Utils from './utils';
export var ImageTransformOrigin = (function () {
    function ImageTransformOrigin(el) {
        if (!el || !el.style[Utils.CSS_TRANS_ORG]) {
            this.x = 0;
            this.y = 0;
        }
        else {
            var css = el.style[Utils.CSS_TRANS_ORG].split(' ');
            this.x = parseFloat(css[0]);
            this.y = parseFloat(css[1]);
        }
    }
    ImageTransformOrigin.prototype.toString = function () {
        return this.x + "px " + this.y + "px";
    };
    ;
    return ImageTransformOrigin;
}());
//# sourceMappingURL=image-transform-origin.js.map