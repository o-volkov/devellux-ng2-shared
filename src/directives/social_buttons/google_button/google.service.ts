import { Injectable, OpaqueToken, Inject, Optional } from '@angular/core';

declare var gapi: any;
export let GOOGLE_APP_ID = new OpaqueToken('GoogleAppId');

@Injectable()
export class GoogleService {
  private googleAccessToken: any;
  private googleAuthInstance: any;

  constructor(@Optional() @Inject(GOOGLE_APP_ID) private appId: string) {
    if (appId) {
      // noinspection TypeScriptUnresolvedVariable
      window['googleAsyncInit'] = () => {
        gapi.load('auth2', () => {
          // Retrieve the singleton for the GoogleAuth library and set up the client.

          this.googleAuthInstance = gapi.auth2.init({
            client_id: appId,
            cookiepolicy: 'single_host_origin',
            // Request scopes in addition to 'profile' and 'email'
            // scope: 'additional_scope'
          });
        });
      };


      (function (d, s, id) {
        var js, fjs = d.getElementsByTagName(s)[0];
        if (d.getElementById(id)) {
          return;
        }
        js = d.createElement(s);
        js.id = id;
        js.async = true;
        js.src = 'https://apis.google.com/js/platform.js?onload=googleAsyncInit';
        fjs.parentNode.insertBefore(js, fjs);
      }(document, 'script', 'google-jssdk'));
    }


  }

  setGoogleToken(token) {
    this.googleAccessToken = token;
  }

  getToken(): Promise<any> {
    return new Promise<string>((resolve, reject) => {
      if (!this.appId) {
        reject('no GOOGLE_APP_ID');
      }
      // this.googleAuthInstance.grantOfflineAccess({'redirect_uri': 'postmessage'}).then((res)=>console.log(res));
      this.googleAuthInstance.signIn().then((result: any) => {
        this.setGoogleToken(result.getAuthResponse().id_token);
        resolve(result.getAuthResponse().id_token);
      }, () => {
        reject('login Google cancelled');
      });
    });
  }
}
