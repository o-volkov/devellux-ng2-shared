import { Injectable, Inject, OpaqueToken } from "@angular/core";
import { of } from "rxjs/observable/of";
import "rxjs/add/operator/map";
import "rxjs/add/operator/catch";
import { fromEvent } from "rxjs/observable/fromEvent";
import { User } from "./user.service";
export var SSE_URL_TOKEN = new OpaqueToken('sseUrl');
export var SSEService = (function () {
    function SSEService(user, sseUrl) {
        var _this = this;
        this.user = user;
        this.sseUrl = sseUrl;
        this.createSource();
        this.user.status$.subscribe(function () { return _this.createSource(); });
    }
    SSEService.prototype.connect = function (eventName) {
        if (!Object.prototype.hasOwnProperty.call(this.observables, eventName)) {
            this.observables[eventName] = fromEvent(this.source, eventName)
                .map(function (e) { return JSON.parse(e.data); })
                .catch(function (er) {
                console.error(er);
                return of({});
            });
        }
        return this.observables[eventName];
    };
    SSEService.prototype.createSource = function () {
        if (this.source) {
            this.source.close();
        }
        this.clearObservables();
        if (this.user && this.user.isAuthenticated()) {
            this.source = new EventSource(this.sseUrl + "?auth_token=" + this.user.token, { withCredentials: true });
        }
        else {
            this.source = new EventSource("" + this.sseUrl);
        }
    };
    SSEService.prototype.clearObservables = function () {
        this.observables = {};
    };
    SSEService.decorators = [
        { type: Injectable },
    ];
    SSEService.ctorParameters = [
        { type: User, },
        { type: undefined, decorators: [{ type: Inject, args: [SSE_URL_TOKEN,] },] },
    ];
    return SSEService;
}());
//# sourceMappingURL=see.service.js.map