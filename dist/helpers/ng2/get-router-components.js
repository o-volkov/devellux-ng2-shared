export function getRouterComponents(routes) {
    function walkRoute(r, cmpList) {
        if (cmpList === void 0) { cmpList = new Set(); }
        r.forEach(function (route) {
            if (route.component) {
                cmpList.add(route.component);
            }
            if (route.children) {
                walkRoute(route.children, cmpList);
            }
        });
        return cmpList;
    }
    return Array.from(walkRoute(routes));
}
//# sourceMappingURL=get-router-components.js.map