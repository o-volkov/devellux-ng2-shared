import {Headers, RequestOptions, RequestOptionsArgs} from "@angular/http";
import {getCookie} from "../../helpers/cookie/cookie";


export class AppRequestOptions extends RequestOptions {
  headers = new Headers({
    'Authorization': `Bearer ${getCookie('token')}`,
    'Content-Type': 'application/json',
    'X-Frontend-Url': window.location.origin,
  });
  withCredentials = true;
  public baseUrl = '';

  private static isAbsoluteUrl(url:string):boolean {
    var absolutePattern = /^https?:\/\//i;

    return absolutePattern.test(url);
  }

  private prepareUrl(url:string):string {
    url = AppRequestOptions.isAbsoluteUrl(url) ? url : this.baseUrl + '/' + url;

    return url.replace(/([^:]\/)\/+/g, '$1');
  }

  merge(options?:RequestOptionsArgs):AppRequestOptions {
    let result = new AppRequestOptions(options);
    result.baseUrl = this.baseUrl;

    result.url = this.prepareUrl(result.url);

    return result;
  }
}
