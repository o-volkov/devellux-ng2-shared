import {Component, Input, forwardRef} from "@angular/core";
import {NG_VALUE_ACCESSOR, ControlValueAccessor} from "@angular/forms";
import {CustomNgInput} from "../custom-ng2-input";

const styles = `.dropdown {
  display: flex;
  flex-direction: column;
  width: 100%;
  position: relative;
}
.dropdown .dropdown__menu {
  display: none;
  width: 100%;;
  overflow-y: scroll;
  margin: 0;
  padding: 0;
  position: absolute;
  top: 43px;
  left: 0;
  z-index: 1000;
  content: "";
  background-color: #eceff4;
  border-color: #8493a8;
  border-style: solid;
  border-width: 0 1px 1px 1px;
  border-radius: 0 0 5px 5px;
}
.dropdown .dropdown-toggle,
.dropdown .dropdown__item {
  display: block;
  width: 100%;
  padding: 12px 30px 12px 15px;
  font-size: 15px;
  line-height: 18px;
  font-weight: 300;
  cursor: pointer;
}
.dropdown .dropdown-toggle {
  color: #8493A8;
  background-color: #eceff4;
  background-repeat: no-repeat;
  background-size: 12px 8px;
  background-position: right 13px top 50%;
  border: 1px solid #cad3df;
  border-radius: 5px;
  outline: none;
  text-align: left;
  white-space: nowrap;
  overflow: hidden;
  text-overflow: ellipsis;
}
.dropdown .dropdown-toggle.default {
  color: #8493a8;
}
.dropdown .dropdown__item {
  color: #677991;
  transition: color 0.2s, background-color 0.2s;
  overflow: hidden;
  text-overflow: ellipsis;
  white-space: nowrap;
}
.dropdown.isopen .dropdown-toggle, .dropdown.open .dropdown-toggle {
  border-color: #8493a8;
  border-bottom-color: #eceff4;
  border-bottom-right-radius: 0;
  border-bottom-left-radius: 0;
}
.dropdown.isopen .dropdown__menu, .dropdown.open .dropdown__menu {
  display: block;
  list-style: none;
}
.dropdown.isopen .dropdown__item:hover, .dropdown.isopen .dropdown__item.active, .dropdown.open .dropdown__item:hover, .dropdown.open .dropdown__item.active {
  color: #fff;
  background-color: #8493a8;
}
.dropdown.required:after {
  position: absolute;
  top: 6px;
  right: 6px;
  z-index: 1000;
  display: block;
  width: 7px;
  height: 7px;
  background-size: 7px 7px;
  background-repeat: no-repeat;
  content: "";
}

:host.ng-dirty.ng-valid:not(.no-validation) .dropdown-toggle {
	border-color: #04be5b !important;
}

:host.ng-dirty.ng-invalid:not(.no-validation) .dropdown-toggle {
	border-color: #ef4773 !important;
}


:host-context(.dropdown-file-uploaded) .dropdown .dropdown-toggle,
:host-context(.dropdown-file-uploaded.ng-dirty.ng-invalid:not(.no-validation)) .dropdown .dropdown-toggle,
:host-context(.dropdown-file-uploaded.ng-dirty.ng-valid:not(.no-validation)) .dropdown .dropdown-toggle {
  background-color: #fff;
  border-color: #fff !important;
  border-radius: 0;
}

:host-context(.dropdown-file-uploaded) .dropdown.open .dropdown-toggle,
:host-context(.dropdown-file-uploaded.ng-dirty.ng-invalid:not(.no-validation)) .dropdown.open .dropdown-toggle,
:host-context(.dropdown-file-uploaded.ng-dirty.ng-valid:not(.no-validation)) .dropdown.open .dropdown-toggle {
  border: 1px solid #cad3df !important;
  border-bottom-color: #fff !important;
}

:host-context(.dropdown-file-uploaded) .dropdown.open .dropdown-toggle {
  border-color: #cad3df;
}

:host-context(.dropdown-file-uploaded) .dropdown.open .dropdown__menu {
  border-radius: 0;
	border-color: #cad3df;
}

:host-context(.dropdown-file-uploaded) .dropdown.open .dropdown__item {
  background-color: #fff;
  color: #627289;
}

:host-context(.dropdown-file-uploaded) .dropdown.open .dropdown__item:hover {
  background-color: #eceff4;
  color: #627289;
}

`;



const CUSTOM_INPUT_CONTROL_VALUE_ACCESSOR = {
  provide: NG_VALUE_ACCESSOR,
  useExisting: forwardRef(() => DropdownSelectorComponent),
  multi: true
};

@Component({
  selector: 'dropdown-select',
  template: `<div class="dropdown" dropdown (onToggle)="onTouched($event)" >
    <button type="button" class="dropdown-toggle" dropdownToggle [disabled]="disabled" [class.selected]="titleSelected"> 
    <ng-content *ngIf="!valueTitle"></ng-content>
      {{valueTitle}}
    </button>
    <ul class="dropdown__menu" dropdownMenu>
      <li  *ngFor="let option of options" (click)="selectOption(option)">
        <span class="dropdown__item">{{option.title}}</span>
      </li>
    </ul>
 </div>`,

  providers: [CUSTOM_INPUT_CONTROL_VALUE_ACCESSOR],
  styles: [`
    :host{
      flex: 1;
      margin: 0;
    }
    :host:first-child {
      margin-right: 15px;
    }
  `, styles],
})
export class DropdownSelectorComponent extends CustomNgInput implements ControlValueAccessor {
  @Input() options:Option[] = [];
  @Input() disabled:boolean = false;
  valueTitle;
  titleSelected = false;


  updateViewValue(v) {
    let item = this.options.filter(option => option.value === v).pop();
    if (item) {
      this.valueTitle = item.title;
      this.titleSelected = true;
    }
  }

  selectOption(option) {
    this.value = option.value;
  }

  onTouched() {
    this.touched();
  }


}


export interface Option {
  title;
  value;
}
