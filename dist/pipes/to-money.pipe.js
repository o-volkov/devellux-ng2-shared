import { Pipe, Inject, LOCALE_ID } from "@angular/core";
import { CurrencyPipe } from "@angular/common";
export var ToMoneyPipe = (function () {
    function ToMoneyPipe(locale) {
        this.locale = locale;
        this.cp = new CurrencyPipe(this.locale);
    }
    ToMoneyPipe.prototype.transform = function (value) {
        if (Number.isInteger(parseInt(value))) {
            value = value / 100;
        }
        else {
            value = 0;
        }
        return this.cp.transform(value, 'USD', true, '1.2-2');
    };
    ToMoneyPipe.decorators = [
        { type: Pipe, args: [{ name: 'toMoney' },] },
    ];
    ToMoneyPipe.ctorParameters = [
        { type: undefined, decorators: [{ type: Inject, args: [LOCALE_ID,] },] },
    ];
    return ToMoneyPipe;
}());
//# sourceMappingURL=to-money.pipe.js.map