import {Injectable, EventEmitter} from "@angular/core";
import {Observable} from "rxjs/Observable";
import {User} from "./user.service";
import {HttpClient} from "../helpers/http/client";
import {commonResponseListHandler, commonErrorHandler} from "../helpers/common/common";


@Injectable()
export class WalletsService implements WalletsServiceInterface {
  private walletsEE:EventEmitter<Wallet[]> = new EventEmitter<Wallet[]>();
  wallets$ = this.walletsEE.asObservable();

  constructor(private httpClient:HttpClient, private user:User) {
    // При изменении статуса пользователя обновляем кошельки
    this.user.status$.subscribe(() => this.getWallets());
  }

  getWallets():Observable<Wallet[]> {
    let obs;
    if (this.user.isAuthenticated()) {
      obs = this.httpClient.get('billing/wallet')
        .map(commonResponseListHandler)
        .catch(commonErrorHandler)
      // todo some error handling here
      ;
    } else {
      obs = Observable.of([]);
    }
    return obs.do(wallets => this.walletsEE.emit(wallets));
  }
}

export interface WalletsServiceInterface {
  wallets$:Observable<Wallet[]>;
  getWallets():Observable<Wallet[]>;
}


export interface Wallet {
  balance:number;
  restrictions:Restrictions;
  _id:string;
  currency:'USD';
}
export interface Restrictions {
  refund:boolean;
  pay:boolean;
  deposit:boolean;
  accept:boolean;
  withdrawal:boolean;
}
