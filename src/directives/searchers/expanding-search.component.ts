import { Component, Output, EventEmitter, forwardRef } from '@angular/core';
import { FormControl, NG_VALUE_ACCESSOR } from '@angular/forms';
import { CustomNgInput } from '../../components/custom_inputs/custom-ng2-input';

@Component({

  selector: 'expanding-search-component',
  template: `<div class="search__container">
    <input type="text" class="form-input type__search" placeholder="Search"
     [formControl]="input" [class.not-empty]="input.value">
    
    <button class="search__clear button empty" type="button"
      *ngIf="input.value" (click)="clearInput()">
      <icon-renderer class="icon close-thin" name="close-thin"></icon-renderer>
    </button>
  </div>`,
  styles: [`
  
  :host {
    position: relative;
    display: inline-block;
    width: auto;
  }
  
  .form-input.ng-valid { 
    border-color: #cad3df;
  }
  
  .form-input.ng-valid:focus { 
    border-color: #54657e;
  }
  
  .seach__container {
    position: relative;
    width: 170px !important;
  }
  
  .form-input {
     width: 170px !important;
     transition: all .3s ease;
     z-index: 3;
  }
  
  .form-input:focus,
  .form-input.not-empty {
    width: 360px !important;
  }
  
  .form-input + .search__clear {
    position: absolute;
    right: 15px;
    top: calc(50% - 12px);
    z-index: 4;
    
    width: 20px;
    height: 20px;
    
    color: #fff;
    background-color: #cad3df;
    border-radius: 10px;	
  }
  
  .form-input + .search__clear:hover,
  .form-input + .search__clear:focus {
    background-color: #8493A8;
  }
  
  .icon.close-thin {
    padding: 5px;
  }
  
    
  `],
  providers: [
    {provide: NG_VALUE_ACCESSOR, useExisting: forwardRef(() => ExpandingSearchComponent), multi: true}
  ]
})
export class ExpandingSearchComponent extends CustomNgInput {
  delay: number = 500;

  @Output() valueChanged: EventEmitter<any> = new EventEmitter();

  input = new FormControl('');

  ngOnInit() {
    // todo  touched more than once, why?
    this.input.valueChanges
      .do(() => this.touched())
      .debounceTime(this.delay)
      .distinctUntilChanged()
      .subscribe(val => {
        this.value = val;
        this.valueChanged.emit(val);
      });
  }

  updateViewValue(value: any) {
    if (this.input) {
      this.input.patchValue(value, {onlySelf: true});
    }
  }

  clearInput() {
    this.input.reset('', {onlySelf : true});
  }
}
