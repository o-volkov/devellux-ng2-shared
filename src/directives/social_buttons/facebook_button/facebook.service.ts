import { Injectable, OpaqueToken, Inject, Optional, isDevMode } from '@angular/core';

declare var FB: any;

export let FACEBOOK_APP_ID = new OpaqueToken('FacebookAppId');

@Injectable()
export class FacebookService {
  private accessToken;

  constructor(@Optional() @Inject(FACEBOOK_APP_ID) private appId: string) {
    if (!appId && isDevMode()) {
      console.error('no FACEBOOK_APP_ID provided');
      return;
    }

    // noinspection TypeScriptUnresolvedVariable
    window['fbAsyncInit'] = () => {
      // Executed when the SDK is loaded
      FB.init({
        /*
         The app id of the web app;
         To register a new app visit Facebook App Dashboard
         ( https://developers.facebook.com/apps/ )
         */
        appId: appId,
        /*
         Adding a Channel File improves the performance
         of the javascript SDK, by addressing issues
         with cross-domain communication in certain browsers.
         */
        // channelUrl: 'app/channel.html',
        /*
         Set if you want to check the authentication status
         at the start up of the app
         */
        status: true,
        /*
         Enable cookies to allow the server to access
         the session
         */
        cookie: true,
        /* Parse XFBML */
        xfbml: true,
        version: 'v2.5'
      });

      FB.Event.subscribe('auth.statusChange', (res: any) => {
        if (res.status === 'connected') {
          this.setToken(res.authResponse.accessToken);
          // FB.api('/me', function (response) {
          //  console.log(response);
          // });
        } else {
          /*
           The user is not logged to the app, or into Facebook:
           destroy the session on the server.
           */
        }
      });
    };
    // attach script
    (function (d, s, id) {
      var js, fjs = d.getElementsByTagName(s)[0];
      if (d.getElementById(id)) {
        return;
      }
      js = d.createElement(s);
      js.id = id;
      js.async = true;
      js.src = '//connect.facebook.net/en_US/sdk.js';
      fjs.parentNode.insertBefore(js, fjs);
    }(document, 'script', 'facebook-jssdk'));
  }

  setToken(fbToken) {
    this.accessToken = fbToken;
  }

  getToken(): Promise<string> {
    return new Promise((resolve, reject) => {
      if (!this.appId && isDevMode()) {
        reject('No FACEBOOK_APP_ID provided');
      }
      FB.login((response) => {
        // noinspection TypeScriptUnresolvedVariable
        if (response.authResponse) {
          resolve(response.authResponse.accessToken);
        } else {
          reject('Login FB cancelled');
        }
      }, {scope: 'public_profile,email'});

    });
  }
}
