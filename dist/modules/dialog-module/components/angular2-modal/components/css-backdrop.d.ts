import { ElementRef, Renderer } from '@angular/core';
import { BaseDynamicComponent } from './base-dynamic-component';
export declare class CSSBackdrop extends BaseDynamicComponent {
    constructor(el: ElementRef, renderer: Renderer);
}
