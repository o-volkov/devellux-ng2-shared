import {Directive, Output, EventEmitter, HostListener} from "@angular/core";
import {FacebookService} from "./facebook.service";

@Directive({
  selector: '[FacebookButton]',
  providers: [FacebookService]
})
export class FacebookButtonDirective {
  @Output() token: EventEmitter<any> = new EventEmitter();

  constructor(private service: FacebookService) {}

  @HostListener('click')
  getFacebookToken() {
    this.service.getToken().then(token => {
      this.token.emit(token);
    })
      .catch(() => '');
  }
}
