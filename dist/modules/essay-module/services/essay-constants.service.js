import { Injectable } from '@angular/core';
import { commonResponseHandler, commonErrorHandler } from "../../../helpers/common/common";
import { HttpClient } from "../../../helpers/http/client";
export var EssayConstantsService = (function () {
    function EssayConstantsService(httpClient) {
        this.httpClient = httpClient;
        this.cached = this.httpClient.get('/essay/directory')
            .map(commonResponseHandler)
            .do(function (constants) {
            constants.subjects.sort(function (a, b) {
                if (a.description === 'Other') {
                    return 1;
                }
                if (b.description === 'Other') {
                    return -1;
                }
                return a.description.localeCompare(b.description);
            });
            constants.styles.sort(function (a, b) {
                if (a.description === 'Other') {
                    return 1;
                }
                if (b.description === 'Other') {
                    return -1;
                }
                return a.description.localeCompare(b.description);
            });
        })
            .catch(commonErrorHandler).cache();
    }
    EssayConstantsService.prototype.getConstants = function () {
        return this.cached;
    };
    EssayConstantsService.decorators = [
        { type: Injectable },
    ];
    EssayConstantsService.ctorParameters = [
        { type: HttpClient, },
    ];
    return EssayConstantsService;
}());
//# sourceMappingURL=essay-constants.service.js.map