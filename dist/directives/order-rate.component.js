import { Component, Input } from "@angular/core";
export var OrderRateComponent = (function () {
    function OrderRateComponent() {
    }
    Object.defineProperty(OrderRateComponent.prototype, "orderRate", {
        get: function () {
            if (this.rate !== 0 && !this.rate) {
                return '-';
            }
            switch (this.rate) {
                case 0:
                    return '0/5';
                case 1:
                    return '1/5';
                case 2:
                    return '2/5';
                case 3:
                    return '3/5';
                case 4:
                    return '4/5';
                case 5:
                    return '5/5';
            }
        },
        set: function (rate) {
            this.rate = rate;
        },
        enumerable: true,
        configurable: true
    });
    OrderRateComponent.decorators = [
        { type: Component, args: [{
                    selector: 'span[orderRate], order-rate-renderer',
                    template: "\n  <!--<span [class]=\"getStateClass()\"><ng-content></ng-content>{{orderState}}</span>-->\n  \n    <span \n      [class.low-rate] = \"orderRate == '0/5' || orderRate == '1/5' || orderRate == '3/5'\"\n      [class.mid-rate] = \"orderRate == '2/5' || orderRate == '3/5'\"\n      [class.high-rate] = \"orderRate == '4/5' || orderRate == '5/5'\"\n    >{{orderRate}}</span>\n  ",
                    styles: ["\n    .mid-rate {\n        color: #ff9948;\n    }\n    .high-rate {\n        color: #039547;\n    }\n    .low-rate {\n        color: #d2335c;\n    }\n    \n        /* LIGHT */\n    \n    :host(.light) .mid-rate {\n        color: #FFAA59;\n    }\n    :host(.light) .high-rate {\n        color: #04BE5B;\n    }\n    :host(.light) .low-rate {\n        color: #F56185;\n    }"],
                },] },
    ];
    OrderRateComponent.ctorParameters = [];
    OrderRateComponent.propDecorators = {
        'orderRate': [{ type: Input },],
    };
    return OrderRateComponent;
}());
//# sourceMappingURL=order-rate.component.js.map