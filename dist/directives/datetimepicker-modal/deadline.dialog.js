import { Component } from "@angular/core";
import * as moment from "moment";
import { FormControl } from "@angular/forms";
import { DialogRef } from "../../modules/dialog-module/components/angular2-modal/index";
export var DeadlineDialogComponent = (function () {
    function DeadlineDialogComponent(dialog) {
        this.dialog = dialog;
        if (dialog.context.deadline) {
            this.deadline = moment.utc(dialog.context.deadline).toDate();
        }
        else {
            var deadline = moment(moment.utc(new Date()));
            this.deadline = deadline.add(2, 'days').toDate();
        }
        this.deadlineControl = new FormControl(this.deadline);
    }
    Object.defineProperty(DeadlineDialogComponent.prototype, "min", {
        get: function () {
            if (moment().add({ minutes: 30 }) < moment().endOf('day')) {
                return moment().toDate().toString();
            }
            else {
                return moment().add(1, 'day').startOf('day').toDate().toString();
            }
        },
        enumerable: true,
        configurable: true
    });
    DeadlineDialogComponent.prototype.onDeadlineSelected = function ($event) {
        $event.preventDefault();
        this.dialog.close(this.deadlineControl.value);
    };
    DeadlineDialogComponent.decorators = [
        { type: Component, args: [{
                    selector: 'dialog-custom',
                    template: "\n    <ng2-datetime-picker *ngIf=\"deadline\" [formControl]=\"deadlineControl\" [min]=\"min\"></ng2-datetime-picker>\n    <div class=\"after-datepicker\">\n        <button type=\"button\" (click)=\"onDeadlineSelected($event)\">Select</button>\n        <button type=\"button\" (click)=\"dialog.dismiss($event)\">Close</button>\n    </div>\n  "
                },] },
    ];
    DeadlineDialogComponent.ctorParameters = [
        { type: DialogRef, },
    ];
    return DeadlineDialogComponent;
}());
//# sourceMappingURL=deadline.dialog.js.map