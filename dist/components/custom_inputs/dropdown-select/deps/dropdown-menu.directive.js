import { Directive, ElementRef, Host } from '@angular/core';
import { DropdownDirective } from './dropdown.directive';
export var DropdownMenuDirective = (function () {
    function DropdownMenuDirective(dropdown, el) {
        this.dropdown = dropdown;
        this.el = el;
    }
    DropdownMenuDirective.prototype.ngOnInit = function () {
        this.dropdown.dropDownMenu = this;
    };
    DropdownMenuDirective.decorators = [
        { type: Directive, args: [{ selector: '[dropdownMenu]' },] },
    ];
    DropdownMenuDirective.ctorParameters = [
        { type: DropdownDirective, decorators: [{ type: Host },] },
        { type: ElementRef, },
    ];
    return DropdownMenuDirective;
}());
//# sourceMappingURL=dropdown-menu.directive.js.map