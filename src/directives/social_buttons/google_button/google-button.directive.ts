import {Directive, Output, EventEmitter, HostListener} from "@angular/core";
import {GoogleService} from "./google.service";

@Directive({
  selector: '[GoogleButton]',
  providers: [GoogleService]
})
export class GoogleButtonDirective {
  @Output() token: EventEmitter<any> = new EventEmitter();


  constructor(private service: GoogleService) {}

  @HostListener('click')
  getFacebookToken() {
    this.service.getToken().then(token => {
      this.token.emit(token);
    })
      .catch(() => '');
  }
}
