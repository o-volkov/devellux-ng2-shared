import { Renderer, ElementRef, OnInit, OnDestroy } from '@angular/core';
import { PresenceService } from './presence.service';
export declare class PresenceDirective implements OnInit, OnDestroy {
    private service;
    private el;
    private renderer;
    presence: any;
    presenceOnline: string;
    presenceOffline: string;
    presenceDefault: boolean;
    private getStateSub;
    private obsStateSub;
    constructor(service: PresenceService, el: ElementRef, renderer: Renderer);
    ngOnInit(): void;
    ngOnDestroy(): void;
    setStatus(status: any): void;
    private updatesListener(obj);
}
