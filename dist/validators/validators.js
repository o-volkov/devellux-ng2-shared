import * as moment from "moment";
export var CommonValidators = (function () {
    function CommonValidators() {
    }
    CommonValidators.isEmail = function (control) {
        if (!control.value) {
            return null;
        }
        var EMAIL_REG = /^[a-z0-9!#$%&'*+\/=?^_`{|}~.-]+@[a-z0-9]([a-z0-9-]*[a-z0-9])?(\.[a-z0-9]([a-z0-9-]*[a-z0-9])?)*$/i;
        return EMAIL_REG.test(control.value) ? null : {
            isEmail: true
        };
    };
    CommonValidators.isPhone = function (control) {
        var PHONE_REG = /^[+0-9]*$/i;
        return PHONE_REG.test(control.value) ? null : {
            isPhone: true
        };
    };
    CommonValidators.isMatch = function (first, second) {
        return function (group) {
            if ((group.controls[first].value !== group.controls[second].value) &&
                (group.controls[first].value !== '' && group.controls[second].value !== '')) {
                return {
                    isMatch: true
                };
            }
        };
    };
    CommonValidators.isNickname = function (control) {
        if (!control.value) {
            return null;
        }
        if (control.value.length > 20) {
            return {
                isNicknameLength: true
            };
        }
        var NICKNAME_REG = /^[_a-zA-Z0-9-]+$/i;
        return NICKNAME_REG.test(control.value) ? null : {
            isNickname: true
        };
    };
    CommonValidators.isName = function (control) {
        var NAME_REG = /^[a-zA-Z ]{0,30}$/;
        return NAME_REG.test(control.value) ? null : {
            isName: true
        };
    };
    CommonValidators.isCreditCard = function (control) {
        var CARD_REG = /^(?:4[0-9]{12}(?:[0-9]{3})?|5[1-5][0-9]{14}|6(?:011|5[0-9][0-9])[0-9]{12}|3[47][0-9]{13}|3(?:0[0-5]|[68][0-9])[0-9]{11}|(?:2131|1800|35\d{3})\d{11})$/;
        return CARD_REG.test(control.value) ? null : {
            isCreditCard: true
        };
    };
    CommonValidators.isAmount = function (control) {
        var AMOUNT_REG = /^\$?[0-9]+(\.[0-9][0-9])?$/;
        return AMOUNT_REG.test(control.value) ? null : {
            isAmount: true
        };
    };
    CommonValidators.minAmount = function (control) {
        var val = parseFloat(control.value);
        if (val < 0.01) {
            return { minAmount: true };
        }
        else {
            return null;
        }
    };
    CommonValidators.maxAmount = function (control) {
        var val = parseFloat(control.value);
        if (val > 3000.00) {
            return { maxAmount: true };
        }
        else {
            return null;
        }
    };
    CommonValidators.isCVV = function (control) {
        var CVV_REG = /^[0-9]{3,4}$/;
        return CVV_REG.test(control.value) ? null : {
            isCVV: true
        };
    };
    CommonValidators.isMMYY = function (control) {
        var MMYY_REG = /^((0[1-9])|(1[0-2]))\/([0-9]{2})$/;
        return MMYY_REG.test(control.value) ? null : {
            isMMYY: true
        };
    };
    CommonValidators.isOverTime = function (control) {
        var timeDiff = moment(control.value).unix() - moment().unix();
        return (timeDiff > 0) ? null : { isOverTime: true };
    };
    CommonValidators.creditCard = function (control) {
        if (!control.value) {
            return;
        }
        var cardTypes = [
            {
                name: 'amex',
                pattern: /^3[47]/,
                valid_length: [15]
            },
            {
                name: 'diners_club_carte_blanche',
                pattern: /^30[0-5]/,
                valid_length: [14]
            },
            {
                name: 'diners_club_international',
                pattern: /^36/,
                valid_length: [14]
            },
            {
                name: 'jcb',
                pattern: /^35(2[89]|[3-8][0-9])/,
                valid_length: [16]
            },
            {
                name: 'laser',
                pattern: /^(6304|670[69]|6771)/,
                valid_length: [16, 19]
            },
            {
                name: 'visa_electron',
                pattern: /^(4026|417500|4508|4844|491(3|7))/,
                valid_length: [16]
            },
            {
                name: 'visa',
                pattern: /^4/,
                valid_length: [16]
            },
            {
                name: 'mastercard',
                pattern: /^5[1-5]/,
                valid_length: [16]
            },
            {
                name: 'maestro',
                pattern: /^(5018|5020|5038|6304|6759|676[1-3])/,
                valid_length: [12, 19]
            },
            {
                name: 'discover',
                pattern: /^(6011|622(12[6-9]|1[3-9][0-9]|[2-8][0-9]{2}|9[0-1][0-9]|92[0-5]|64[4-9])|65)/,
                valid_length: [16]
            }
        ];
        function getCardType(cardNumber) {
            var cards = cardTypes.filter(function (cardType) {
                if (cardNumber.match(cardType.pattern)) {
                    return cardType;
                }
            });
            return cards.pop();
        }
        function isValidLuhn(cardNumber) {
            var digit, n, sum, j, len1, ref1;
            sum = 0;
            ref1 = cardNumber.split('').reverse();
            for (n = j = 0, len1 = ref1.length; j < len1; n = ++j) {
                digit = ref1[n];
                digit = +digit;
                if (n % 2) {
                    digit *= 2;
                    if (digit < 10) {
                        sum += digit;
                    }
                    else {
                        sum += digit - 9;
                    }
                }
                else {
                    sum += digit;
                }
            }
            return sum % 10 === 0;
        }
        function validateNumber(cardNumber) {
            var cardType = getCardType(cardNumber);
            var luhnValid = false;
            var lengthValid = false;
            if (cardType) {
                luhnValid = isValidLuhn(cardNumber);
                lengthValid = cardType.valid_length.indexOf(cardNumber.length) >= 0;
            }
            return {
                card_type: cardType,
                valid: luhnValid && lengthValid,
                luhn_valid: luhnValid,
                length_valid: lengthValid
            };
        }
        var cardNumber = control.value.replace(/ [-] /g, '');
        return validateNumber(cardNumber) ? null : true;
    };
    return CommonValidators;
}());
export function minNumberValidator(minNumber) {
    return function (c) {
        if (c.value < minNumber) {
            return { minNumber: true };
        }
        return null;
    };
}
//# sourceMappingURL=validators.js.map