export { FileItem } from './file-item.class';
export { FileLikeObject } from './file-like-object.class';
export { FileDropDirective } from './file-drop.directive';
export { FileSelectDirective } from './file-select.directive';
export { FileType } from './file-type.class';
export { FileUploader, FileUploaderOptions } from './file-uploader.class';
export declare const FILE_UPLOAD_DIRECTIVES: [any];
