import { Pipe } from '@angular/core';
export var KeysPipe = (function () {
    function KeysPipe() {
    }
    KeysPipe.prototype.transform = function (input) {
        if (typeof input !== 'object') {
            return input;
        }
        return Object.keys(input);
    };
    KeysPipe.decorators = [
        { type: Pipe, args: [{ name: 'keys' },] },
    ];
    KeysPipe.ctorParameters = [];
    return KeysPipe;
}());
//# sourceMappingURL=keys.pipe.js.map