import { Pipe } from '@angular/core';
export var WherePipe = (function () {
    function WherePipe() {
    }
    WherePipe.prototype.transform = function (input, fn) {
        if (!Array.isArray(input)) {
            return input;
        }
        if (typeof fn === 'function') {
            return input.filter(fn);
        }
        else if (Array.isArray((fn))) {
            var key_1 = fn[0], value_1 = fn[1];
            return input.filter(function (item) { return getProperty(item, key_1) === value_1; });
        }
        else if (fn) {
            return input.filter(function (item) { return item === fn; });
        }
        else {
            return input;
        }
    };
    WherePipe.decorators = [
        { type: Pipe, args: [{
                    name: 'where'
                },] },
    ];
    WherePipe.ctorParameters = [];
    return WherePipe;
}());
export function getProperty(value, key) {
    var keys = key.split('.');
    var result = value[keys.shift()];
    while (keys.length && (result = result[keys.shift()])) {
    }
    return result;
}
//# sourceMappingURL=where.pipe.js.map