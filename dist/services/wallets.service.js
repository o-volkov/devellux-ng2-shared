import { Injectable, EventEmitter } from "@angular/core";
import { Observable } from "rxjs/Observable";
import { User } from "./user.service";
import { HttpClient } from "../helpers/http/client";
import { commonResponseListHandler, commonErrorHandler } from "../helpers/common/common";
export var WalletsService = (function () {
    function WalletsService(httpClient, user) {
        var _this = this;
        this.httpClient = httpClient;
        this.user = user;
        this.walletsEE = new EventEmitter();
        this.wallets$ = this.walletsEE.asObservable();
        this.user.status$.subscribe(function () { return _this.getWallets(); });
    }
    WalletsService.prototype.getWallets = function () {
        var _this = this;
        var obs;
        if (this.user.isAuthenticated()) {
            obs = this.httpClient.get('billing/wallet')
                .map(commonResponseListHandler)
                .catch(commonErrorHandler);
        }
        else {
            obs = Observable.of([]);
        }
        return obs.do(function (wallets) { return _this.walletsEE.emit(wallets); });
    };
    WalletsService.decorators = [
        { type: Injectable },
    ];
    WalletsService.ctorParameters = [
        { type: HttpClient, },
        { type: User, },
    ];
    return WalletsService;
}());
//# sourceMappingURL=wallets.service.js.map