export function getCookie(name) {
    var matches = document.cookie.match(new RegExp('(?:^|; )' + name.replace(/([\.$?*|{}\(\)\[\]\\\/\+^])/g, '\\$1') + '=([^;]*)'));
    return matches ? decodeURIComponent(matches[1]) : undefined;
}
export function setCookie(name, value, options) {
    options = options || { expires: 14 * 24 * 3600 };
    var expires = options.expires;
    if (typeof expires == 'number' && expires) {
        var d = new Date();
        d.setTime(d.getTime() + expires * 1000);
        expires = options.expires = d;
    }
    if (expires && expires.toUTCString) {
        options.expires = expires.toUTCString();
    }
    if (!options.path) {
        options.path = '/';
    }
    value = encodeURIComponent(value);
    var updatedCookie = name + '=' + value;
    for (var propName in options) {
        updatedCookie += '; ' + propName;
        var propValue = options[propName];
        if (propValue !== true) {
            updatedCookie += '=' + propValue;
        }
    }
    document.cookie = updatedCookie;
}
export function deleteCookie(name) {
    setCookie(name, '', {
        expires: -1
    });
}
export function moveFromUriToCookies(urlKey, cookieKey) {
    if (cookieKey === void 0) { cookieKey = urlKey; }
    var search = window.location.search;
    if (search) {
        var obj_1 = {};
        search
            .replace('?', '')
            .split('&')
            .forEach(function (part) {
            var tmp = part.split('=');
            if (tmp[0]) {
                obj_1[tmp[0]] = tmp[1];
            }
        });
        if (obj_1[urlKey]) {
            setCookie(cookieKey, obj_1[urlKey], {});
        }
        delete obj_1[urlKey];
        if (Object.keys(obj_1).length) {
            search = '?';
            Object.keys(obj_1).forEach(function (key) {
                search += key + '=' + obj_1[key] + '&';
            });
            search = search.slice(0, -1);
        }
        else {
            search = '';
        }
    }
    var url = window.location.pathname + search + window.location.hash;
    window.history.replaceState({}, document.title, url);
}
//# sourceMappingURL=cookie.js.map