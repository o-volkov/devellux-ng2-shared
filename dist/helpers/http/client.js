import { Injectable, Optional, Inject } from '@angular/core';
import { RequestMethod, Request, Http } from '@angular/http';
import { AppRequestOptions } from './request-options';
import { OpaqueToken } from '@angular/core';
export var API_URL_TOKEN = new OpaqueToken('apiUrl');
export var HttpClient = (function () {
    function HttpClient(http, baseUrl) {
        this.http = http;
        this.baseUrl = baseUrl;
        this.successIntercept = [];
        this.errorIntercept = [];
    }
    HttpClient.prototype.addSuccessIntercept = function (intercept) {
        this.successIntercept.push(intercept);
    };
    HttpClient.prototype.addErrorIntercept = function (intercept) {
        this.errorIntercept.push(intercept);
    };
    HttpClient.prototype.prepareRequest = function (requestArgs, additionalOptions) {
        var options = new AppRequestOptions();
        options.baseUrl = this.baseUrl;
        if (additionalOptions) {
            requestArgs = Object.assign(requestArgs, additionalOptions);
        }
        options = options.merge(requestArgs);
        if (typeof options.body !== 'string') {
            options.body = JSON.stringify(options.body);
        }
        var req = new Request(options);
        return this.intercept(this.http.request(req));
    };
    HttpClient.prototype.get = function (url, options) {
        return this.prepareRequest({ url: url, method: RequestMethod.Get }, options);
    };
    HttpClient.prototype.post = function (url, body, options) {
        return this.prepareRequest({ url: url, body: body, method: RequestMethod.Post }, options);
    };
    HttpClient.prototype.put = function (url, body, options) {
        return this.prepareRequest({ url: url, body: body, method: RequestMethod.Put }, options);
    };
    HttpClient.prototype.delete = function (url, body, options) {
        return this.prepareRequest({ url: url, body: body, method: RequestMethod.Delete }, options);
    };
    HttpClient.prototype.patch = function (url, body, options) {
        return this.prepareRequest({ url: url, body: body, method: RequestMethod.Patch }, options);
    };
    HttpClient.prototype.intercept = function (observable) {
        observable = this.successIntercept.reduce(function (pv, cv) { return pv.map(cv); }, observable);
        observable = this.errorIntercept.reduce(function (pv, cv) { return pv.catch(cv); }, observable);
        return observable;
    };
    HttpClient.decorators = [
        { type: Injectable },
    ];
    HttpClient.ctorParameters = [
        { type: Http, },
        { type: undefined, decorators: [{ type: Optional }, { type: Inject, args: [API_URL_TOKEN,] },] },
    ];
    return HttpClient;
}());
//# sourceMappingURL=client.js.map