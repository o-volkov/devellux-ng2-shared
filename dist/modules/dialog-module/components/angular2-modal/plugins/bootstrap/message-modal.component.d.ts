import { DialogRef, ModalComponent } from '../../../../components/angular2-modal';
import { MessageModalPreset } from './presets/message-modal-preset';
export interface BSMessageModalButtonHandler {
    (cmp: ModalComponent<MessageModalPreset>, $event: MouseEvent): void;
}
export interface BSMessageModalButtonConfig {
    cssClass: string;
    caption: string;
    onClick: BSMessageModalButtonHandler;
}
export declare class BSMessageModalTitle {
    dialog: DialogRef<MessageModalPreset>;
    context: MessageModalPreset;
    constructor(dialog: DialogRef<MessageModalPreset>);
    readonly titleHtml: number;
}
export declare class BSMessageModalBody {
    dialog: DialogRef<MessageModalPreset>;
    private context;
    constructor(dialog: DialogRef<MessageModalPreset>);
}
export declare class BSModalFooter {
    dialog: DialogRef<MessageModalPreset>;
    constructor(dialog: DialogRef<MessageModalPreset>);
    onClick(btn: BSMessageModalButtonConfig, $event: MouseEvent): void;
}
export declare class BSMessageModal implements ModalComponent<MessageModalPreset> {
    dialog: DialogRef<MessageModalPreset>;
    constructor(dialog: DialogRef<MessageModalPreset>);
}
