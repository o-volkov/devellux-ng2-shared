import { Directive, Input, Output, HostBinding, EventEmitter, ElementRef } from '@angular/core';
import { dropdownService, NONINPUT } from './dropdown.service';
export var DropdownDirective = (function () {
    function DropdownDirective(el) {
        this.onToggle = new EventEmitter(false);
        this.isOpenChange = new EventEmitter(false);
        this.addClass = true;
        this.el = el;
    }
    Object.defineProperty(DropdownDirective.prototype, "isOpen", {
        get: function () {
            return this.iisOpen;
        },
        set: function (value) {
            this.iisOpen = !!value;
            if (this.isOpen) {
                this.focusToggleElement();
                dropdownService.open(this);
            }
            else {
                dropdownService.close(this);
                this.selectedOption = void 0;
            }
            this.onToggle.emit(this.isOpen);
            this.isOpenChange.emit(this.isOpen);
        },
        enumerable: true,
        configurable: true
    });
    DropdownDirective.prototype.ngOnInit = function () {
        this.autoClose = this.autoClose || NONINPUT;
        if (this.isOpen) {
        }
    };
    DropdownDirective.prototype.ngOnDestroy = function () {
        if (this.appendToBody && this.menuEl) {
            this.menuEl.nativeElement.remove();
        }
    };
    Object.defineProperty(DropdownDirective.prototype, "dropDownMenu", {
        set: function (dropdownMenu) {
            this.menuEl = dropdownMenu.el;
            if (this.appendToBody) {
                window.document.body.appendChild(this.menuEl.nativeElement);
            }
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(DropdownDirective.prototype, "dropDownToggle", {
        set: function (dropdownToggle) {
            this.toggleEl = dropdownToggle.el;
        },
        enumerable: true,
        configurable: true
    });
    DropdownDirective.prototype.toggle = function (open) {
        return this.isOpen = arguments.length ? !!open : !this.isOpen;
    };
    DropdownDirective.prototype.focusDropdownEntry = function (keyCode) {
        var hostEl = this.menuEl ?
            this.menuEl.nativeElement :
            this.el.nativeElement.getElementsByTagName('ul')[0];
        if (!hostEl) {
            return;
        }
        var elems = hostEl.getElementsByTagName('a');
        if (!elems || !elems.length) {
            return;
        }
        switch (keyCode) {
            case (40):
                if (typeof this.selectedOption !== 'number') {
                    this.selectedOption = 0;
                    break;
                }
                if (this.selectedOption === elems.length - 1) {
                    break;
                }
                this.selectedOption++;
                break;
            case (38):
                if (typeof this.selectedOption !== 'number') {
                    return;
                }
                if (this.selectedOption === 0) {
                    break;
                }
                this.selectedOption--;
                break;
            default:
                break;
        }
        elems[this.selectedOption].focus();
    };
    DropdownDirective.prototype.focusToggleElement = function () {
        if (this.toggleEl) {
            this.toggleEl.nativeElement.focus();
        }
    };
    DropdownDirective.decorators = [
        { type: Directive, args: [{ selector: '[dropdown]' },] },
    ];
    DropdownDirective.ctorParameters = [
        { type: ElementRef, },
    ];
    DropdownDirective.propDecorators = {
        'isOpen': [{ type: HostBinding, args: ['class.open',] }, { type: Input },],
        'autoClose': [{ type: Input },],
        'keyboardNav': [{ type: Input },],
        'appendToBody': [{ type: Input },],
        'onToggle': [{ type: Output },],
        'isOpenChange': [{ type: Output },],
        'addClass': [{ type: HostBinding, args: ['class.dropdown',] },],
    };
    return DropdownDirective;
}());
//# sourceMappingURL=dropdown.directive.js.map