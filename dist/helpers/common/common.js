import { Observable } from 'rxjs/Observable';
export function formErrorHandler(form, errors) {
    if (errors && errors.error_map) {
        Object.keys(errors.error_map).forEach((function (key) {
            if (!form.contains(key)) {
                return;
            }
            var e = {};
            e[errors.error_map[key].code] = errors.error_map[key].description;
            form.controls[key].setErrors(e);
        }));
    }
}
export function copyObject(object) {
    var objectCopy = {};
    for (var key in object) {
        if (object.hasOwnProperty(key)) {
            objectCopy[key] = object[key];
        }
    }
    return objectCopy;
}
export function commonResponseHandler(response) {
    var res = response.json();
    return res;
}
export function commonResponseListHandler(response) {
    var res = response.json().objects;
    return res;
}
export function commonErrorHandler(error) {
    var e;
    try {
        console.log('Request error', error.json());
        e = Observable.throw(error.json());
    }
    catch (error) {
        e = Observable.throw({ 'description': 'Server error' });
    }
    return e;
}
//# sourceMappingURL=common.js.map