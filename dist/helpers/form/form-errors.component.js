import { Component, Input, SkipSelf, Host, Optional } from "@angular/core";
import { FormControl, ControlContainer } from "@angular/forms";
export var FormErrorsComponent = (function () {
    function FormErrorsComponent(parent) {
        this.parent = parent;
        this.errors = {};
        this.ERRORS = {
            'AUTH.EMAIL_ALREADY_TAKEN': 'EMAIL ALREADY TAKEN',
            'AUTH.EMAIL_NOT_FOUND': 'EMAIL IS NOT REGISTERED',
            'AUTH.WRONG_PASSWORD': 'PASSWORD DOES NOT MATCH',
            isEmail: 'INVALID EMAIL ADDRESS',
            required: 'REQUIRED',
            minlength: 'PASSWORD IS TOO SHORT',
            isMatch: 'PASSWORD DO NOT MATCH',
            pattern: 'NOT ALLOWED SYMBOLS',
        };
    }
    FormErrorsComponent.prototype.ngOnInit = function () {
        if (typeof this.errors === "object") {
            Object.assign(this.ERRORS, this.errors);
        }
        if (!(this.control instanceof FormControl)) {
            this.control = this.parent['form'].controls[this.control];
        }
    };
    FormErrorsComponent.decorators = [
        { type: Component, args: [{
                    selector: 'form-errors',
                    template: "\n    <div *ngIf=\"!control.pristine && !control.valid && control.touched\">\n      <template ngFor let-key [ngForOf]=\"control.errors | keys\">\n          <div class=\"auth-cmp__input-error\" role=\"alert\">\n              {{ERRORS[key] || control.errors[key]}}\n          </div>\n      </template>\n    </div>\n  ",
                },] },
    ];
    FormErrorsComponent.ctorParameters = [
        { type: ControlContainer, decorators: [{ type: Optional }, { type: Host }, { type: SkipSelf },] },
    ];
    FormErrorsComponent.propDecorators = {
        'control': [{ type: Input },],
        'errors': [{ type: Input },],
    };
    return FormErrorsComponent;
}());
//# sourceMappingURL=form-errors.component.js.map