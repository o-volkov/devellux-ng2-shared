import { ControlValueAccessor } from "@angular/forms";
import { CustomNgInput } from "../custom-ng2-input";
export declare class Select2Component extends CustomNgInput implements ControlValueAccessor {
    options: Select2Option[];
    disabled: boolean;
    placeholder: string;
    viewValue: any;
    private items;
    ngOnInit(): void;
    selectOption(value: any): void;
    onTouched(): void;
    updateViewValue(v: any): void;
}
export interface Select2Option {
    title: any;
    value: any;
}
