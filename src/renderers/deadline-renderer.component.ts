import { Component, Input, OnInit } from '@angular/core';
import * as moment from 'moment';
@Component({
  selector: 'deadline-renderer',

  template: `<span *ngIf="!isFutute">+ {{deadline | toNow}}</span>
             <span *ngIf="isFutute" [ngClass] = "setClasses()">{{deadline | toNow}} ago</span>`,
})
export class DeadlineRendererComponent implements OnInit {
  @Input() deadline;
  @Input() highlighted;
  isFutute;

  ngOnInit() {
    this.isFutute = moment(moment.utc(this.deadline).toDate()).diff(moment(new Date())) < 0;
  };

  setClasses() {
    let classes = {
    'text-color__danger': this.highlighted,
    'weight-400': this.highlighted
    };
    return classes;
  }

}
