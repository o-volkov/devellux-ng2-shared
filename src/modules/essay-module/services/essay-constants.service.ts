import { Injectable } from '@angular/core';
import {commonResponseHandler, commonErrorHandler} from "../../../helpers/common/common";
import {HttpClient} from "../../../helpers/http/client";
import {Observable} from "rxjs";


@Injectable()
export class EssayConstantsService {
  private cached:Observable<Constants> = this.httpClient.get('/essay/directory')
    .map<Constants>(commonResponseHandler)
    .do((constants: Constants) => {

        constants.subjects.sort((a, b) => {
          if (a.description === 'Other') {
            return 1;
          }
          if (b.description === 'Other') {
            return -1;
          }
          return a.description.localeCompare(b.description);

        });

        constants.styles.sort((a, b) => {
          if (a.description === 'Other') {
            return 1;
          }
          if (b.description === 'Other') {
            return -1;
          }
          return a.description.localeCompare(b.description);

        });
    })
    .catch(commonErrorHandler).cache();

  constructor(private httpClient: HttpClient) {
  }


  getConstants() {
    return this.cached;
  }

}

export interface Service {
  'default': boolean;
  value: number;
  key: string;
  description: string;
}

export interface Style {
  'default': boolean;
  value: number;
  key: string;
  description: string;
}

export interface Level {
  'default': boolean;
  value: number;
  key: string;
  description: string;
}

export interface Subject {
  'default': boolean;
  value: number;
  key: string;
  description: string;
}

export interface Type {
  'default': boolean;
  value: number;
  key: string;
  description: string;
}

export interface Constants {
  services: Service[];
  styles: Style[];
  levels: Level[];
  subjects: Subject[];
  types: Type[];
}


