import { HttpClient } from "../../../helpers/http/client";
import { Observable } from "rxjs";
export declare class EssayConstantsService {
    private httpClient;
    private cached;
    constructor(httpClient: HttpClient);
    getConstants(): Observable<Constants>;
}
export interface Service {
    'default': boolean;
    value: number;
    key: string;
    description: string;
}
export interface Style {
    'default': boolean;
    value: number;
    key: string;
    description: string;
}
export interface Level {
    'default': boolean;
    value: number;
    key: string;
    description: string;
}
export interface Subject {
    'default': boolean;
    value: number;
    key: string;
    description: string;
}
export interface Type {
    'default': boolean;
    value: number;
    key: string;
    description: string;
}
export interface Constants {
    services: Service[];
    styles: Style[];
    levels: Level[];
    subjects: Subject[];
    types: Type[];
}
