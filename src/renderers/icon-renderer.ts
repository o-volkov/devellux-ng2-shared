import { Component, Input, ViewEncapsulation } from '@angular/core';

@Component({
  selector: 'icon-renderer',
  template: `
    <svg width="100%" height="100%">
      <use [attr.xlink:href]="'/assets/symbols.svg#'+name" x="0" y="0" width="100%" height="100%"></use>
    </svg>
  `,
  styles: [`
    svg {
      fill: currentColor;
    }
  `],
  encapsulation: ViewEncapsulation.None,
})

export class IconRendererComponent {

  @Input() name;

}
