import { URLSearchParams } from "@angular/http";
import { Observable } from "rxjs/Observable";
export declare function makeInfinite<T>(func: (p: URLSearchParams) => any, opts: any): InfiniteList<T>;
export interface InfiniteList<T> {
    result: Observable<any>;
    loadMore: Function;
    hasMore: boolean;
    objects?: any[];
}
