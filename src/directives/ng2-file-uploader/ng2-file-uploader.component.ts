import { Component, ViewEncapsulation } from '@angular/core';
import {
  FileItem,
  FileUploader,
  FileUploaderOptions,
  FILE_UPLOAD_DIRECTIVES
} from './shared/index';

const MAX_FILE_SIZE = 2048000;
const MAX_FILES_COUNT = 10;
const URL = 'http://localhost:8081';

@Component({
  selector: 'ng2-file-uploader',
  template: `<template [ngIf]="uploader && uploader.queue && uploader.queue.length == 0">
  <div class="drag-and-drop__block"
       ng2FileDrop
       [uploader]="uploader"
       (fileOver)="fileOver($event)"
       [class.nv-file-over]="hasDropZoneOver">
    <i class="icon icon__drag-and-drop"></i>
    <div class="drag-and-drop__title">Drag & Drop<br>your files here</div>
  </div>
  <div class="flex__container flex__j-center">
    <input type="file" ng2FileSelect [uploader]="uploader" />
  </div>
</template>

<template [ngIf]="uploader.queue.length > 0">
  <h3>Files count {{uploader.queue.length}}</h3>
</template>`,
  styles: [ `.nv-file-over {
  border: dotted 3px red;
}` ],
  encapsulation: ViewEncapsulation.None,
})
export class Ng2FileUploaderComponent {
  uploader: FileUploader = new FileUploader({url: URL});
  hasDropZoneOver: boolean = false;
  fileItem: FileItem;

  constructor() {
    let uploaderOptions: FileUploaderOptions = {
      maxFileSize: MAX_FILE_SIZE,
      queueLimit: MAX_FILES_COUNT,
    };
    this.uploader.setOptions(uploaderOptions);
    this.uploader.onAfterAddingFile = this.fileSelected.bind(this);
  }

  fileOver(event: any): void {
    this.hasDropZoneOver = event;
  }

  fileSelected(fileItem: FileItem) {
    this.fileItem = fileItem;
    return {fileItem}
  }
}
