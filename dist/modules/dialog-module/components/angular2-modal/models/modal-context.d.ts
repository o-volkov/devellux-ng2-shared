import { FluentAssignMethod } from './../framework/fluent-assign';
import { OverlayContext, OverlayContextBuilder } from './overlay-context';
export declare const DEFAULT_VALUES: {};
export declare class ModalContext extends OverlayContext {
    message: string;
}
export declare class ModalContextBuilder<T extends ModalContext> extends OverlayContextBuilder<T> {
    message: FluentAssignMethod<string, this>;
    constructor(defaultValues?: T | T[], initialSetters?: string[], baseType?: new () => T);
}
