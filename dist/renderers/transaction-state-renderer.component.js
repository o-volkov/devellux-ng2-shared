import { Component, Input } from '@angular/core';
export var TransactionStateRendererComponent = (function () {
    function TransactionStateRendererComponent() {
        this.states = {
            new: {
                color: '#bd10e0', text: 'New'
            },
            closed: {
                color: '#54657e', text: 'Closed'
            },
            finished: {
                color: '#00a259', text: 'Ok',
            },
            success: {
                color: '#00a259', text: 'Success',
            },
            pending: {
                color: '#ff9948', text: 'Pending'
            },
            in_progress: {
                color: '#ff9948', text: 'In progress'
            },
            progress: {
                color: '#ff9948', text: 'In progress'
            },
            declined: {
                color: '#d2335c', text: 'Declined'
            },
            canceled: {
                color: '#d2335c', text: 'Canceled'
            }
        };
        this.state = '';
    }
    TransactionStateRendererComponent.decorators = [
        { type: Component, args: [{
                    selector: 'transaction-state-renderer',
                    template: "<span *ngIf=\"states[state]\"\n                   style=\"text-transform: none\" \n                   [style.color]=\"states[state].color\">\n                      {{states[state].text}}\n                   </span>",
                },] },
    ];
    TransactionStateRendererComponent.ctorParameters = [];
    TransactionStateRendererComponent.propDecorators = {
        'state': [{ type: Input },],
    };
    return TransactionStateRendererComponent;
}());
//# sourceMappingURL=transaction-state-renderer.component.js.map