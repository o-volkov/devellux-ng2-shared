import { Pipe, PipeTransform } from '@angular/core';
import { EssayConstantsService, Constants } from '../services/essay-constants.service';
@Pipe({
  name: 'essayConstants',
  pure: false,
})
export class EssayConstantsPipe implements PipeTransform {
  data: Constants;

  constructor(private constants: EssayConstantsService) {
    this.constants.getConstants().subscribe(data => this.data = data);
  }

  transform(value, type: 'services'| 'styles'| 'levels'| 'subjects'| 'types') {
    if (type && this.data && this.data[type]) {
      let essayConst = this.data[type].filter(item => item.value == value).pop();
      if (essayConst) {
        return essayConst.description;
      }
    }
  }
}
