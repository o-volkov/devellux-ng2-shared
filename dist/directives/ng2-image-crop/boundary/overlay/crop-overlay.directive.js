import { Directive, ElementRef, Renderer } from '@angular/core';
export var CropBoundaryOverlayDirective = (function () {
    function CropBoundaryOverlayDirective(element, renderer) {
        this.element = element;
        this.renderer = renderer;
    }
    CropBoundaryOverlayDirective.decorators = [
        { type: Directive, args: [{
                    selector: '[crop-boundary-overlay]',
                },] },
    ];
    CropBoundaryOverlayDirective.ctorParameters = [
        { type: ElementRef, },
        { type: Renderer, },
    ];
    return CropBoundaryOverlayDirective;
}());
//# sourceMappingURL=crop-overlay.directive.js.map