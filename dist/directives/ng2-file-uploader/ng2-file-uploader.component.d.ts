import { FileItem, FileUploader } from './shared/index';
export declare class Ng2FileUploaderComponent {
    uploader: FileUploader;
    hasDropZoneOver: boolean;
    fileItem: FileItem;
    constructor();
    fileOver(event: any): void;
    fileSelected(fileItem: FileItem): {
        fileItem: FileItem;
    };
}
