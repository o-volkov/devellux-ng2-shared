import { PipeTransform } from '@angular/core';
export declare class CutPipe implements PipeTransform {
    transform(value: any, arg1: number): any;
}
