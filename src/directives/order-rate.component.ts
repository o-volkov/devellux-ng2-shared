import {Component, Input} from "@angular/core";

@Component({
  /* tslint:disable */
  selector: 'span[orderRate], order-rate-renderer',
  /* tslint:enable*/
  template: `
  <!--<span [class]="getStateClass()"><ng-content></ng-content>{{orderState}}</span>-->
  
    <span 
      [class.low-rate] = "orderRate == '0/5' || orderRate == '1/5' || orderRate == '3/5'"
      [class.mid-rate] = "orderRate == '2/5' || orderRate == '3/5'"
      [class.high-rate] = "orderRate == '4/5' || orderRate == '5/5'"
    >{{orderRate}}</span>
  `,
  styles: [`
    .mid-rate {
        color: #ff9948;
    }
    .high-rate {
        color: #039547;
    }
    .low-rate {
        color: #d2335c;
    }
    
        /* LIGHT */
    
    :host(.light) .mid-rate {
        color: #FFAA59;
    }
    :host(.light) .high-rate {
        color: #04BE5B;
    }
    :host(.light) .low-rate {
        color: #F56185;
    }`],
})
export class OrderRateComponent {
  private rate;

  @Input()
  set orderRate(rate) {
    this.rate = rate;
  }

  get orderRate() {
    if (this.rate !==0 && !this.rate) {
      return '-';
    }

    switch (this.rate) {
      case 0:
        return '0/5';
      case 1:
        return '1/5';
      case 2:
        return '2/5';
      case 3:
        return '3/5';
      case 4:
        return '4/5';
      case 5:
        return '5/5';
    }
  }

}
