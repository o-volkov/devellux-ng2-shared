import { Injectable, OpaqueToken, Inject, Optional, isDevMode } from '@angular/core';
export var FACEBOOK_APP_ID = new OpaqueToken('FacebookAppId');
export var FacebookService = (function () {
    function FacebookService(appId) {
        var _this = this;
        this.appId = appId;
        if (!appId && isDevMode()) {
            console.error('no FACEBOOK_APP_ID provided');
            return;
        }
        window['fbAsyncInit'] = function () {
            FB.init({
                appId: appId,
                status: true,
                cookie: true,
                xfbml: true,
                version: 'v2.5'
            });
            FB.Event.subscribe('auth.statusChange', function (res) {
                if (res.status === 'connected') {
                    _this.setToken(res.authResponse.accessToken);
                }
                else {
                }
            });
        };
        (function (d, s, id) {
            var js, fjs = d.getElementsByTagName(s)[0];
            if (d.getElementById(id)) {
                return;
            }
            js = d.createElement(s);
            js.id = id;
            js.async = true;
            js.src = '//connect.facebook.net/en_US/sdk.js';
            fjs.parentNode.insertBefore(js, fjs);
        }(document, 'script', 'facebook-jssdk'));
    }
    FacebookService.prototype.setToken = function (fbToken) {
        this.accessToken = fbToken;
    };
    FacebookService.prototype.getToken = function () {
        var _this = this;
        return new Promise(function (resolve, reject) {
            if (!_this.appId && isDevMode()) {
                reject('No FACEBOOK_APP_ID provided');
            }
            FB.login(function (response) {
                if (response.authResponse) {
                    resolve(response.authResponse.accessToken);
                }
                else {
                    reject('Login FB cancelled');
                }
            }, { scope: 'public_profile,email' });
        });
    };
    FacebookService.decorators = [
        { type: Injectable },
    ];
    FacebookService.ctorParameters = [
        { type: undefined, decorators: [{ type: Optional }, { type: Inject, args: [FACEBOOK_APP_ID,] },] },
    ];
    return FacebookService;
}());
//# sourceMappingURL=facebook.service.js.map