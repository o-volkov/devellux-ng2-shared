var __extends = (this && this.__extends) || function (d, b) {
    for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p];
    function __() { this.constructor = d; }
    d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
};
import { Component, Input, forwardRef } from "@angular/core";
import { NG_VALUE_ACCESSOR } from "@angular/forms";
import { CustomNgInput } from "../custom-ng2-input";
var CUSTOM_INPUT_CONTROL_VALUE_ACCESSOR = {
    provide: NG_VALUE_ACCESSOR,
    useExisting: forwardRef(function () { return Select2Component; }),
    multi: true
};
export var Select2Component = (function (_super) {
    __extends(Select2Component, _super);
    function Select2Component() {
        _super.apply(this, arguments);
        this.options = [];
        this.disabled = false;
        this.placeholder = 'some';
        this.items = [];
    }
    Select2Component.prototype.ngOnInit = function () {
        this.items = this.options.map(function (o) { return ({ id: o.value, text: o.title }); });
    };
    Select2Component.prototype.selectOption = function (value) {
        this.value = value.id;
        this.touched();
    };
    Select2Component.prototype.onTouched = function () {
        this.touched();
    };
    Select2Component.prototype.updateViewValue = function (v) {
        var view = this.items.find(function (item) { return item.id === v; });
        if (view) {
            this.viewValue = [view];
        }
    };
    Select2Component.decorators = [
        { type: Component, args: [{
                    selector: 'select2',
                    template: "<ng-select [allowClear]=\"true\"\n              [items]=\"items\"\n              [active]=\"viewValue\"\n              [disabled]=\"disabled\"\n              (selected)=\"selectOption($event)\"\n              [placeholder]=\"placeholder\"\n              >\n  </ng-select>",
                    providers: [CUSTOM_INPUT_CONTROL_VALUE_ACCESSOR],
                },] },
    ];
    Select2Component.ctorParameters = [];
    Select2Component.propDecorators = {
        'options': [{ type: Input },],
        'disabled': [{ type: Input },],
        'placeholder': [{ type: Input },],
    };
    return Select2Component;
}(CustomNgInput));
//# sourceMappingURL=select2.component.js.map