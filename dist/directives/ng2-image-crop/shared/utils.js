export var CSS_PREFIXES = ['Webkit', 'Moz', 'ms'];
export function vendorPrefix(prop) {
    var emptyStyles = document.createElement('div').style;
    if (prop in emptyStyles) {
        return prop;
    }
    var capProp = prop[0].toUpperCase() + prop.slice(1);
    var i = CSS_PREFIXES.length;
    while (i--) {
        prop = CSS_PREFIXES[i] + capProp;
        if (prop in emptyStyles) {
            return prop;
        }
    }
}
export var CSS_TRANSFORM = vendorPrefix('transform');
export var CSS_TRANS_ORG = vendorPrefix('transformOrigin');
export var CSS_USERSELECT = vendorPrefix('userSelect');
export function fix(v, decimalPoints) {
    return parseFloat(v).toFixed(decimalPoints || 0);
}
//# sourceMappingURL=utils.js.map