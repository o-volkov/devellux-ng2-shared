export var countries = {
    "AD": {
        "alpha2": "AD",
        "countryCallingCodes": [
            "+376"
        ],
        "name": "Andorra"
    },
    "AE": {
        "alpha2": "AE",
        "countryCallingCodes": [
            "+971"
        ],
        "name": "United Arab Emirates"
    },
    "AF": {
        "alpha2": "AF",
        "countryCallingCodes": [
            "+93"
        ],
        "name": "Afghanistan"
    },
    "AG": {
        "alpha2": "AG",
        "countryCallingCodes": [
            "+1 268"
        ],
        "name": "Antigua And Barbuda"
    },
    "AI": {
        "alpha2": "AI",
        "countryCallingCodes": [
            "+1 264"
        ],
        "name": "Anguilla"
    },
    "AL": {
        "alpha2": "AL",
        "countryCallingCodes": [
            "+355"
        ],
        "name": "Albania"
    },
    "AM": {
        "alpha2": "AM",
        "countryCallingCodes": [
            "+374"
        ],
        "name": "Armenia"
    },
    "AO": {
        "alpha2": "AO",
        "countryCallingCodes": [
            "+244"
        ],
        "name": "Angola"
    },
    "AQ": {
        "alpha2": "AQ",
        "countryCallingCodes": [
            "+672"
        ],
        "name": "Antarctica"
    },
    "AR": {
        "alpha2": "AR",
        "countryCallingCodes": [
            "+54"
        ],
        "name": "Argentina"
    },
    "AS": {
        "alpha2": "AS",
        "countryCallingCodes": [
            "+1 684"
        ],
        "name": "American Samoa"
    },
    "AT": {
        "alpha2": "AT",
        "countryCallingCodes": [
            "+43"
        ],
        "name": "Austria"
    },
    "AW": {
        "alpha2": "AW",
        "countryCallingCodes": [
            "+297"
        ],
        "name": "Aruba"
    },
    "AX": {
        "alpha2": "AX",
        "countryCallingCodes": [
            "+358"
        ],
        "name": "Åland Islands"
    },
    "AZ": {
        "alpha2": "AZ",
        "countryCallingCodes": [
            "+994"
        ],
        "name": "Azerbaijan"
    },
    "BA": {
        "alpha2": "BA",
        "countryCallingCodes": [
            "+387"
        ],
        "name": "Bosnia & Herzegovina"
    },
    "BB": {
        "alpha2": "BB",
        "countryCallingCodes": [
            "+1 246"
        ],
        "name": "Barbados"
    },
    "BD": {
        "alpha2": "BD",
        "countryCallingCodes": [
            "+880"
        ],
        "name": "Bangladesh"
    },
    "BE": {
        "alpha2": "BE",
        "countryCallingCodes": [
            "+32"
        ],
        "name": "Belgium"
    },
    "BF": {
        "alpha2": "BF",
        "countryCallingCodes": [
            "+226"
        ],
        "name": "Burkina Faso"
    },
    "BG": {
        "alpha2": "BG",
        "countryCallingCodes": [
            "+359"
        ],
        "name": "Bulgaria"
    },
    "BH": {
        "alpha2": "BH",
        "countryCallingCodes": [
            "+973"
        ],
        "name": "Bahrain"
    },
    "BI": {
        "alpha2": "BI",
        "countryCallingCodes": [
            "+257"
        ],
        "name": "Burundi"
    },
    "BJ": {
        "alpha2": "BJ",
        "countryCallingCodes": [
            "+229"
        ],
        "name": "Benin"
    },
    "BL": {
        "alpha2": "BL",
        "countryCallingCodes": [
            "+590"
        ],
        "name": "Saint Barthélemy"
    },
    "BM": {
        "alpha2": "BM",
        "countryCallingCodes": [
            "+1 441"
        ],
        "name": "Bermuda"
    },
    "BN": {
        "alpha2": "BN",
        "countryCallingCodes": [
            "+673"
        ],
        "name": "Brunei Darussalam"
    },
    "BO": {
        "alpha2": "BO",
        "countryCallingCodes": [
            "+591"
        ],
        "name": "Bolivia, Plurinational State Of"
    },
    "BQ": {
        "alpha2": "BQ",
        "countryCallingCodes": [
            "+599"
        ],
        "name": "Bonaire, Saint Eustatius And Saba"
    },
    "BR": {
        "alpha2": "BR",
        "countryCallingCodes": [
            "+55"
        ],
        "name": "Brazil"
    },
    "BS": {
        "alpha2": "BS",
        "countryCallingCodes": [
            "+1 242"
        ],
        "name": "Bahamas"
    },
    "BT": {
        "alpha2": "BT",
        "countryCallingCodes": [
            "+975"
        ],
        "name": "Bhutan"
    },
    "BV": {
        "alpha2": "BV",
        "countryCallingCodes": [],
        "name": "Bouvet Island"
    },
    "BW": {
        "alpha2": "BW",
        "countryCallingCodes": [
            "+267"
        ],
        "name": "Botswana"
    },
    "BY": {
        "alpha2": "BY",
        "countryCallingCodes": [
            "+375"
        ],
        "name": "Belarus"
    },
    "BZ": {
        "alpha2": "BZ",
        "countryCallingCodes": [
            "+501"
        ],
        "name": "Belize"
    },
    "CA": {
        "alpha2": "CA",
        "countryCallingCodes": [
            "+1"
        ],
        "name": "Canada"
    },
    "CC": {
        "alpha2": "CC",
        "countryCallingCodes": [
            "+61"
        ],
        "name": "Cocos (Keeling) Islands"
    },
    "CD": {
        "alpha2": "CD",
        "countryCallingCodes": [
            "+243"
        ],
        "name": "Democratic Republic Of Congo"
    },
    "CF": {
        "alpha2": "CF",
        "countryCallingCodes": [
            "+236"
        ],
        "name": "Central African Republic"
    },
    "CG": {
        "alpha2": "CG",
        "countryCallingCodes": [
            "+242"
        ],
        "name": "Republic Of Congo"
    },
    "CH": {
        "alpha2": "CH",
        "countryCallingCodes": [
            "+41"
        ],
        "name": "Switzerland"
    },
    "CI": {
        "alpha2": "CI",
        "countryCallingCodes": [
            "+225"
        ],
        "name": "Cote d'Ivoire"
    },
    "CK": {
        "alpha2": "CK",
        "countryCallingCodes": [
            "+682"
        ],
        "name": "Cook Islands"
    },
    "CL": {
        "alpha2": "CL",
        "countryCallingCodes": [
            "+56"
        ],
        "name": "Chile"
    },
    "CM": {
        "alpha2": "CM",
        "countryCallingCodes": [
            "+237"
        ],
        "name": "Cameroon"
    },
    "CN": {
        "alpha2": "CN",
        "countryCallingCodes": [
            "+86"
        ],
        "name": "China"
    },
    "CO": {
        "alpha2": "CO",
        "countryCallingCodes": [
            "+57"
        ],
        "name": "Colombia"
    },
    "CR": {
        "alpha2": "CR",
        "countryCallingCodes": [
            "+506"
        ],
        "name": "Costa Rica"
    },
    "CV": {
        "alpha2": "CV",
        "countryCallingCodes": [
            "+238"
        ],
        "name": "Cabo Verde"
    },
    "CW": {
        "alpha2": "CW",
        "countryCallingCodes": [
            "+599"
        ],
        "name": "Curacao"
    },
    "CX": {
        "alpha2": "CX",
        "countryCallingCodes": [
            "+61"
        ],
        "name": "Christmas Island"
    },
    "CY": {
        "alpha2": "CY",
        "countryCallingCodes": [
            "+357"
        ],
        "name": "Cyprus"
    },
    "CZ": {
        "alpha2": "CZ",
        "countryCallingCodes": [
            "+420"
        ],
        "name": "Czech Republic"
    },
    "DE": {
        "alpha2": "DE",
        "countryCallingCodes": [
            "+49"
        ],
        "name": "Germany"
    },
    "DJ": {
        "alpha2": "DJ",
        "countryCallingCodes": [
            "+253"
        ],
        "name": "Djibouti"
    },
    "DK": {
        "alpha2": "DK",
        "countryCallingCodes": [
            "+45"
        ],
        "name": "Denmark"
    },
    "DM": {
        "alpha2": "DM",
        "countryCallingCodes": [
            "+1 767"
        ],
        "name": "Dominica"
    },
    "DO": {
        "alpha2": "DO",
        "countryCallingCodes": [
            "+1 809",
            "+1 829",
            "+1 849"
        ],
        "name": "Dominican Republic"
    },
    "DZ": {
        "alpha2": "DZ",
        "countryCallingCodes": [
            "+213"
        ],
        "name": "Algeria"
    },
    "EC": {
        "alpha2": "EC",
        "countryCallingCodes": [
            "+593"
        ],
        "name": "Ecuador"
    },
    "EE": {
        "alpha2": "EE",
        "countryCallingCodes": [
            "+372"
        ],
        "name": "Estonia"
    },
    "EG": {
        "alpha2": "EG",
        "countryCallingCodes": [
            "+20"
        ],
        "name": "Egypt"
    },
    "EH": {
        "alpha2": "EH",
        "countryCallingCodes": [
            "+212"
        ],
        "name": "Western Sahara"
    },
    "ER": {
        "alpha2": "ER",
        "countryCallingCodes": [
            "+291"
        ],
        "name": "Eritrea"
    },
    "ES": {
        "alpha2": "ES",
        "countryCallingCodes": [
            "+34"
        ],
        "name": "Spain"
    },
    "ET": {
        "alpha2": "ET",
        "countryCallingCodes": [
            "+251"
        ],
        "name": "Ethiopia"
    },
    "FI": {
        "alpha2": "FI",
        "countryCallingCodes": [
            "+358"
        ],
        "name": "Finland"
    },
    "FJ": {
        "alpha2": "FJ",
        "countryCallingCodes": [
            "+679"
        ],
        "name": "Fiji"
    },
    "FK": {
        "alpha2": "FK",
        "countryCallingCodes": [
            "+500"
        ],
        "name": "Falkland Islands"
    },
    "FM": {
        "alpha2": "FM",
        "countryCallingCodes": [
            "+691"
        ],
        "name": "Micronesia, Federated States Of"
    },
    "FO": {
        "alpha2": "FO",
        "countryCallingCodes": [
            "+298"
        ],
        "name": "Faroe Islands"
    },
    "FR": {
        "alpha2": "FR",
        "countryCallingCodes": [
            "+33"
        ],
        "name": "France"
    },
    "GA": {
        "alpha2": "GA",
        "countryCallingCodes": [
            "+241"
        ],
        "name": "Gabon"
    },
    "GB": {
        "alpha2": "GB",
        "countryCallingCodes": [
            "+44"
        ],
        "name": "United Kingdom"
    },
    "GD": {
        "alpha2": "GD",
        "countryCallingCodes": [
            "+473"
        ],
        "name": "Grenada"
    },
    "GE": {
        "alpha2": "GE",
        "countryCallingCodes": [
            "+995"
        ],
        "name": "Georgia"
    },
    "GF": {
        "alpha2": "GF",
        "countryCallingCodes": [
            "+594"
        ],
        "name": "French Guiana"
    },
    "GG": {
        "alpha2": "GG",
        "countryCallingCodes": [
            "+44"
        ],
        "name": "Guernsey"
    },
    "GH": {
        "alpha2": "GH",
        "countryCallingCodes": [
            "+233"
        ],
        "name": "Ghana"
    },
    "GI": {
        "alpha2": "GI",
        "countryCallingCodes": [
            "+350"
        ],
        "name": "Gibraltar"
    },
    "GL": {
        "alpha2": "GL",
        "countryCallingCodes": [
            "+299"
        ],
        "name": "Greenland"
    },
    "GM": {
        "alpha2": "GM",
        "countryCallingCodes": [
            "+220"
        ],
        "name": "Gambia"
    },
    "GN": {
        "alpha2": "GN",
        "countryCallingCodes": [
            "+224"
        ],
        "name": "Guinea"
    },
    "GP": {
        "alpha2": "GP",
        "countryCallingCodes": [
            "+590"
        ],
        "name": "Guadeloupe"
    },
    "GQ": {
        "alpha2": "GQ",
        "countryCallingCodes": [
            "+240"
        ],
        "name": "Equatorial Guinea"
    },
    "GR": {
        "alpha2": "GR",
        "countryCallingCodes": [
            "+30"
        ],
        "name": "Greece"
    },
    "GS": {
        "alpha2": "GS",
        "countryCallingCodes": [],
        "name": "South Georgia And The South Sandwich Islands"
    },
    "GT": {
        "alpha2": "GT",
        "countryCallingCodes": [
            "+502"
        ],
        "name": "Guatemala"
    },
    "GW": {
        "alpha2": "GW",
        "countryCallingCodes": [
            "+245"
        ],
        "name": "Guinea-bissau"
    },
    "GY": {
        "alpha2": "GY",
        "countryCallingCodes": [
            "+592"
        ],
        "name": "Guyana"
    },
    "HK": {
        "alpha2": "HK",
        "countryCallingCodes": [
            "+852"
        ],
        "name": "Hong Kong"
    },
    "HM": {
        "alpha2": "HM",
        "countryCallingCodes": [],
        "name": "Heard Island And McDonald Islands"
    },
    "HN": {
        "alpha2": "HN",
        "countryCallingCodes": [
            "+504"
        ],
        "name": "Honduras"
    },
    "HR": {
        "alpha2": "HR",
        "countryCallingCodes": [
            "+385"
        ],
        "name": "Croatia"
    },
    "HT": {
        "alpha2": "HT",
        "countryCallingCodes": [
            "+509"
        ],
        "name": "Haiti"
    },
    "ID": {
        "alpha2": "ID",
        "countryCallingCodes": [
            "+62"
        ],
        "name": "Indonesia"
    },
    "IE": {
        "alpha2": "IE",
        "countryCallingCodes": [
            "+353"
        ],
        "name": "Ireland"
    },
    "IL": {
        "alpha2": "IL",
        "countryCallingCodes": [
            "+972"
        ],
        "name": "Israel"
    },
    "IM": {
        "alpha2": "IM",
        "countryCallingCodes": [
            "+44"
        ],
        "name": "Isle Of Man"
    },
    "IN": {
        "alpha2": "IN",
        "countryCallingCodes": [
            "+91"
        ],
        "name": "India"
    },
    "IO": {
        "alpha2": "IO",
        "countryCallingCodes": [
            "+246"
        ],
        "name": "British Indian Ocean Territory"
    },
    "IQ": {
        "alpha2": "IQ",
        "countryCallingCodes": [
            "+964"
        ],
        "name": "Iraq"
    },
    "IR": {
        "alpha2": "IR",
        "countryCallingCodes": [
            "+98"
        ],
        "name": "Iran, Islamic Republic Of"
    },
    "IS": {
        "alpha2": "IS",
        "countryCallingCodes": [
            "+354"
        ],
        "name": "Iceland"
    },
    "IT": {
        "alpha2": "IT",
        "countryCallingCodes": [
            "+39"
        ],
        "name": "Italy"
    },
    "JE": {
        "alpha2": "JE",
        "countryCallingCodes": [
            "+44"
        ],
        "name": "Jersey"
    },
    "JM": {
        "alpha2": "JM",
        "countryCallingCodes": [
            "+1 876"
        ],
        "name": "Jamaica"
    },
    "JO": {
        "alpha2": "JO",
        "countryCallingCodes": [
            "+962"
        ],
        "name": "Jordan"
    },
    "JP": {
        "alpha2": "JP",
        "countryCallingCodes": [
            "+81"
        ],
        "name": "Japan"
    },
    "KE": {
        "alpha2": "KE",
        "countryCallingCodes": [
            "+254"
        ],
        "name": "Kenya"
    },
    "KG": {
        "alpha2": "KG",
        "countryCallingCodes": [
            "+996"
        ],
        "name": "Kyrgyzstan"
    },
    "KH": {
        "alpha2": "KH",
        "countryCallingCodes": [
            "+855"
        ],
        "name": "Cambodia"
    },
    "KI": {
        "alpha2": "KI",
        "countryCallingCodes": [
            "+686"
        ],
        "name": "Kiribati"
    },
    "KM": {
        "alpha2": "KM",
        "countryCallingCodes": [
            "+269"
        ],
        "name": "Comoros"
    },
    "KN": {
        "alpha2": "KN",
        "countryCallingCodes": [
            "+1 869"
        ],
        "name": "Saint Kitts And Nevis"
    },
    "KP": {
        "alpha2": "KP",
        "countryCallingCodes": [
            "+850"
        ],
        "name": "Korea, Democratic People's Republic Of"
    },
    "KR": {
        "alpha2": "KR",
        "countryCallingCodes": [
            "+82"
        ],
        "name": "Korea, Republic Of"
    },
    "KW": {
        "alpha2": "KW",
        "countryCallingCodes": [
            "+965"
        ],
        "name": "Kuwait"
    },
    "KY": {
        "alpha2": "KY",
        "countryCallingCodes": [
            "+1 345"
        ],
        "name": "Cayman Islands"
    },
    "KZ": {
        "alpha2": "KZ",
        "countryCallingCodes": [
            "+7",
            "+7 6",
            "+7 7"
        ],
        "name": "Kazakhstan"
    },
    "LA": {
        "alpha2": "LA",
        "countryCallingCodes": [
            "+856"
        ],
        "name": "Lao People's Democratic Republic"
    },
    "LB": {
        "alpha2": "LB",
        "countryCallingCodes": [
            "+961"
        ],
        "name": "Lebanon"
    },
    "LC": {
        "alpha2": "LC",
        "countryCallingCodes": [
            "+1 758"
        ],
        "name": "Saint Lucia"
    },
    "LI": {
        "alpha2": "LI",
        "countryCallingCodes": [
            "+423"
        ],
        "name": "Liechtenstein"
    },
    "LK": {
        "alpha2": "LK",
        "countryCallingCodes": [
            "+94"
        ],
        "name": "Sri Lanka"
    },
    "LR": {
        "alpha2": "LR",
        "countryCallingCodes": [
            "+231"
        ],
        "name": "Liberia"
    },
    "LS": {
        "alpha2": "LS",
        "countryCallingCodes": [
            "+266"
        ],
        "name": "Lesotho"
    },
    "LT": {
        "alpha2": "LT",
        "countryCallingCodes": [
            "+370"
        ],
        "name": "Lithuania"
    },
    "LV": {
        "alpha2": "LV",
        "countryCallingCodes": [
            "+371"
        ],
        "name": "Latvia"
    },
    "LY": {
        "alpha2": "LY",
        "countryCallingCodes": [
            "+218"
        ],
        "name": "Libya"
    },
    "MA": {
        "alpha2": "MA",
        "countryCallingCodes": [
            "+212"
        ],
        "name": "Morocco"
    },
    "MC": {
        "alpha2": "MC",
        "countryCallingCodes": [
            "+377"
        ],
        "name": "Monaco"
    },
    "MD": {
        "alpha2": "MD",
        "countryCallingCodes": [
            "+373"
        ],
        "name": "Moldova"
    },
    "ME": {
        "alpha2": "ME",
        "countryCallingCodes": [
            "+382"
        ],
        "name": "Montenegro"
    },
    "MF": {
        "alpha2": "MF",
        "countryCallingCodes": [
            "+590"
        ],
        "name": "Saint Martin"
    },
    "MG": {
        "alpha2": "MG",
        "countryCallingCodes": [
            "+261"
        ],
        "name": "Madagascar"
    },
    "MH": {
        "alpha2": "MH",
        "countryCallingCodes": [
            "+692"
        ],
        "name": "Marshall Islands"
    },
    "MK": {
        "alpha2": "MK",
        "countryCallingCodes": [
            "+389"
        ],
        "name": "Macedonia, The Former Yugoslav Republic Of"
    },
    "ML": {
        "alpha2": "ML",
        "countryCallingCodes": [
            "+223"
        ],
        "name": "Mali"
    },
    "MM": {
        "alpha2": "MM",
        "countryCallingCodes": [
            "+95"
        ],
        "name": "Myanmar"
    },
    "MN": {
        "alpha2": "MN",
        "countryCallingCodes": [
            "+976"
        ],
        "name": "Mongolia"
    },
    "MO": {
        "alpha2": "MO",
        "countryCallingCodes": [
            "+853"
        ],
        "name": "Macao"
    },
    "MP": {
        "alpha2": "MP",
        "countryCallingCodes": [
            "+1 670"
        ],
        "name": "Northern Mariana Islands"
    },
    "MQ": {
        "alpha2": "MQ",
        "countryCallingCodes": [
            "+596"
        ],
        "name": "Martinique"
    },
    "MR": {
        "alpha2": "MR",
        "countryCallingCodes": [
            "+222"
        ],
        "name": "Mauritania"
    },
    "MS": {
        "alpha2": "MS",
        "countryCallingCodes": [
            "+1 664"
        ],
        "name": "Montserrat"
    },
    "MT": {
        "alpha2": "MT",
        "countryCallingCodes": [
            "+356"
        ],
        "name": "Malta"
    },
    "MV": {
        "alpha2": "MV",
        "countryCallingCodes": [
            "+960"
        ],
        "name": "Maldives"
    },
    "MW": {
        "alpha2": "MW",
        "countryCallingCodes": [
            "+265"
        ],
        "name": "Malawi"
    },
    "MX": {
        "alpha2": "MX",
        "countryCallingCodes": [
            "+52"
        ],
        "name": "Mexico"
    },
    "MY": {
        "alpha2": "MY",
        "countryCallingCodes": [
            "+60"
        ],
        "name": "Malaysia"
    },
    "MZ": {
        "alpha2": "MZ",
        "countryCallingCodes": [
            "+258"
        ],
        "name": "Mozambique"
    },
    "NA": {
        "alpha2": "NA",
        "countryCallingCodes": [
            "+264"
        ],
        "name": "Namibia"
    },
    "NC": {
        "alpha2": "NC",
        "countryCallingCodes": [
            "+687"
        ],
        "name": "New Caledonia"
    },
    "NE": {
        "alpha2": "NE",
        "countryCallingCodes": [
            "+227"
        ],
        "name": "Niger"
    },
    "NF": {
        "alpha2": "NF",
        "countryCallingCodes": [
            "+672"
        ],
        "name": "Norfolk Island"
    },
    "NG": {
        "alpha2": "NG",
        "countryCallingCodes": [
            "+234"
        ],
        "name": "Nigeria"
    },
    "NI": {
        "alpha2": "NI",
        "countryCallingCodes": [
            "+505"
        ],
        "name": "Nicaragua"
    },
    "NL": {
        "alpha2": "NL",
        "countryCallingCodes": [
            "+31"
        ],
        "name": "Netherlands"
    },
    "NO": {
        "alpha2": "NO",
        "countryCallingCodes": [
            "+47"
        ],
        "name": "Norway"
    },
    "NP": {
        "alpha2": "NP",
        "countryCallingCodes": [
            "+977"
        ],
        "name": "Nepal"
    },
    "NR": {
        "alpha2": "NR",
        "countryCallingCodes": [
            "+674"
        ],
        "name": "Nauru"
    },
    "NZ": {
        "alpha2": "NZ",
        "countryCallingCodes": [
            "+64"
        ],
        "name": "New Zealand"
    },
    "OM": {
        "alpha2": "OM",
        "countryCallingCodes": [
            "+968"
        ],
        "name": "Oman"
    },
    "PA": {
        "alpha2": "PA",
        "countryCallingCodes": [
            "+507"
        ],
        "name": "Panama"
    },
    "PE": {
        "alpha2": "PE",
        "countryCallingCodes": [
            "+51"
        ],
        "name": "Peru"
    },
    "PF": {
        "alpha2": "PF",
        "countryCallingCodes": [
            "+689"
        ],
        "name": "French Polynesia"
    },
    "PG": {
        "alpha2": "PG",
        "countryCallingCodes": [
            "+675"
        ],
        "name": "Papua New Guinea"
    },
    "PH": {
        "alpha2": "PH",
        "countryCallingCodes": [
            "+63"
        ],
        "name": "Philippines"
    },
    "PK": {
        "alpha2": "PK",
        "countryCallingCodes": [
            "+92"
        ],
        "name": "Pakistan"
    },
    "PL": {
        "alpha2": "PL",
        "countryCallingCodes": [
            "+48"
        ],
        "name": "Poland"
    },
    "PM": {
        "alpha2": "PM",
        "countryCallingCodes": [
            "+508"
        ],
        "name": "Saint Pierre And Miquelon"
    },
    "PN": {
        "alpha2": "PN",
        "countryCallingCodes": [
            "+872"
        ],
        "name": "Pitcairn"
    },
    "PR": {
        "alpha2": "PR",
        "countryCallingCodes": [
            "+1 787",
            "+1 939"
        ],
        "name": "Puerto Rico"
    },
    "PS": {
        "alpha2": "PS",
        "countryCallingCodes": [
            "+970"
        ],
        "name": "Palestinian Territory, Occupied"
    },
    "PT": {
        "alpha2": "PT",
        "countryCallingCodes": [
            "+351"
        ],
        "name": "Portugal"
    },
    "PW": {
        "alpha2": "PW",
        "countryCallingCodes": [
            "+680"
        ],
        "name": "Palau"
    },
    "PY": {
        "alpha2": "PY",
        "countryCallingCodes": [
            "+595"
        ],
        "name": "Paraguay"
    },
    "QA": {
        "alpha2": "QA",
        "countryCallingCodes": [
            "+974"
        ],
        "name": "Qatar"
    },
    "RE": {
        "alpha2": "RE",
        "countryCallingCodes": [
            "+262"
        ],
        "name": "Reunion"
    },
    "RO": {
        "alpha2": "RO",
        "countryCallingCodes": [
            "+40"
        ],
        "name": "Romania"
    },
    "RS": {
        "alpha2": "RS",
        "countryCallingCodes": [
            "+381"
        ],
        "name": "Serbia"
    },
    "RU": {
        "alpha2": "RU",
        "countryCallingCodes": [
            "+7",
            "+7 3",
            "+7 4",
            "+7 8"
        ],
        "name": "Russian Federation"
    },
    "RW": {
        "alpha2": "RW",
        "countryCallingCodes": [
            "+250"
        ],
        "name": "Rwanda"
    },
    "SA": {
        "alpha2": "SA",
        "countryCallingCodes": [
            "+966"
        ],
        "name": "Saudi Arabia"
    },
    "SB": {
        "alpha2": "SB",
        "countryCallingCodes": [
            "+677"
        ],
        "name": "Solomon Islands"
    },
    "SC": {
        "alpha2": "SC",
        "countryCallingCodes": [
            "+248"
        ],
        "name": "Seychelles"
    },
    "SD": {
        "alpha2": "SD",
        "countryCallingCodes": [
            "+249"
        ],
        "name": "Sudan"
    },
    "SE": {
        "alpha2": "SE",
        "countryCallingCodes": [
            "+46"
        ],
        "name": "Sweden"
    },
    "SG": {
        "alpha2": "SG",
        "countryCallingCodes": [
            "+65"
        ],
        "name": "Singapore"
    },
    "SH": {
        "alpha2": "SH",
        "countryCallingCodes": [
            "+290"
        ],
        "name": "Saint Helena, Ascension And Tristan Da Cunha"
    },
    "SI": {
        "alpha2": "SI",
        "countryCallingCodes": [
            "+386"
        ],
        "name": "Slovenia"
    },
    "SJ": {
        "alpha2": "SJ",
        "countryCallingCodes": [
            "+47"
        ],
        "name": "Svalbard And Jan Mayen"
    },
    "SK": {
        "alpha2": "SK",
        "countryCallingCodes": [
            "+421"
        ],
        "name": "Slovakia"
    },
    "SL": {
        "alpha2": "SL",
        "countryCallingCodes": [
            "+232"
        ],
        "name": "Sierra Leone"
    },
    "SM": {
        "alpha2": "SM",
        "countryCallingCodes": [
            "+378"
        ],
        "name": "San Marino"
    },
    "SN": {
        "alpha2": "SN",
        "countryCallingCodes": [
            "+221"
        ],
        "name": "Senegal"
    },
    "SO": {
        "alpha2": "SO",
        "countryCallingCodes": [
            "+252"
        ],
        "name": "Somalia"
    },
    "SR": {
        "alpha2": "SR",
        "countryCallingCodes": [
            "+597"
        ],
        "name": "Suriname"
    },
    "SS": {
        "alpha2": "SS",
        "countryCallingCodes": [
            "+211"
        ],
        "name": "South Sudan"
    },
    "ST": {
        "alpha2": "ST",
        "countryCallingCodes": [
            "+239"
        ],
        "name": "São Tomé and Príncipe"
    },
    "SV": {
        "alpha2": "SV",
        "countryCallingCodes": [
            "+503"
        ],
        "name": "El Salvador"
    },
    "SX": {
        "alpha2": "SX",
        "countryCallingCodes": [
            "+1 721"
        ],
        "name": "Sint Maarten"
    },
    "SY": {
        "alpha2": "SY",
        "countryCallingCodes": [
            "+963"
        ],
        "name": "Syrian Arab Republic"
    },
    "SZ": {
        "alpha2": "SZ",
        "countryCallingCodes": [
            "+268"
        ],
        "name": "Swaziland"
    },
    "TC": {
        "alpha2": "TC",
        "countryCallingCodes": [
            "+1 649"
        ],
        "name": "Turks And Caicos Islands"
    },
    "TD": {
        "alpha2": "TD",
        "countryCallingCodes": [
            "+235"
        ],
        "name": "Chad"
    },
    "TF": {
        "alpha2": "TF",
        "countryCallingCodes": [],
        "name": "French Southern Territories"
    },
    "TG": {
        "alpha2": "TG",
        "countryCallingCodes": [
            "+228"
        ],
        "name": "Togo"
    },
    "TH": {
        "alpha2": "TH",
        "countryCallingCodes": [
            "+66"
        ],
        "name": "Thailand"
    },
    "TJ": {
        "alpha2": "TJ",
        "countryCallingCodes": [
            "+992"
        ],
        "name": "Tajikistan"
    },
    "TK": {
        "alpha2": "TK",
        "countryCallingCodes": [
            "+690"
        ],
        "name": "Tokelau"
    },
    "TL": {
        "alpha2": "TL",
        "countryCallingCodes": [
            "+670"
        ],
        "name": "Timor-Leste, Democratic Republic of"
    },
    "TM": {
        "alpha2": "TM",
        "countryCallingCodes": [
            "+993"
        ],
        "name": "Turkmenistan"
    },
    "TN": {
        "alpha2": "TN",
        "countryCallingCodes": [
            "+216"
        ],
        "name": "Tunisia"
    },
    "TO": {
        "alpha2": "TO",
        "countryCallingCodes": [
            "+676"
        ],
        "name": "Tonga"
    },
    "TR": {
        "alpha2": "TR",
        "countryCallingCodes": [
            "+90"
        ],
        "name": "Turkey"
    },
    "TT": {
        "alpha2": "TT",
        "countryCallingCodes": [
            "+1 868"
        ],
        "name": "Trinidad And Tobago"
    },
    "TV": {
        "alpha2": "TV",
        "countryCallingCodes": [
            "+688"
        ],
        "name": "Tuvalu"
    },
    "TW": {
        "alpha2": "TW",
        "countryCallingCodes": [
            "+886"
        ],
        "name": "Taiwan"
    },
    "TZ": {
        "alpha2": "TZ",
        "countryCallingCodes": [
            "+255"
        ],
        "name": "Tanzania, United Republic Of"
    },
    "UA": {
        "alpha2": "UA",
        "countryCallingCodes": [
            "+380"
        ],
        "name": "Ukraine"
    },
    "UG": {
        "alpha2": "UG",
        "countryCallingCodes": [
            "+256"
        ],
        "name": "Uganda"
    },
    "UM": {
        "alpha2": "UM",
        "countryCallingCodes": [
            "+1"
        ],
        "name": "United States Minor Outlying Islands"
    },
    "US": {
        "alpha2": "US",
        "countryCallingCodes": [
            "+1"
        ],
        "name": "United States"
    },
    "UY": {
        "alpha2": "UY",
        "countryCallingCodes": [
            "+598"
        ],
        "name": "Uruguay"
    },
    "UZ": {
        "alpha2": "UZ",
        "countryCallingCodes": [
            "+998"
        ],
        "name": "Uzbekistan"
    },
    "VA": {
        "alpha2": "VA",
        "countryCallingCodes": [
            "+379",
            "+39"
        ],
        "name": "Vatican City State"
    },
    "VC": {
        "alpha2": "VC",
        "countryCallingCodes": [
            "+1 784"
        ],
        "name": "Saint Vincent And The Grenadines"
    },
    "VE": {
        "alpha2": "VE",
        "countryCallingCodes": [
            "+58"
        ],
        "name": "Venezuela, Bolivarian Republic Of"
    },
    "VG": {
        "alpha2": "VG",
        "countryCallingCodes": [
            "+1 284"
        ],
        "name": "Virgin Islands (British)"
    },
    "VI": {
        "alpha2": "VI",
        "countryCallingCodes": [
            "+1 340"
        ],
        "name": "Virgin Islands (US)"
    },
    "VN": {
        "alpha2": "VN",
        "countryCallingCodes": [
            "+84"
        ],
        "name": "Viet Nam"
    },
    "WF": {
        "alpha2": "WF",
        "countryCallingCodes": [
            "+681"
        ],
        "name": "Wallis And Futuna"
    },
    "WS": {
        "alpha2": "WS",
        "countryCallingCodes": [
            "+685"
        ],
        "name": "Samoa"
    },
    "YE": {
        "alpha2": "YE",
        "countryCallingCodes": [
            "+967"
        ],
        "name": "Yemen"
    },
    "YT": {
        "alpha2": "YT",
        "countryCallingCodes": [
            "+262"
        ],
        "name": "Mayotte"
    },
    "ZA": {
        "alpha2": "ZA",
        "countryCallingCodes": [
            "+27"
        ],
        "name": "South Africa"
    },
    "ZM": {
        "alpha2": "ZM",
        "countryCallingCodes": [
            "+260"
        ],
        "name": "Zambia"
    },
    "ZW": {
        "alpha2": "ZW",
        "countryCallingCodes": [
            "+263"
        ],
        "name": "Zimbabwe"
    }
};
//# sourceMappingURL=countries.js.map