import { Component, Input } from '@angular/core';

@Component({
  selector: 'order-state-renderer',
  template: `
    <span 
      [class.available]="orderState == 'Available'"
      [class.bidding]="orderState == 'Bidding'"
      [class.in_progress]="orderState == 'In Progress'"
      [class.finished]="orderState == 'Finished'"
      [class.cancelled]="orderState == 'Canceled'"
    >{{orderState}}</span>`,
  styles: [`.bidding{
    color: #0082D5;
}
.in_progress{
    color: #ff9948;
}
.finished{
    color: #039547;
}
.cancelled{
    color: #d2335c;
}
    /* LIGHT */
:host(.light) .bidding{
    color: #81BBFF;
}
:host(.light) .in_progress{
    color: #FFAA59;
}
:host(.light) .finished{
    color: #04BE5B;
}
:host(.light) .cancelled{
    color: #F56185;
}`]
})
export class OrderStateRendererComponent {
  private state;

  @Input()
  set orderState(state) {
    this.state = state;
  }

  get orderState() {
    if (!this.state) {
      return '';
    }

    switch (this.state) {
      case 'available':
        return 'Available';
      case 'bidding':
        return 'Bidding';
      case 'in_progress':
        return 'In Progress';
      case 'finished':
        return 'Finished';
      case 'cancelled':
        return 'Canceled';
      case 'canceled':
        return 'Canceled';
    }
  }

}
