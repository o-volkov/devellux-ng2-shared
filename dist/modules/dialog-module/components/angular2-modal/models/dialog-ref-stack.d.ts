import { DialogRef } from './dialog-ref';
export declare class DialogRefStack<T> {
    private _stack;
    private _stackMap;
    readonly length: number;
    constructor();
    push(dialogRef: DialogRef<T>, group?: any): void;
    pushManaged(dialogRef: DialogRef<T>, group?: any): void;
    pop(): DialogRef<T>;
    remove(dialogRef: DialogRef<T>): void;
    index(index: number): DialogRef<T>;
    indexOf(dialogRef: DialogRef<T>): number;
    groupOf(dialogRef: DialogRef<T>): any;
    groupBy(group: any): DialogRef<T>[];
    groupLength(group: any): number;
}
