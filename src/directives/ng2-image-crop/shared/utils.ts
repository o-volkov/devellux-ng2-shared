export const CSS_PREFIXES = ['Webkit', 'Moz', 'ms'];

export function vendorPrefix(prop: string): string {
  let emptyStyles = document.createElement('div').style;
  if (prop in emptyStyles) {
    return prop;
  }

  let capProp = prop[0].toUpperCase() + prop.slice(1);
  let i = CSS_PREFIXES.length;

  while (i--) {
    prop = CSS_PREFIXES[i] + capProp;
    if (prop in emptyStyles) {
      return prop;
    }
  }
}

export const CSS_TRANSFORM = vendorPrefix('transform');
export const CSS_TRANS_ORG = vendorPrefix('transformOrigin');
export const CSS_USERSELECT = vendorPrefix('userSelect');

export function fix(v: any, decimalPoints?: number): string {
  return parseFloat(v).toFixed(decimalPoints || 0);
}
