import { ControlValueAccessor } from "@angular/forms";
import { CustomNgInput } from "../../components/custom_inputs/custom-ng2-input";
import { DialogService } from "../../modules/dialog-module/dialog.module";
export declare class DatetimePickerModalComponent extends CustomNgInput implements ControlValueAccessor {
    private modal;
    constructor(modal: DialogService);
    valueView: string;
    updateViewValue(v: any): void;
    onDeadlineClick(): void;
}
