import { Component, ViewEncapsulation } from '@angular/core';
import { FileUploader } from './shared/index';
var MAX_FILE_SIZE = 2048000;
var MAX_FILES_COUNT = 10;
var URL = 'http://localhost:8081';
export var Ng2FileUploaderComponent = (function () {
    function Ng2FileUploaderComponent() {
        this.uploader = new FileUploader({ url: URL });
        this.hasDropZoneOver = false;
        var uploaderOptions = {
            maxFileSize: MAX_FILE_SIZE,
            queueLimit: MAX_FILES_COUNT,
        };
        this.uploader.setOptions(uploaderOptions);
        this.uploader.onAfterAddingFile = this.fileSelected.bind(this);
    }
    Ng2FileUploaderComponent.prototype.fileOver = function (event) {
        this.hasDropZoneOver = event;
    };
    Ng2FileUploaderComponent.prototype.fileSelected = function (fileItem) {
        this.fileItem = fileItem;
        return { fileItem: fileItem };
    };
    Ng2FileUploaderComponent.decorators = [
        { type: Component, args: [{
                    selector: 'ng2-file-uploader',
                    template: "<template [ngIf]=\"uploader && uploader.queue && uploader.queue.length == 0\">\n  <div class=\"drag-and-drop__block\"\n       ng2FileDrop\n       [uploader]=\"uploader\"\n       (fileOver)=\"fileOver($event)\"\n       [class.nv-file-over]=\"hasDropZoneOver\">\n    <i class=\"icon icon__drag-and-drop\"></i>\n    <div class=\"drag-and-drop__title\">Drag & Drop<br>your files here</div>\n  </div>\n  <div class=\"flex__container flex__j-center\">\n    <input type=\"file\" ng2FileSelect [uploader]=\"uploader\" />\n  </div>\n</template>\n\n<template [ngIf]=\"uploader.queue.length > 0\">\n  <h3>Files count {{uploader.queue.length}}</h3>\n</template>",
                    styles: [".nv-file-over {\n  border: dotted 3px red;\n}"],
                    encapsulation: ViewEncapsulation.None,
                },] },
    ];
    Ng2FileUploaderComponent.ctorParameters = [];
    return Ng2FileUploaderComponent;
}());
//# sourceMappingURL=ng2-file-uploader.component.js.map