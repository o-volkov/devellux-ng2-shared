import { PipeTransform } from '@angular/core';
export declare class BytesFormatPipe implements PipeTransform {
    transform(bytes: any, precision?: number): string;
}
