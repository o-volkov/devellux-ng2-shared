import {Component, Input, forwardRef} from "@angular/core";
import {NG_VALUE_ACCESSOR, ControlValueAccessor} from "@angular/forms";
import {CustomNgInput} from "../custom-ng2-input";


const CUSTOM_INPUT_CONTROL_VALUE_ACCESSOR = {
  provide: NG_VALUE_ACCESSOR,
  useExisting: forwardRef(() => Select2Component),
  multi: true
};

@Component({
  selector: 'select2',
  template: `<ng-select [allowClear]="true"
              [items]="items"
              [active]="viewValue"
              [disabled]="disabled"
              (selected)="selectOption($event)"
              [placeholder]="placeholder"
              >
  </ng-select>`,
  // (removed)="removed($event)"
  // (typed)="typed($event)"
  // (data)="refreshValue($event)"
  providers: [CUSTOM_INPUT_CONTROL_VALUE_ACCESSOR],
})
export class Select2Component extends CustomNgInput implements ControlValueAccessor {
  @Input() options:Select2Option[] = [];
  @Input() disabled:boolean = false;
  @Input() placeholder:string = 'some';

  viewValue;
  private items = [];

  ngOnInit() {
    // convert to ng-select items
    this.items = this.options.map((o:Select2Option) => ({id: o.value, text: o.title}));
  }

  selectOption(value) {
    this.value = value.id;
    this.touched();
  }

  onTouched() {
    this.touched();
  }


  updateViewValue(v) {
    let view = this.items.find(item => item.id === v);
    if (view) {
      this.viewValue = [view];
    }
  }

}

export interface Select2Option {
  title;
  value;
}
