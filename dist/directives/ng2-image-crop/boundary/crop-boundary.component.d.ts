import { EventEmitter, AfterViewInit, ChangeDetectorRef, ElementRef, Renderer } from "@angular/core";
import { CropBoundaryImageDirective } from "./image/index";
import { CropBoundaryOverlayDirective } from "./overlay/index";
import { CropBoundaryViewportDirective } from "./viewport/index";
export declare class CropBoundaryComponent implements AfterViewInit {
    private element;
    private renderer;
    private changeDetectionRef;
    _zoomValue: number;
    private mouseMoveListenFuncs;
    private mouseUpListenFuncs;
    minZoom: number;
    maxZoom: number;
    private isDragging;
    private originalX;
    private originalY;
    private originalDistance;
    private vpRect;
    private transform;
    imageDirective: CropBoundaryImageDirective;
    overlayDirective: CropBoundaryOverlayDirective;
    viewportDirective: CropBoundaryViewportDirective;
    zoomChanged: EventEmitter<number>;
    width: number;
    height: number;
    options: {
        image: {};
        viewport: {
            width: number;
            height: number;
        };
        mouseWheelZoom: boolean;
        update: Function;
    };
    constructor(element: ElementRef, renderer: Renderer, changeDetectionRef: ChangeDetectorRef);
    ngAfterViewInit(): void;
    image: string;
    zoomValue: number;
    private _onZoom(ui);
    _updateOverlay(): void;
    _triggerUpdate(): void;
    _isVisible(): boolean;
    _getVirtualBoundaries(viewport: any): {
        translate: {
            maxX: number;
            minX: number;
            maxY: number;
            minY: number;
        };
        origin: {
            maxX: number;
            minX: number;
            maxY: number;
            minY: number;
        };
    };
    _get(): {
        points: [any];
        zoom: number;
    };
    getBoundingClientRect(): ClientRect;
    mouseDown(ev: any): void;
    mouseMove(ev: any): void;
    mouseUp(): void;
    _initDraggable(): void;
    _updateCenterPoint(): void;
    _bindPoints(points: any): void;
    _centerImage(): void;
    _initializeZoom(): void;
    scroll(ev: any): void;
}
