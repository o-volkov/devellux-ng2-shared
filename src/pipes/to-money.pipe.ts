import {Pipe, PipeTransform, Inject, LOCALE_ID} from "@angular/core";
import {CurrencyPipe} from "@angular/common";

@Pipe({name: 'toMoney'})
export class ToMoneyPipe implements PipeTransform {
  private cp = new CurrencyPipe(this.locale);

  constructor(@Inject(LOCALE_ID) private locale) {
  }

  transform(value): string {
    if (Number.isInteger(parseInt(value))) {
      value = value / 100;
    } else {
      value = 0;
    }

    return this.cp.transform(value, 'USD', true, '1.2-2');

  }

}
