import { ModalOpenContext, ModalOpenContextBuilder, FluentAssignMethod } from '../../../../components/angular2-modal';
export declare type BootstrapModalSize = 'sm' | 'lg';
export declare class BSModalContext extends ModalOpenContext {
    dialogClass: string;
    size: BootstrapModalSize;
    showClose: boolean;
    normalize(): void;
}
export declare class BSModalContextBuilder<T extends BSModalContext> extends ModalOpenContextBuilder<T> {
    size: FluentAssignMethod<string, this>;
    dialogClass: FluentAssignMethod<BootstrapModalSize, this>;
    showClose: FluentAssignMethod<boolean, this>;
    constructor(defaultValues?: T, initialSetters?: string[], baseType?: any);
}
