import { ModuleWithProviders } from "@angular/core";
import { Modal, ModalModule, ModalComponent, Overlay } from "./components/angular2-modal/index";
import { BootstrapModalModule } from "./components/angular2-modal/plugins/bootstrap/bootstrap.module";
import { DialogRef } from "./components/angular2-modal/models/dialog-ref";
export declare class DialogService {
    private modal;
    constructor(modal: Modal);
    open<CMP, DATA>(component: {
        new (...args: any[]): CMP & DATA;
    }, data: DATA): Promise<any>;
}
export declare function bootstrapDialogModule(): any[];
export declare class DialogModule {
    static forRoot(): ModuleWithProviders;
}
export { ModalModule, BootstrapModalModule, DialogRef, ModalComponent, Overlay };
