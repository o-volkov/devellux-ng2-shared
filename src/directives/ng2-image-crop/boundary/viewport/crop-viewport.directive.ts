import { Directive, Input, HostBinding, ElementRef, Renderer } from '@angular/core';


@Directive({
  selector: '[crop-boundary-viewport]',
})
export class CropBoundaryViewportDirective {
  // @Input() options: { width: number, height: number };
  @HostBinding('style.width.px')
  @Input() width: number;
  @HostBinding('style.height.px')
  @Input() height: number;

  constructor(public element: ElementRef, public renderer: Renderer) {
  }
  //
  // @HostBinding('style.width.px')
  // get styleWidthPx() {
  //   return this.options.width;
  // }
  //
  // @HostBinding('style.height.px')
  // get styleHeightPx() {
  //   return this.options.height;
  // }

  public getBoundingClientRect(): ClientRect {
    return this.element.nativeElement.getBoundingClientRect();
  }
}
