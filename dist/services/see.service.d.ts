import { OpaqueToken } from "@angular/core";
import { Observable } from "rxjs/Observable";
import "rxjs/add/operator/map";
import "rxjs/add/operator/catch";
import { User } from "./user.service";
export declare const SSE_URL_TOKEN: OpaqueToken;
export declare class SSEService {
    private user;
    private sseUrl;
    private source;
    private observables;
    constructor(user: User, sseUrl: any);
    connect(eventName: string): Observable<any>;
    private createSource();
    private clearObservables();
}
