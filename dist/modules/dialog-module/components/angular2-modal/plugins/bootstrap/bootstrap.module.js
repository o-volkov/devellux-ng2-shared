import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ModalModule, Modal as BaseModal } from '../../../../components/angular2-modal';
import { Modal } from './modal';
import { BSModalContainer } from './modal-container.component';
import { BSMessageModal, BSMessageModalTitle, BSMessageModalBody, BSModalFooter } from './message-modal.component';
export function getProviders() {
    return [
        { provide: BaseModal, useClass: Modal },
        { provide: Modal, useClass: Modal }
    ];
}
export var BootstrapModalModule = (function () {
    function BootstrapModalModule() {
    }
    BootstrapModalModule.getProviders = function () {
        return getProviders();
    };
    BootstrapModalModule.decorators = [
        { type: NgModule, args: [{
                    imports: [ModalModule, CommonModule],
                    declarations: [
                        BSModalFooter,
                        BSMessageModalTitle,
                        BSMessageModalBody,
                        BSMessageModal,
                        BSModalContainer
                    ],
                    providers: getProviders(),
                    entryComponents: [
                        BSModalContainer,
                        BSMessageModal
                    ]
                },] },
    ];
    BootstrapModalModule.ctorParameters = [];
    return BootstrapModalModule;
}());
//# sourceMappingURL=bootstrap.module.js.map