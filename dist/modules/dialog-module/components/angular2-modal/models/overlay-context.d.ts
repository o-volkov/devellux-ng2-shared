import { FluentAssign, FluentAssignMethod } from './../framework/fluent-assign';
import { DialogRef } from './dialog-ref';
import { WideVCRef, OverlayConfig } from './tokens';
export declare const DEFAULT_VALUES: {
    inElement: boolean;
    isBlocking: boolean;
    keyboard: number[];
    supportsKey: (keyCode: number) => boolean;
};
export declare class OverlayContext {
    inElement: boolean;
    isBlocking: boolean;
    keyboard: Array<number> | number;
    normalize(): void;
}
export declare class OverlayContextBuilder<T extends OverlayContext> extends FluentAssign<T> {
    inElement: FluentAssignMethod<boolean, this>;
    isBlocking: FluentAssignMethod<boolean, this>;
    keyboard: FluentAssignMethod<Array<number> | number, this>;
    constructor(defaultValues?: T | T[], initialSetters?: string[], baseType?: new () => T);
    toOverlayConfig(base?: OverlayConfig): OverlayConfig;
}
export interface ModalControllingContextBuilder<T> {
    open(viewContainer?: WideVCRef): Promise<DialogRef<T>>;
}
export declare function overlayConfigFactory<T>(context: T, baseContextType?: any, baseConfig?: OverlayConfig): OverlayConfig;
