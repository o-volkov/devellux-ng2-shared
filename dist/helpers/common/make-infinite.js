import { URLSearchParams } from "@angular/http";
import { Subject } from "rxjs/Subject";
export function makeInfinite(func, opts) {
    var params = new URLSearchParams();
    Object.keys(opts).forEach(function (key) {
        if (opts[key]) {
            params.set(key, opts[key]);
        }
    });
    var skip = 0;
    var hasMore = true;
    var result = new Subject();
    var isFirstRun = true;
    function loadMore() {
        var _this = this;
        if (isFirstRun && this) {
            if (Array.isArray(this.objects)) {
                this.objects.length = 0;
            }
            else {
                this.objects = [];
            }
            isFirstRun = false;
        }
        if (!hasMore) {
            return;
        }
        params.set('skip', skip.toString());
        return func(params)
            .subscribe(function (res) {
            skip = skip + res.objects.length;
            hasMore = skip < res.total;
            if (_this) {
                _this.hasMore = hasMore;
                _this.objects.push.apply(_this.objects, res.objects);
            }
            result.next(res.objects);
            if (!hasMore) {
                result.complete();
            }
        });
    }
    return { result: result, loadMore: loadMore, hasMore: true };
}
//# sourceMappingURL=make-infinite.js.map