import { Directive, EventEmitter, ElementRef, HostListener, Input, Output } from '@angular/core';
export var FileDropDirective = (function () {
    function FileDropDirective(element) {
        this.fileOver = new EventEmitter();
        this.onFileDrop = new EventEmitter();
        this.element = element;
    }
    FileDropDirective.prototype.getOptions = function () {
        return this.uploader.options;
    };
    FileDropDirective.prototype.getFilters = function () {
        return {};
    };
    FileDropDirective.prototype.onDrop = function (event) {
        var transfer = this._getTransfer(event);
        if (!transfer) {
            return;
        }
        var options = this.getOptions();
        var filters = this.getFilters();
        this._preventAndStop(event);
        this.uploader.addToQueue(transfer.files, options, filters);
        this.fileOver.emit(false);
        this.onFileDrop.emit(transfer.files);
    };
    FileDropDirective.prototype.onDragOver = function (event) {
        var transfer = this._getTransfer(event);
        if (!this._haveFiles(transfer.types)) {
            return;
        }
        transfer.dropEffect = 'copy';
        this._preventAndStop(event);
        this.fileOver.emit(true);
        clearTimeout(this.timeout);
    };
    FileDropDirective.prototype.onDragLeave = function (event) {
        if (event.currentTarget === this.element[0]) {
            return;
        }
        this._preventAndStop(event);
        this.fileOver.emit(false);
        clearTimeout(this.timeout);
    };
    FileDropDirective.prototype._getTransfer = function (event) {
        return event.dataTransfer ? event.dataTransfer : event.originalEvent.dataTransfer;
    };
    FileDropDirective.prototype._preventAndStop = function (event) {
        event.preventDefault();
        event.stopPropagation();
    };
    FileDropDirective.prototype._haveFiles = function (types) {
        if (!types) {
            return false;
        }
        if (types.indexOf) {
            return types.indexOf('Files') !== -1;
        }
        else if (types.contains) {
            return types.contains('Files');
        }
        else {
            return false;
        }
    };
    FileDropDirective.decorators = [
        { type: Directive, args: [{ selector: '[ng2FileDrop]' },] },
    ];
    FileDropDirective.ctorParameters = [
        { type: ElementRef, },
    ];
    FileDropDirective.propDecorators = {
        'uploader': [{ type: Input },],
        'fileOver': [{ type: Output },],
        'onFileDrop': [{ type: Output },],
        'onDrop': [{ type: HostListener, args: ['document:drop', ['$event'],] },],
        'onDragOver': [{ type: HostListener, args: ['document:dragover', ['$event'],] },],
        'onDragLeave': [{ type: HostListener, args: ['document:dragleave', ['$event'],] },],
    };
    return FileDropDirective;
}());
//# sourceMappingURL=file-drop.directive.js.map