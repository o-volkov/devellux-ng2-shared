import { FormControl, FormGroup, ValidatorFn } from "@angular/forms";
export declare class CommonValidators {
    static isEmail(control: FormControl): {
        isEmail: boolean;
    };
    static isPhone(control: FormControl): {
        isPhone: boolean;
    };
    static isMatch(first: any, second: any): (group: FormGroup) => {
        [key: string]: any;
    };
    static isNickname(control: FormControl): any;
    static isName(control: FormControl): {
        isName: boolean;
    };
    static isCreditCard(control: FormControl): {
        isCreditCard: boolean;
    };
    static isAmount(control: FormControl): {
        isAmount: boolean;
    };
    static minAmount(control: FormControl): {
        minAmount: boolean;
    };
    static maxAmount(control: FormControl): {
        maxAmount: boolean;
    };
    static isCVV(control: FormControl): {
        isCVV: boolean;
    };
    static isMMYY(control: FormControl): {
        isMMYY: boolean;
    };
    static isOverTime(control: FormControl): {
        isOverTime: boolean;
    };
    static creditCard(control: FormControl): boolean;
}
export declare function minNumberValidator(minNumber: number): ValidatorFn;
