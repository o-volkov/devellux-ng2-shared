import { EventEmitter } from "@angular/core";
import { GoogleService } from "./google.service";
export declare class GoogleButtonDirective {
    private service;
    token: EventEmitter<any>;
    constructor(service: GoogleService);
    getFacebookToken(): void;
}
