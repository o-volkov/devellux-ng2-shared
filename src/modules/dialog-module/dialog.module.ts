import {NgModule, Injectable, ModuleWithProviders} from "@angular/core";
import {Modal, OverlayConfig, ModalModule, ModalComponent, Overlay} from "./components/angular2-modal/index";
import {BSModalContextBuilder, BSModalContext} from "./components/angular2-modal/plugins/bootstrap/modal-context";
import {BootstrapModalModule} from "./components/angular2-modal/plugins/bootstrap/bootstrap.module";
import {DialogRef} from "./components/angular2-modal/models/dialog-ref";
import {OverlayRenderer} from "./components/angular2-modal/index";
import {DOMOverlayRenderer} from "./components/angular2-modal/index";
import {EVENT_MANAGER_PLUGINS} from "@angular/platform-browser";
import {DOMOutsideEventPlugin} from "./components/angular2-modal/providers/outside-event-plugin";

@Injectable()
export class DialogService {
  constructor(private modal:Modal) {
  }

  open<CMP,DATA>(component:{ new(...args:any[]):CMP&DATA}, data:DATA):Promise<any> {
    const builder = new BSModalContextBuilder<BSModalContext>(
      data as any,
      undefined,
      BSModalContext
    );
    let overlayConfig:OverlayConfig = {
      context: builder.toJSON()
    };
    return this.modal.open(component, overlayConfig).then(ref=> {
      return ref.result;
    });
  }
}



export function bootstrapDialogModule(){
   return BootstrapModalModule.getProviders()
}

@NgModule({
  imports: [
    ModalModule,
    BootstrapModalModule
  ],
  providers: [
    DialogService,
    bootstrapDialogModule()
  ],
})
export class DialogModule {
  static forRoot():ModuleWithProviders{
    return{
      ngModule:DialogModule,
      providers: [
        Overlay,
        {provide: OverlayRenderer, useClass: DOMOverlayRenderer},
        {provide: EVENT_MANAGER_PLUGINS, useClass: DOMOutsideEventPlugin, multi: true},
      ]
    }
  }

}

export {ModalModule, BootstrapModalModule, DialogRef, ModalComponent, Overlay}
