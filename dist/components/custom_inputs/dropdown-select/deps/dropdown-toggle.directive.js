import { Directive, ElementRef, Host, Input, HostBinding, HostListener } from '@angular/core';
import { DropdownDirective } from './dropdown.directive';
export var DropdownToggleDirective = (function () {
    function DropdownToggleDirective(dropdown, el) {
        this.disabled = false;
        this.addClass = true;
        this.dropdown = dropdown;
        this.el = el;
    }
    DropdownToggleDirective.prototype.ngOnInit = function () {
        this.dropdown.dropDownToggle = this;
    };
    Object.defineProperty(DropdownToggleDirective.prototype, "isOpen", {
        get: function () {
            return this.dropdown.isOpen;
        },
        enumerable: true,
        configurable: true
    });
    DropdownToggleDirective.prototype.toggleDropdown = function (event) {
        event.stopPropagation();
        if (!this.disabled) {
            this.dropdown.toggle();
        }
        return false;
    };
    DropdownToggleDirective.decorators = [
        { type: Directive, args: [{ selector: '[dropdownToggle]' },] },
    ];
    DropdownToggleDirective.ctorParameters = [
        { type: DropdownDirective, decorators: [{ type: Host },] },
        { type: ElementRef, },
    ];
    DropdownToggleDirective.propDecorators = {
        'disabled': [{ type: HostBinding, args: ['class.disabled',] }, { type: Input },],
        'addClass': [{ type: HostBinding, args: ['class.dropdown-select-toggle',] }, { type: HostBinding, args: ['attr.aria-haspopup',] },],
        'isOpen': [{ type: HostBinding, args: ['attr.aria-expanded',] },],
        'toggleDropdown': [{ type: HostListener, args: ['click', ['$event'],] },],
    };
    return DropdownToggleDirective;
}());
//# sourceMappingURL=dropdown-toggle.directive.js.map