import { Component, Input, Output, EventEmitter, HostBinding, ChangeDetectorRef, ViewEncapsulation, ViewChild, ElementRef, Renderer } from "@angular/core";
import { CropBoundaryImageDirective } from "./image/index";
import { CropBoundaryOverlayDirective } from "./overlay/index";
import { CropBoundaryViewportDirective } from "./viewport/index";
import { ImageTransform, ImageTransformOrigin } from "../shared/index";
import * as Utils from "../shared/utils";
export var CropBoundaryComponent = (function () {
    function CropBoundaryComponent(element, renderer, changeDetectionRef) {
        this.element = element;
        this.renderer = renderer;
        this.changeDetectionRef = changeDetectionRef;
        this._zoomValue = 1;
        this.mouseMoveListenFuncs = [];
        this.mouseUpListenFuncs = [];
        this.minZoom = 0;
        this.maxZoom = 1.5;
        this.isDragging = false;
        this.zoomChanged = new EventEmitter();
        this.width = 330;
        this.height = 175;
        this.options = {
            image: {},
            viewport: { width: 100, height: 100 },
            mouseWheelZoom: true,
            update: function () { }
        };
        renderer.setElementClass(element.nativeElement, 'image-crop-boundary', true);
    }
    CropBoundaryComponent.prototype.ngAfterViewInit = function () {
        this._initDraggable();
        if (this.options.mouseWheelZoom) {
            this._initializeZoom();
        }
    };
    Object.defineProperty(CropBoundaryComponent.prototype, "image", {
        set: function (image) {
            this.imageDirective.element.nativeElement.src = image;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(CropBoundaryComponent.prototype, "zoomValue", {
        get: function () {
            return this._zoomValue;
        },
        set: function (value) {
            this._zoomValue = value;
            this._onZoom({
                value: this.zoomValue,
                origin: new ImageTransformOrigin(this.imageDirective.element.nativeElement),
                viewportRect: this.viewportDirective.getBoundingClientRect(),
                transform: ImageTransform.parse(this.imageDirective.element.nativeElement)
            });
            this.changeDetectionRef.detectChanges();
        },
        enumerable: true,
        configurable: true
    });
    CropBoundaryComponent.prototype._onZoom = function (ui) {
        var transform = ui ?
            ui.transform : ImageTransform.parse(this.imageDirective.element.nativeElement);
        var vpRect = ui ?
            ui.viewportRect : this.viewportDirective.getBoundingClientRect();
        var origin = ui ?
            ui.origin : new ImageTransformOrigin(this.imageDirective.element.nativeElement);
        this._zoomValue = ui ? ui.value : this.zoomValue;
        transform.scale = this.zoomValue;
        var boundaries = this._getVirtualBoundaries(vpRect);
        var transBoundaries = boundaries.translate;
        var oBoundaries = boundaries.origin;
        if (transform.x >= transBoundaries.maxX) {
            origin.x = oBoundaries.minX;
            transform.x = transBoundaries.maxX;
        }
        if (transform.x <= transBoundaries.minX) {
            origin.x = oBoundaries.maxX;
            transform.x = transBoundaries.minX;
        }
        if (transform.y >= transBoundaries.maxY) {
            origin.y = oBoundaries.minY;
            transform.y = transBoundaries.maxY;
        }
        if (transform.y <= transBoundaries.minY) {
            origin.y = oBoundaries.maxY;
            transform.y = transBoundaries.minY;
        }
        this.imageDirective.renderer.setElementStyle(this.imageDirective.element.nativeElement, Utils.CSS_TRANSFORM, transform.toString());
        this.imageDirective.renderer.setElementStyle(this.imageDirective.element.nativeElement, Utils.CSS_TRANS_ORG, origin.toString());
        this._updateOverlay();
        this._triggerUpdate();
    };
    CropBoundaryComponent.prototype._updateOverlay = function () {
        var boundRect = this.getBoundingClientRect();
        var imgData = this.imageDirective.getBoundingClientRect();
        this.overlayDirective.renderer.setElementStyle(this.overlayDirective.element.nativeElement, 'width', imgData.width + "px");
        this.overlayDirective.renderer.setElementStyle(this.overlayDirective.element.nativeElement, 'height', imgData.height + "px");
        this.overlayDirective.renderer.setElementStyle(this.overlayDirective.element.nativeElement, 'top', (imgData.top - boundRect.top) + "px");
        this.overlayDirective.renderer.setElementStyle(this.overlayDirective.element.nativeElement, 'left', (imgData.left - boundRect.left) + "px");
    };
    CropBoundaryComponent.prototype._triggerUpdate = function () {
        if (this._isVisible()) {
            this.options.update();
        }
    };
    CropBoundaryComponent.prototype._isVisible = function () {
        return this.imageDirective.element.nativeElement.offsetHeight > 0 &&
            this.imageDirective.element.nativeElement.offsetWidth > 0;
    };
    CropBoundaryComponent.prototype._getVirtualBoundaries = function (viewport) {
        var scale = this.zoomValue;
        var vpWidth = viewport.width;
        var vpHeight = viewport.height;
        var centerFromBoundaryX = this.width / 2;
        var centerFromBoundaryY = this.height / 2;
        var imgRect = this.imageDirective.getBoundingClientRect();
        var curImgWidth = imgRect.width;
        var curImgHeight = imgRect.height;
        var halfWidth = vpWidth / 2;
        var halfHeight = vpHeight / 2;
        var maxX = ((halfWidth / scale) - centerFromBoundaryX) * -1;
        var minX = maxX - ((curImgWidth * (1 / scale)) - (vpWidth * (1 / scale)));
        var maxY = ((halfHeight / scale) - centerFromBoundaryY) * -1;
        var minY = maxY - ((curImgHeight * (1 / scale)) - (vpHeight * (1 / scale)));
        var originMinX = (1 / scale) * halfWidth;
        var originMaxX = (curImgWidth * (1 / scale)) - originMinX;
        var originMinY = (1 / scale) * halfHeight;
        var originMaxY = (curImgHeight * (1 / scale)) - originMinY;
        return {
            translate: {
                maxX: maxX,
                minX: minX,
                maxY: maxY,
                minY: minY
            },
            origin: {
                maxX: originMaxX,
                minX: originMinX,
                maxY: originMaxY,
                minY: originMinY
            }
        };
    };
    CropBoundaryComponent.prototype._get = function () {
        var imgData = this.imageDirective.getBoundingClientRect();
        var vpData = this.viewportDirective.getBoundingClientRect();
        var x1 = vpData.left - imgData.left;
        var y1 = vpData.top - imgData.top;
        var x2 = x1 + vpData.width;
        var y2 = y1 + vpData.height;
        var scale = this.zoomValue;
        if (scale === Infinity || isNaN(scale)) {
            scale = 1;
        }
        x1 = Math.max(0, x1 / scale);
        y1 = Math.max(0, y1 / scale);
        x2 = Math.max(0, x2 / scale);
        y2 = Math.max(0, y2 / scale);
        return {
            points: [Utils.fix(x1), Utils.fix(y1), Utils.fix(x2), Utils.fix(y2)],
            zoom: scale
        };
    };
    CropBoundaryComponent.prototype.getBoundingClientRect = function () {
        return this.element.nativeElement.getBoundingClientRect();
    };
    CropBoundaryComponent.prototype.mouseDown = function (ev) {
        ev.preventDefault();
        if (this.isDragging) {
            return;
        }
        this.isDragging = true;
        this.originalX = ev.pageX;
        this.originalY = ev.pageY;
        if (ev.touches) {
            var touches = ev.touches[0];
            this.originalX = touches.pageX;
            this.originalY = touches.pageY;
        }
        this.transform = ImageTransform.parse(this.imageDirective.element.nativeElement);
        this.mouseMoveListenFuncs.push(this.renderer.listenGlobal('window', 'mousemove', this.mouseMove.bind(this)));
        this.mouseMoveListenFuncs.push(this.renderer.listenGlobal('window', 'touchmove', this.mouseMove.bind(this)));
        this.mouseUpListenFuncs.push(this.renderer.listenGlobal('window', 'mouseup', this.mouseUp.bind(this)));
        this.mouseUpListenFuncs.push(this.renderer.listenGlobal('window', 'mouseutouchend', this.mouseUp.bind(this)));
        this.imageDirective.renderer.setElementStyle(this.imageDirective.element.nativeElement, Utils.CSS_USERSELECT, 'none');
        this.vpRect = this.viewportDirective.getBoundingClientRect();
    };
    CropBoundaryComponent.prototype.mouseMove = function (ev) {
        ev.preventDefault();
        var pageX = ev.pageX;
        var pageY = ev.pageY;
        if (ev.touches) {
            var touches = ev.touches[0];
            pageX = touches.pageX;
            pageY = touches.pageY;
        }
        var deltaX = pageX - this.originalX;
        var deltaY = pageY - this.originalY;
        var imgRect = this.imageDirective.getBoundingClientRect();
        var top = this.transform.y + deltaY;
        var left = this.transform.x + deltaX;
        if (ev.type === 'touchmove') {
            if (ev.touches.length > 1) {
                var touch1 = ev.touches[0];
                var touch2 = ev.touches[1];
                var dist = Math.sqrt((touch1.pageX - touch2.pageX) * (touch1.pageX - touch2.pageX) +
                    (touch1.pageY - touch2.pageY) * (touch1.pageY - touch2.pageY));
                if (!this.originalDistance) {
                    this.originalDistance = dist / this.zoomValue;
                }
                var scale = dist / this.originalDistance;
                this.zoomChanged.emit(scale);
                return;
            }
        }
        if (this.vpRect.top > imgRect.top + deltaY && this.vpRect.bottom < imgRect.bottom + deltaY) {
            this.transform.y = top;
        }
        if (this.vpRect.left > imgRect.left + deltaX && this.vpRect.right < imgRect.right + deltaX) {
            this.transform.x = left;
        }
        this.transform.scale = this.zoomValue;
        this.imageDirective.renderer.setElementStyle(this.imageDirective.element.nativeElement, Utils.CSS_TRANSFORM, this.transform.toString());
        this._updateOverlay();
        this.originalY = pageY;
        this.originalX = pageX;
    };
    CropBoundaryComponent.prototype.mouseUp = function () {
        this.isDragging = false;
        if (this.mouseMoveListenFuncs) {
            this.mouseMoveListenFuncs.forEach(function (fn) { return fn(); });
        }
        if (this.mouseUpListenFuncs) {
            this.mouseUpListenFuncs.forEach(function (fn) { return fn(); });
        }
        this.imageDirective.renderer.setElementStyle(this.imageDirective.element.nativeElement, Utils.CSS_USERSELECT, 'initial');
        this._updateCenterPoint();
        this._triggerUpdate();
        this.originalDistance = 0;
    };
    CropBoundaryComponent.prototype._initDraggable = function () {
        this.overlayDirective.renderer.listen(this.overlayDirective.element.nativeElement, 'mousedown', this.mouseDown.bind(this));
        this.overlayDirective.renderer.listen(this.overlayDirective.element.nativeElement, 'touchstart', this.mouseDown.bind(this));
    };
    CropBoundaryComponent.prototype._updateCenterPoint = function () {
        var data = this.imageDirective.getBoundingClientRect();
        var vpData = this.viewportDirective.getBoundingClientRect();
        var transform = ImageTransform.parse(this.imageDirective.element.nativeElement.style[Utils.CSS_TRANSFORM]);
        var pc = new ImageTransformOrigin(this.imageDirective.element.nativeElement);
        var top = (vpData.top - data.top) + (vpData.height / 2);
        var left = (vpData.left - data.left) + (vpData.width / 2);
        var center = { x: 0, y: 0 };
        var adj = { x: 0, y: 0 };
        center.y = top / this.zoomValue;
        center.x = left / this.zoomValue;
        adj.y = (center.y - pc.y) * (1 - this.zoomValue);
        adj.x = (center.x - pc.x) * (1 - this.zoomValue);
        transform.x -= adj.x;
        transform.y -= adj.y;
        transform.scale = this.zoomValue;
        this.imageDirective.renderer.setElementStyle(this.imageDirective.element.nativeElement, Utils.CSS_TRANS_ORG, center.x + "px " + center.y + "px");
        this.imageDirective.renderer.setElementStyle(this.imageDirective.element.nativeElement, Utils.CSS_TRANSFORM, transform.toString());
    };
    CropBoundaryComponent.prototype._bindPoints = function (points) {
        if (points.length != 4) {
            throw "ImageCrop - Invalid number of points supplied: " + points;
        }
        var pointsWidth = points[2] - points[0];
        var vpData = this.viewportDirective.getBoundingClientRect();
        var boundRect = this.getBoundingClientRect();
        var vpOffset = {
            left: vpData.left - boundRect.left,
            top: vpData.top - boundRect.top
        };
        var scale = vpData.width / pointsWidth;
        var originTop = points[1];
        var originLeft = points[0];
        var transformTop = (-1 * points[1]) + vpOffset.top;
        var transformLeft = (-1 * points[0]) + vpOffset.left;
        this.imageDirective.renderer.setElementStyle(this.imageDirective.element.nativeElement, Utils.CSS_TRANS_ORG, originLeft + "px " + originTop + "px");
        this.imageDirective.renderer.setElementStyle(this.imageDirective.element.nativeElement, Utils.CSS_TRANSFORM, new ImageTransform(transformLeft, transformTop, scale).toString());
        this.zoomChanged.emit(scale);
    };
    CropBoundaryComponent.prototype._centerImage = function () {
        var imgDim = this.imageDirective.getBoundingClientRect();
        var vpDim = this.viewportDirective.getBoundingClientRect();
        var boundDim = this.getBoundingClientRect();
        var vpLeft = vpDim.left - boundDim.left;
        var vpTop = vpDim.top - boundDim.top;
        var w = vpLeft - ((imgDim.width - vpDim.width) / 2);
        var h = vpTop - ((imgDim.height - vpDim.height) / 2);
        var transform = new ImageTransform(w, h, this.zoomValue);
        this.imageDirective.renderer.setElementStyle(this.imageDirective.element.nativeElement, Utils.CSS_TRANSFORM, transform.toString());
    };
    CropBoundaryComponent.prototype._initializeZoom = function () {
        this.renderer.listen(this.element.nativeElement, 'mousewheel', this.scroll.bind(this));
        this.renderer.listen(this.element.nativeElement, 'DOMMouseScroll', this.scroll.bind(this));
    };
    CropBoundaryComponent.prototype.scroll = function (ev) {
        var delta;
        var targetZoom;
        if (ev.wheelDelta) {
            delta = ev.wheelDelta / 1200;
        }
        else if (ev.deltaY) {
            delta = ev.deltaY / 1060;
        }
        else if (ev.detail) {
            delta = ev.detail / -60;
        }
        else {
            delta = 0;
        }
        targetZoom = this.zoomValue + delta;
        if (targetZoom > this.maxZoom || targetZoom < this.minZoom) {
            return;
        }
        ev.preventDefault();
        this.zoomValue = targetZoom;
        this.zoomChanged.emit(targetZoom);
    };
    CropBoundaryComponent.decorators = [
        { type: Component, args: [{
                    selector: 'crop-boundary',
                    template: "<img class=\"image-crop-image\" crop-boundary-image />\n<div class=\"image-crop-viewport image-crop-vp-circle\"\n     crop-boundary-viewport\n     [width]=\"options.viewport?.width\"\n     [height]=\"options.viewport?.height\">\n</div>\n<div class=\"image-crop-overlay\" crop-boundary-overlay></div>\n<div class=\"image-crop-mask\"></div>",
                    styles: [".image-crop-viewport {\n\tbox-shadow: none;\n\tborder: none;\n}\n\n.image-crop-mask {\n\tposi\n\tposition: absolute;\n\ttop: 0;\n\tleft: 0;\n\twidth: 330px;\n\theight: 175px;\n\tbackground-image: url(data:image/svg+xml;base64,PD94bWwgdmVyc2lvbj0iMS4wIiBzdGFuZGFsb25lPSJubyI/PjwhRE9DVFlQRSBzdmcgUFVCTElDICItLy9XM0MvL0RURCBTVkcgMS4xLy9FTiIgImh0dHA6Ly93d3cudzMub3JnL0dyYXBoaWNzL1NWRy8xLjEvRFREL3N2ZzExLmR0ZCI+PHN2ZyB3aWR0aD0iMTAwJSIgaGVpZ2h0PSIxMDAlIiB2aWV3Qm94PSIwIDAgMzMwIDE3NSIgdmVyc2lvbj0iMS4xIiB4bWxucz0iaHR0cDovL3d3dy53My5vcmcvMjAwMC9zdmciIHhtbG5zOnhsaW5rPSJodHRwOi8vd3d3LnczLm9yZy8xOTk5L3hsaW5rIiB4bWw6c3BhY2U9InByZXNlcnZlIiBzdHlsZT0iZmlsbC1ydWxlOmV2ZW5vZGQ7Y2xpcC1ydWxlOmV2ZW5vZGQ7c3Ryb2tlLW1pdGVybGltaXQ6MTA7Ij48ZyBpZD0iTGF5ZXIxIj48Y2xpcFBhdGggaWQ9Il9jbGlwMSI+PHBhdGggZD0iTTAsNS4wMDJjMCwtMi43NjIgMi4yMzksLTUuMDAyIDUuMDAyLC01LjAwMmwzMTkuOTk2LDBjMi43NjIsMCA1LjAwMiwyLjI0NSA1LjAwMiw1LjAwMmwwLDE2NC45OTZjMCwyLjc2MyAtMi4yMzksNS4wMDIgLTUuMDAyLDUuMDAybC0zMTkuOTk2LDBjLTIuNzYyLDAgLTUuMDAyLC0yLjI0NSAtNS4wMDIsLTUuMDAybDAsLTE2NC45OTZaTTE2NSwxMzZjMjYuNTEsMCA0OCwtMjEuNDkgNDgsLTQ4YzAsLTI2LjUxIC0yMS40OSwtNDggLTQ4LC00OGMtMjYuNTEsMCAtNDgsMjEuNDkgLTQ4LDQ4YzAsMjYuNTEgMjEuNDksNDggNDgsNDhaIi8+PC9jbGlwUGF0aD48ZyBjbGlwLXBhdGg9InVybCgjX2NsaXAxKSI+PHBhdGggZD0iTTAsNS4wMDJjMCwtMi43NjMgMi4yMzksLTUuMDAyIDUuMDAyLC01LjAwMmwzMTkuOTk2LDBjMi43NjIsMCA1LjAwMiwyLjI0NSA1LjAwMiw1LjAwMmwwLDE2NC45OTZjMCwyLjc2MiAtMi4yMzksNS4wMDIgLTUuMDAyLDUuMDAybC0zMTkuOTk2LDBjLTIuNzYyLDAgLTUuMDAyLC0yLjI0NSAtNS4wMDIsLTUuMDAybDAsLTE2NC45OTZaTTE2NSwxMzZjMjYuNTEsMCA0OCwtMjEuNDkgNDgsLTQ4YzAsLTI2LjUxIC0yMS40OSwtNDggLTQ4LC00OGMtMjYuNTEsMCAtNDgsMjEuNDkgLTQ4LDQ4YzAsMjYuNTEgMjEuNDksNDggNDgsNDhaIiBzdHlsZT0iZmlsbDpub25lO3N0cm9rZS13aWR0aDoycHg7c3Ryb2tlLWRhc2hhcnJheTo0LDM7c3Ryb2tlOiNhZmI4YzM7Ii8+PC9nPjxjbGlwUGF0aCBpZD0iX2NsaXAyIj48cGF0aCBkPSJNMCw1LjAwMmMwLC0yLjc2MiAyLjIzOSwtNS4wMDIgNS4wMDIsLTUuMDAybDMxOS45OTYsMGMyLjc2MiwwIDUuMDAyLDIuMjQ1IDUuMDAyLDUuMDAybDAsMTY0Ljk5NmMwLDIuNzYzIC0yLjIzOSw1LjAwMiAtNS4wMDIsNS4wMDJsLTMxOS45OTYsMGMtMi43NjIsMCAtNS4wMDIsLTIuMjQ1IC01LjAwMiwtNS4wMDJsMCwtMTY0Ljk5NlpNMTY1LDEzNmMyNi41MSwwIDQ4LC0yMS40OSA0OCwtNDhjMCwtMjYuNTEgLTIxLjQ5LC00OCAtNDgsLTQ4Yy0yNi41MSwwIC00OCwyMS40OSAtNDgsNDhjMCwyNi41MSAyMS40OSw0OCA0OCw0OFoiLz48L2NsaXBQYXRoPjxnIGNsaXAtcGF0aD0idXJsKCNfY2xpcDIpIj48Zz48cGF0aCBkPSJNMCw1LjAwMmMwLC0yLjc2MiAyLjIzOSwtNS4wMDIgNS4wMDIsLTUuMDAybDMxOS45OTYsMGMyLjc2MiwwIDUuMDAyLDIuMjQ1IDUuMDAyLDUuMDAybDAsMTY0Ljk5NmMwLDIuNzYzIC0yLjIzOSw1LjAwMiAtNS4wMDIsNS4wMDJsLTMxOS45OTYsMGMtMi43NjIsMCAtNS4wMDIsLTIuMjQ1IC01LjAwMiwtNS4wMDJsMCwtMTY0Ljk5NlpNMTY1LDEzNmMyNi41MSwwIDQ4LC0yMS40OSA0OCwtNDhjMCwtMjYuNTEgLTIxLjQ5LC00OCAtNDgsLTQ4Yy0yNi41MSwwIC00OCwyMS40OSAtNDgsNDhjMCwyNi41MSAyMS40OSw0OCA0OCw0OFoiIHN0eWxlPSJmaWxsOiNjYWQyZTA7ZmlsbC1vcGFjaXR5OjAuNzAxOTYxOyIvPjxjbGlwUGF0aCBpZD0iX2NsaXAzIj48cGF0aCBkPSJNMCw1LjAwMmMwLC0yLjc2MiAyLjIzOSwtNS4wMDIgNS4wMDIsLTUuMDAybDMxOS45OTYsMGMyLjc2MiwwIDUuMDAyLDIuMjQ1IDUuMDAyLDUuMDAybDAsMTY0Ljk5NmMwLDIuNzYzIC0yLjIzOSw1LjAwMiAtNS4wMDIsNS4wMDJsLTMxOS45OTYsMGMtMi43NjIsMCAtNS4wMDIsLTIuMjQ1IC01LjAwMiwtNS4wMDJsMCwtMTY0Ljk5NlpNMTY1LDEzNmMyNi41MSwwIDQ4LC0yMS40OSA0OCwtNDhjMCwtMjYuNTEgLTIxLjQ5LC00OCAtNDgsLTQ4Yy0yNi41MSwwIC00OCwyMS40OSAtNDgsNDhjMCwyNi41MSAyMS40OSw0OCA0OCw0OFoiLz48L2NsaXBQYXRoPjxnIGNsaXAtcGF0aD0idXJsKCNfY2xpcDMpIj48cGF0aCBkPSJNMCw1LjAwMmMwLC0yLjc2MyAyLjIzOSwtNS4wMDIgNS4wMDIsLTUuMDAybDMxOS45OTYsMGMyLjc2MiwwIDUuMDAyLDIuMjQ1IDUuMDAyLDUuMDAybDAsMTY0Ljk5NmMwLDIuNzYyIC0yLjIzOSw1LjAwMiAtNS4wMDIsNS4wMDJsLTMxOS45OTYsMGMtMi43NjIsMCAtNS4wMDIsLTIuMjQ1IC01LjAwMiwtNS4wMDJsMCwtMTY0Ljk5NlpNMTY1LDEzNmMyNi41MSwwIDQ4LC0yMS40OSA0OCwtNDhjMCwtMjYuNTEgLTIxLjQ5LC00OCAtNDgsLTQ4Yy0yNi41MSwwIC00OCwyMS40OSAtNDgsNDhjMCwyNi41MSAyMS40OSw0OCA0OCw0OFoiIHN0eWxlPSJmaWxsOm5vbmU7c3Ryb2tlLXdpZHRoOjJweDtzdHJva2UtZGFzaGFycmF5OjQsMztzdHJva2U6I2FmYjhjMzsiLz48L2c+PC9nPjwvZz48Y2xpcFBhdGggaWQ9Il9jbGlwNCI+PHBhdGggZD0iTTAsNS4wMDJjMCwtMi43NjIgMi4yMzksLTUuMDAyIDUuMDAyLC01LjAwMmwzMTkuOTk2LDBjMi43NjIsMCA1LjAwMiwyLjI0NSA1LjAwMiw1LjAwMmwwLDE2NC45OTZjMCwyLjc2MyAtMi4yMzksNS4wMDIgLTUuMDAyLDUuMDAybC0zMTkuOTk2LDBjLTIuNzYyLDAgLTUuMDAyLC0yLjI0NSAtNS4wMDIsLTUuMDAybDAsLTE2NC45OTZaTTE2NSwxMzZjMjYuNTEsMCA0OCwtMjEuNDkgNDgsLTQ4YzAsLTI2LjUxIC0yMS40OSwtNDggLTQ4LC00OGMtMjYuNTEsMCAtNDgsMjEuNDkgLTQ4LDQ4YzAsMjYuNTEgMjEuNDksNDggNDgsNDhaIi8+PC9jbGlwUGF0aD48ZyBjbGlwLXBhdGg9InVybCgjX2NsaXA0KSI+PHBhdGggZD0iTTAsNS4wMDJjMCwtMi43NjMgMi4yMzksLTUuMDAyIDUuMDAyLC01LjAwMmwzMTkuOTk2LDBjMi43NjIsMCA1LjAwMiwyLjI0NSA1LjAwMiw1LjAwMmwwLDE2NC45OTZjMCwyLjc2MiAtMi4yMzksNS4wMDIgLTUuMDAyLDUuMDAybC0zMTkuOTk2LDBjLTIuNzYyLDAgLTUuMDAyLC0yLjI0NSAtNS4wMDIsLTUuMDAybDAsLTE2NC45OTZaTTE2NSwxMzZjMjYuNTEsMCA0OCwtMjEuNDkgNDgsLTQ4YzAsLTI2LjUxIC0yMS40OSwtNDggLTQ4LC00OGMtMjYuNTEsMCAtNDgsMjEuNDkgLTQ4LDQ4YzAsMjYuNTEgMjEuNDksNDggNDgsNDhaIiBzdHlsZT0iZmlsbDpub25lO3N0cm9rZS13aWR0aDoycHg7c3Ryb2tlLWRhc2hhcnJheTo0LDM7c3Ryb2tlOiNhZmI4YzM7Ii8+PC9nPjxjbGlwUGF0aCBpZD0iX2NsaXA1Ij48cGF0aCBkPSJNMCw1LjAwMmMwLC0yLjc2MiAyLjIzOSwtNS4wMDIgNS4wMDIsLTUuMDAybDMxOS45OTYsMGMyLjc2MiwwIDUuMDAyLDIuMjQ1IDUuMDAyLDUuMDAybDAsMTY0Ljk5NmMwLDIuNzYzIC0yLjIzOSw1LjAwMiAtNS4wMDIsNS4wMDJsLTMxOS45OTYsMGMtMi43NjIsMCAtNS4wMDIsLTIuMjQ1IC01LjAwMiwtNS4wMDJsMCwtMTY0Ljk5NlpNMTY1LDEzNmMyNi41MSwwIDQ4LC0yMS40OSA0OCwtNDhjMCwtMjYuNTEgLTIxLjQ5LC00OCAtNDgsLTQ4Yy0yNi41MSwwIC00OCwyMS40OSAtNDgsNDhjMCwyNi41MSAyMS40OSw0OCA0OCw0OFoiLz48L2NsaXBQYXRoPjxnIGNsaXAtcGF0aD0idXJsKCNfY2xpcDUpIj48Zz48cGF0aCBkPSJNMCw1LjAwMmMwLC0yLjc2MiAyLjIzOSwtNS4wMDIgNS4wMDIsLTUuMDAybDMxOS45OTYsMGMyLjc2MiwwIDUuMDAyLDIuMjQ1IDUuMDAyLDUuMDAybDAsMTY0Ljk5NmMwLDIuNzYzIC0yLjIzOSw1LjAwMiAtNS4wMDIsNS4wMDJsLTMxOS45OTYsMGMtMi43NjIsMCAtNS4wMDIsLTIuMjQ1IC01LjAwMiwtNS4wMDJsMCwtMTY0Ljk5NlpNMTY1LDEzNmMyNi41MSwwIDQ4LC0yMS40OSA0OCwtNDhjMCwtMjYuNTEgLTIxLjQ5LC00OCAtNDgsLTQ4Yy0yNi41MSwwIC00OCwyMS40OSAtNDgsNDhjMCwyNi41MSAyMS40OSw0OCA0OCw0OFoiIHN0eWxlPSJmaWxsOiNjYWQyZTA7ZmlsbC1vcGFjaXR5OjAuNzAxOTYxOyIvPjxjbGlwUGF0aCBpZD0iX2NsaXA2Ij48cGF0aCBkPSJNMCw1LjAwMmMwLC0yLjc2MiAyLjIzOSwtNS4wMDIgNS4wMDIsLTUuMDAybDMxOS45OTYsMGMyLjc2MiwwIDUuMDAyLDIuMjQ1IDUuMDAyLDUuMDAybDAsMTY0Ljk5NmMwLDIuNzYzIC0yLjIzOSw1LjAwMiAtNS4wMDIsNS4wMDJsLTMxOS45OTYsMGMtMi43NjIsMCAtNS4wMDIsLTIuMjQ1IC01LjAwMiwtNS4wMDJsMCwtMTY0Ljk5NlpNMTY1LDEzNmMyNi41MSwwIDQ4LC0yMS40OSA0OCwtNDhjMCwtMjYuNTEgLTIxLjQ5LC00OCAtNDgsLTQ4Yy0yNi41MSwwIC00OCwyMS40OSAtNDgsNDhjMCwyNi41MSAyMS40OSw0OCA0OCw0OFoiLz48L2NsaXBQYXRoPjxnIGNsaXAtcGF0aD0idXJsKCNfY2xpcDYpIj48cGF0aCBkPSJNMCw1LjAwMmMwLC0yLjc2MyAyLjIzOSwtNS4wMDIgNS4wMDIsLTUuMDAybDMxOS45OTYsMGMyLjc2MiwwIDUuMDAyLDIuMjQ1IDUuMDAyLDUuMDAybDAsMTY0Ljk5NmMwLDIuNzYyIC0yLjIzOSw1LjAwMiAtNS4wMDIsNS4wMDJsLTMxOS45OTYsMGMtMi43NjIsMCAtNS4wMDIsLTIuMjQ1IC01LjAwMiwtNS4wMDJsMCwtMTY0Ljk5NlpNMTY1LDEzNmMyNi41MSwwIDQ4LC0yMS40OSA0OCwtNDhjMCwtMjYuNTEgLTIxLjQ5LC00OCAtNDgsLTQ4Yy0yNi41MSwwIC00OCwyMS40OSAtNDgsNDhjMCwyNi41MSAyMS40OSw0OCA0OCw0OFoiIHN0eWxlPSJmaWxsOm5vbmU7c3Ryb2tlLXdpZHRoOjJweDtzdHJva2UtZGFzaGFycmF5OjQsMztzdHJva2U6I2FmYjhjMzsiLz48L2c+PC9nPjwvZz48Y2xpcFBhdGggaWQ9Il9jbGlwNyI+PHBhdGggZD0iTTEwNSwyOGwxMjAsMGwwLDEyMGwtMTIwLDBsMCwtMTIwWk0xNjUsMTM2YzI2LjUxLDAgNDgsLTIxLjQ5IDQ4LC00OGMwLC0yNi41MSAtMjEuNDksLTQ4IC00OCwtNDhjLTI2LjUxLDAgLTQ4LDIxLjQ5IC00OCw0OGMwLDI2LjUxIDIxLjQ5LDQ4IDQ4LDQ4WiIvPjwvY2xpcFBhdGg+PGcgY2xpcC1wYXRoPSJ1cmwoI19jbGlwNykiPjxjbGlwUGF0aCBpZD0iX2NsaXA4Ij48cGF0aCBkPSJNMTY1LDEzOGMyNy42MTQsMCA1MCwtMjIuMzg2IDUwLC01MGMwLC0yNy42MTQgLTIyLjM4NiwtNTAgLTUwLC01MGMtMjcuNjE0LDAgLTUwLDIyLjM4NiAtNTAsNTBjMCwyNy42MTQgMjIuMzg2LDUwIDUwLDUwWk0xNjUsMTM0Yy0yNS40MDUsMCAtNDYsLTIwLjU5NSAtNDYsLTQ2YzAsLTI1LjQwNSAyMC41OTUsLTQ2IDQ2LC00NmMyNS40MDUsMCA0NiwyMC41OTUgNDYsNDZjMCwyNS40MDUgLTIwLjU5NSw0NiAtNDYsNDZaIi8+PC9jbGlwUGF0aD48ZyBjbGlwLXBhdGg9InVybCgjX2NsaXA4KSI+PHJlY3QgeD0iMTE3IiB5PSI0MCIgd2lkdGg9Ijk2IiBoZWlnaHQ9Ijk2IiBzdHlsZT0iZmlsbDp1cmwoI19MaW5lYXI5KTtmaWxsLXJ1bGU6bm9uemVybzsiLz48L2c+PC9nPjxjbGlwUGF0aCBpZD0iX2NsaXAxMCI+PHBhdGggZD0iTTEwNSwyOGwxMjAsMGwwLDEyMGwtMTIwLDBsMCwtMTIwWk0xNjUsMTM2YzI2LjUxLDAgNDgsLTIxLjQ5IDQ4LC00OGMwLC0yNi41MSAtMjEuNDksLTQ4IC00OCwtNDhjLTI2LjUxLDAgLTQ4LDIxLjQ5IC00OCw0OGMwLDI2LjUxIDIxLjQ5LDQ4IDQ4LDQ4WiIvPjwvY2xpcFBhdGg+PGcgY2xpcC1wYXRoPSJ1cmwoI19jbGlwMTApIj48Y2xpcFBhdGggaWQ9Il9jbGlwMTEiPjxwYXRoIGQ9Ik0xNjUsMTM4YzI3LjYxNCwwIDUwLC0yMi4zODYgNTAsLTUwYzAsLTI3LjYxNCAtMjIuMzg2LC01MCAtNTAsLTUwYy0yNy42MTQsMCAtNTAsMjIuMzg2IC01MCw1MGMwLDI3LjYxNCAyMi4zODYsNTAgNTAsNTBaTTE2NSwxMzRjLTI1LjQwNSwwIC00NiwtMjAuNTk1IC00NiwtNDZjMCwtMjUuNDA1IDIwLjU5NSwtNDYgNDYsLTQ2YzI1LjQwNSwwIDQ2LDIwLjU5NSA0Niw0NmMwLDI1LjQwNSAtMjAuNTk1LDQ2IC00Niw0NloiLz48L2NsaXBQYXRoPjxnIGNsaXAtcGF0aD0idXJsKCNfY2xpcDExKSI+PHJlY3QgeD0iLTg4MCIgeT0iLTE1OSIgd2lkdGg9IjIxNDgiIGhlaWdodD0iNzY4IiBzdHlsZT0iZmlsbDp1cmwoI19MaW5lYXIxMik7ZmlsbC1ydWxlOm5vbnplcm87Ii8+PC9nPjwvZz48L2c+PGRlZnM+PGxpbmVhckdyYWRpZW50IGlkPSJfTGluZWFyOSIgeDE9IjAiIHkxPSIwIiB4Mj0iMSIgeTI9IjAiIGdyYWRpZW50VW5pdHM9InVzZXJTcGFjZU9uVXNlIiBncmFkaWVudFRyYW5zZm9ybT0ibWF0cml4KDEwNDQuNDgsMTAzNC4yNCwtMTAzNC4yNCwxMDQ0LjQ4LDExNyw0MCkiPjxzdG9wIG9mZnNldD0iMCUiIHN0eWxlPSJzdG9wLWNvbG9yOiMzMDIzYWU7c3RvcC1vcGFjaXR5OjAiLz48c3RvcCBvZmZzZXQ9IjEwMCUiIHN0eWxlPSJzdG9wLWNvbG9yOiNjODZkZDc7c3RvcC1vcGFjaXR5OjAiLz48L2xpbmVhckdyYWRpZW50PjxsaW5lYXJHcmFkaWVudCBpZD0iX0xpbmVhcjEyIiB4MT0iMCIgeTE9IjAiIHgyPSIxIiB5Mj0iMCIgZ3JhZGllbnRVbml0cz0idXNlclNwYWNlT25Vc2UiIGdyYWRpZW50VHJhbnNmb3JtPSJtYXRyaXgoOTcuOTIsOTYuOTYsLTk2Ljk2LDk3LjkyLDExNyw0MCkiPjxzdG9wIG9mZnNldD0iMCUiIHN0eWxlPSJzdG9wLWNvbG9yOiMzMDIzYWU7c3RvcC1vcGFjaXR5OjEiLz48c3RvcCBvZmZzZXQ9IjEwMCUiIHN0eWxlPSJzdG9wLWNvbG9yOiNjODZkZDc7c3RvcC1vcGFjaXR5OjEiLz48L2xpbmVhckdyYWRpZW50PjwvZGVmcz48L3N2Zz4=);\n\tz-index: 10;\n}"],
                    encapsulation: ViewEncapsulation.None,
                },] },
    ];
    CropBoundaryComponent.ctorParameters = [
        { type: ElementRef, },
        { type: Renderer, },
        { type: ChangeDetectorRef, },
    ];
    CropBoundaryComponent.propDecorators = {
        'imageDirective': [{ type: ViewChild, args: [CropBoundaryImageDirective,] },],
        'overlayDirective': [{ type: ViewChild, args: [CropBoundaryOverlayDirective,] },],
        'viewportDirective': [{ type: ViewChild, args: [CropBoundaryViewportDirective,] },],
        'zoomChanged': [{ type: Output },],
        'width': [{ type: HostBinding, args: ['style.width.px',] }, { type: Input },],
        'height': [{ type: HostBinding, args: ['style.height.px',] }, { type: Input },],
        'options': [{ type: Input },],
    };
    return CropBoundaryComponent;
}());
//# sourceMappingURL=crop-boundary.component.js.map