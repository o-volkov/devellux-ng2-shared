import { RequestOptionsArgs, Http, Response } from '@angular/http';
import { Observable } from 'rxjs/Observable';
import { OpaqueToken } from '@angular/core';
export declare const API_URL_TOKEN: OpaqueToken;
export declare class HttpClient {
    private http;
    private baseUrl;
    private successIntercept;
    private errorIntercept;
    constructor(http: Http, baseUrl: any);
    addSuccessIntercept(intercept: (val) => any): void;
    addErrorIntercept(intercept: (er) => Observable<any>): void;
    private prepareRequest(requestArgs, additionalOptions);
    get(url: string, options?: RequestOptionsArgs): Observable<Response>;
    post(url: string, body?: any, options?: RequestOptionsArgs): Observable<Response>;
    put(url: string, body?: any, options?: RequestOptionsArgs): Observable<Response>;
    delete(url: string, body?: any, options?: RequestOptionsArgs): Observable<Response>;
    patch(url: string, body?: any, options?: RequestOptionsArgs): Observable<Response>;
    intercept(observable: Observable<Response>): Observable<Response>;
}
