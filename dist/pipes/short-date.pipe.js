import { Pipe } from '@angular/core';
import * as moment from 'moment';
export var ShortDatePipe = (function () {
    function ShortDatePipe() {
    }
    ShortDatePipe.prototype.transform = function (date) {
        return moment(moment.utc(date).toDate()).format('D MMM, h:mm A');
    };
    ShortDatePipe.decorators = [
        { type: Pipe, args: [{ name: 'shortDate' },] },
    ];
    ShortDatePipe.ctorParameters = [];
    return ShortDatePipe;
}());
//# sourceMappingURL=short-date.pipe.js.map