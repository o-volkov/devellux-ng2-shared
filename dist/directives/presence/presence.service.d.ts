import { Observable } from 'rxjs/Observable';
import { SSEService } from '../../services/see.service';
import { HttpClient } from "../../helpers/http/client";
export declare class PresenceService {
    private sse;
    private http;
    private cache;
    private userStatusChangeEE;
    userStatusChange$: Observable<{
        id: string;
        status: boolean;
    }>;
    constructor(sse: SSEService, http: HttpClient);
    isUserOnline(userId: any): any;
    changeUserStatus(id: any, status: any): void;
    private getUserStatus(userId);
    private randomStatusChange();
}
