import { PipeTransform } from '@angular/core';
export declare class FromNowPipe implements PipeTransform {
    transform(date: any): string;
}
