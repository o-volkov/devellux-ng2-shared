import { ElementRef, Renderer } from '@angular/core';
export declare class CropBoundaryOverlayDirective {
    element: ElementRef;
    renderer: Renderer;
    constructor(element: ElementRef, renderer: Renderer);
}
