import { Subscription } from 'rxjs/Subscription';
export declare class ButtonSpinnerComponent {
    spinner: Subscription;
    requesting(): boolean;
    getSpinnerOpacity(): number;
    getContentOpacity(): number;
}
