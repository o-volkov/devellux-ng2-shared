import { ComponentFactoryResolver, Injectable, ReflectiveInjector } from '@angular/core';
import createComponent from '../framework/createComponent';
import { DialogRef } from '../models/dialog-ref';
import { ModalOverlay } from '../overlay/index';
export var DOMOverlayRenderer = (function () {
    function DOMOverlayRenderer(_cr) {
        this._cr = _cr;
    }
    DOMOverlayRenderer.prototype.render = function (dialog, vcRef) {
        var b = ReflectiveInjector.resolve([
            { provide: DialogRef, useValue: dialog }
        ]);
        var cmpRef = createComponent(this._cr, ModalOverlay, vcRef, b);
        if (dialog.inElement) {
            vcRef.element.nativeElement.appendChild(cmpRef.location.nativeElement);
        }
        else {
            document.body.appendChild(cmpRef.location.nativeElement);
        }
        return cmpRef;
    };
    DOMOverlayRenderer.decorators = [
        { type: Injectable },
    ];
    DOMOverlayRenderer.ctorParameters = [
        { type: ComponentFactoryResolver, },
    ];
    return DOMOverlayRenderer;
}());
//# sourceMappingURL=dom-modal-renderer.js.map