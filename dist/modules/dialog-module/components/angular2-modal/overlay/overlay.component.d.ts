import { ComponentRef, ElementRef, EmbeddedViewRef, ResolvedReflectiveProvider, ViewContainerRef, Renderer, TemplateRef, Type } from '@angular/core';
import { DialogRef } from '../models/dialog-ref';
import { BaseDynamicComponent } from '../components/index';
export interface EmbedComponentConfig {
    component: any;
    bindings?: ResolvedReflectiveProvider[];
    projectableNodes?: any[][];
}
export declare class ModalOverlay extends BaseDynamicComponent {
    private dialogRef;
    private vcr;
    private beforeDestroyHandlers;
    private innerVcr;
    private template;
    constructor(dialogRef: DialogRef<any>, vcr: ViewContainerRef, el: ElementRef, renderer: Renderer);
    getProjectables<T>(content: string | TemplateRef<any> | Type<any>, bindings?: ResolvedReflectiveProvider[]): any[][];
    embedComponent(config: EmbedComponentConfig): EmbeddedViewRef<EmbedComponentConfig>;
    addComponent<T>(type: any, bindings?: ResolvedReflectiveProvider[], projectableNodes?: any[][]): ComponentRef<T>;
    fullscreen(): void;
    insideElement(): void;
    setClickBoundary(element: Element): void;
    canDestroy(): Promise<void>;
    beforeDestroy(fn: () => Promise<void>): void;
    documentKeypress(event: KeyboardEvent): void;
    ngOnDestroy(): void;
}
