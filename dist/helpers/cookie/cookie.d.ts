export declare function getCookie(name: any): string;
export declare function setCookie(name: any, value: any, options: any): void;
export declare function deleteCookie(name: any): void;
export declare function moveFromUriToCookies(urlKey: string, cookieKey?: string): void;
