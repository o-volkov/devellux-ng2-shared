import { Pipe } from '@angular/core';
import * as moment from 'moment';
export var UploadDatePipe = (function () {
    function UploadDatePipe() {
    }
    UploadDatePipe.prototype.transform = function (date) {
        return moment(date).format('D MMM h:mm A');
    };
    UploadDatePipe.decorators = [
        { type: Pipe, args: [{
                    name: 'uploadDate'
                },] },
    ];
    UploadDatePipe.ctorParameters = [];
    return UploadDatePipe;
}());
//# sourceMappingURL=upload-date.pipe.js.map