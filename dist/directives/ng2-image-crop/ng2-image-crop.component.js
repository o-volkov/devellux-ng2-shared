import { Component, Input, Output, EventEmitter, ChangeDetectorRef, ViewEncapsulation, ViewChild, ElementRef, Renderer } from "@angular/core";
import { CropBoundaryComponent } from "./boundary/index";
import * as Utils from "./shared/utils";
import { ImageTransform, ImageTransformOrigin } from "./shared/index";
var styles = ".image-crop-container {\n  display: block;\n}\n.image-crop-image {\n  z-index: -1;\n  position: absolute;\n  top: 0;\n  left: 0;\n  transform-origin: 0 0;\n  max-width: none;\n}\n\n.image-crop-boundary {\n  display: block;\n  position: relative;\n  overflow: hidden;\n  margin: 0 auto;\n  z-index: 1;\n}\n\n.image-crop-viewport {\n  position: absolute;\n  border: 2px solid #fff;\n  margin: auto;\n  top: 0;\n  bottom: 0;\n  right: 0;\n  left: 0;\n  box-shadow:0 0 0 899px rgba(0, 0, 0, 0.5);\n  z-index: 0;\n}\n.image-crop-vp-circle {\n  border-radius: 50%;\n}\n.image-crop-overlay {\n  z-index: 1;\n  position: absolute;\n  cursor: move;\n}\n.image-zoom-wrap {\n  width: 100%;\n  margin: 0 auto;\n  margin-top: 5px;\n  text-align: center;\n}\n\n.image-zoom {\n  -webkit-appearance: none;/*removes default webkit styles*/\n  /*border: 1px solid white; *//*fix for FF unable to apply focus style bug */\n  width: 250px;/*required for proper track sizing in FF*/\n  max-width: 100%;\n}\n.image-zoom::-webkit-slider-runnable-track {\n  width: 100%;\n  height: 3px;\n  background: rgba(0, 0, 0, 0.5);\n  border: 0;\n  border-radius: 3px;\n}\n.image-zoom::-webkit-slider-thumb {\n  -webkit-appearance: none;\n  border: none;\n  height: 16px;\n  width: 16px;\n  border-radius: 50%;\n  background: #ddd;\n  margin-top: -6px;\n}\n.image-zoom:focus {\n  outline: none;\n}\n/*\n.image-zoom:focus::-webkit-slider-runnable-track {\n    background: #ccc;\n}\n*/\n\n.image-zoom::-moz-range-track {\n  width: 100%;\n  height: 3px;\n  background: rgba(0, 0, 0, 0.5);\n  border: 0;\n  border-radius: 3px;\n}\n.image-zoom::-moz-range-thumb {\n  border: none;\n  height: 16px;\n  width: 16px;\n  border-radius: 50%;\n  background: #ddd;\n  margin-top: -6px;\n}\n\n/*hide the outline behind the border*/\n.image-zoom:-moz-focusring{\n  outline: 1px solid white;\n  outline-offset: -1px;\n}\n\n.image-zoom::-ms-track {\n  width: 300px;\n  height: 5px;\n  background: transparent;/*remove bg colour from the track, we'll use ms-fill-lower and ms-fill-upper instead */\n  border-color: transparent;/*leave room for the larger thumb to overflow with a transparent border */\n  border-width: 6px 0;\n  color: transparent;/*remove default tick marks*/\n}\n.image-zoom::-ms-fill-lower {\n  background: rgba(0, 0, 0, 0.5);\n  border-radius: 10px;\n}\n.image-zoom::-ms-fill-upper {\n  background: rgba(0, 0, 0, 0.5);\n  border-radius: 10px;\n}\n.image-zoom::-ms-thumb {\n  border: none;\n  height: 16px;\n  width: 16px;\n  border-radius: 50%;\n  background: #ddd;\n}\n.image-zoom:focus::-ms-fill-lower {\n  background: rgba(0, 0, 0, 0.5);\n}\n.image-zoom:focus::-ms-fill-upper {\n  background: rgba(0, 0, 0, 0.5);\n}";
export var Ng2ImageCropComponent = (function () {
    function Ng2ImageCropComponent(element, renderer, changeDetectionRef) {
        var _this = this;
        this.element = element;
        this.renderer = renderer;
        this.changeDetectionRef = changeDetectionRef;
        this.data = {
            bound: false,
            points: [],
            url: '',
        };
        this.minZoom = 0;
        this.maxZoom = 1.5;
        this.boundaryWidth = 330;
        this.boundaryHeight = 175;
        this.options = {
            image: {},
            viewport: { width: 100, height: 100 },
            mouseWheelZoom: true,
            update: function () {
                _this.cropChanged.emit(_this.getResult());
            }
        };
        this.zoomValue = 1;
        this.loaded = false;
        this.cropChanged = new EventEmitter();
        renderer.setElementClass(element.nativeElement, 'image-crop-container', true);
    }
    Ng2ImageCropComponent.prototype.ngAfterViewInit = function () {
        var _this = this;
        var reader = new FileReader();
        reader.onloadend = function () {
            _this.boundaryComponent.image = reader.result;
            _this.loaded = true;
            _this.boundaryComponent.imageDirective.renderer.listen(_this.boundaryComponent.imageDirective.element.nativeElement, 'load', function () {
                _this.changeDetectionRef.detectChanges();
                setTimeout(function () {
                    var points = [];
                    _this.data.bound = false;
                    _this.data.url = reader.result;
                    _this.data.points = (points || _this.data.points).map(function (p) {
                        return parseFloat("" + p);
                    });
                    _this._updatePropertiesFromImage();
                    _this.boundaryComponent._triggerUpdate();
                }, 1);
            });
        };
        this.boundaryComponent.imageDirective.opacity = 0;
        this.changeDetectionRef.detectChanges();
        reader.readAsDataURL(this.fileItem._file);
    };
    Ng2ImageCropComponent.prototype.zoomDecrement = function () {
        if (this.zoomValue > this.minZoom) {
            this.zoomValue -= 0.01;
            this.onZoomSliderChanged();
        }
    };
    Ng2ImageCropComponent.prototype.increaseZoom = function () {
        if (this.zoomValue < this.maxZoom) {
            this.zoomValue += 0.01;
            this.onZoomSliderChanged();
        }
    };
    Ng2ImageCropComponent.prototype.onZoomSliderChanged = function () {
        if (this.loaded && this.boundaryComponent.zoomValue != this.zoomValue) {
            this.boundaryComponent.zoomValue = this.zoomValue;
        }
    };
    Ng2ImageCropComponent.prototype.onZoomImageChanged = function (scale) {
        if (this.loaded && this.boundaryComponent.zoomValue != this.zoomValue) {
            this.zoomValue = scale;
        }
    };
    Ng2ImageCropComponent.prototype._updatePropertiesFromImage = function () {
        var initialZoom = 1;
        var minZoom;
        var maxZoom = 1.5;
        var transformReset = new ImageTransform(0, 0, initialZoom);
        var originReset = new ImageTransformOrigin();
        var imgData;
        var vpData;
        var boundaryData;
        var minW;
        var minH;
        var imgDir = this.boundaryComponent.imageDirective;
        if (!this.boundaryComponent._isVisible() || this.data.bound) {
            return;
        }
        this.data.bound = true;
        imgDir.renderer.setElementStyle(imgDir.element.nativeElement, Utils.CSS_TRANSFORM, transformReset.toString());
        imgDir.renderer.setElementStyle(imgDir.element.nativeElement, Utils.CSS_TRANS_ORG, originReset.toString());
        imgDir.renderer.setElementStyle(imgDir.element.nativeElement, 'opacity', '1');
        imgData = imgDir.getBoundingClientRect();
        vpData = this.boundaryComponent.viewportDirective.getBoundingClientRect();
        boundaryData = this.boundaryComponent.getBoundingClientRect();
        minW = vpData.width / imgData.width;
        minH = vpData.height / imgData.height;
        minZoom = Math.max(minW, minH);
        if (minZoom >= maxZoom) {
            this.maxZoom = minZoom + 1;
        }
        initialZoom = Math.max((boundaryData.width / imgData.width), (boundaryData.height / imgData.height));
        this.zoomValue = transformReset.scale = initialZoom;
        if (maxZoom < initialZoom) {
            maxZoom = initialZoom;
        }
        this.minZoom = minZoom;
        this.maxZoom = maxZoom;
        this.boundaryComponent.minZoom = minZoom;
        this.boundaryComponent.maxZoom = maxZoom;
        imgDir.renderer.setElementStyle(imgDir.element.nativeElement, Utils.CSS_TRANSFORM, transformReset.toString());
        imgDir.renderer.setElementStyle(imgDir.element.nativeElement, Utils.CSS_TRANS_ORG, originReset.toString());
        imgDir.opacity = 1;
        if (this.data.points.length) {
            this.boundaryComponent._bindPoints(this.data.points);
        }
        else {
            this.boundaryComponent._centerImage();
        }
        this.boundaryComponent._zoomValue = initialZoom;
        this.boundaryComponent._triggerUpdate();
        this.onZoomImageChanged(initialZoom);
        this.boundaryComponent._updateCenterPoint();
        this.boundaryComponent._updateOverlay();
    };
    Ng2ImageCropComponent.prototype.getResult = function () {
        var data = this.boundaryComponent._get();
        var size = 'original';
        var format = 'png';
        var quality = 1;
        var vpRect = this.boundaryComponent.viewportDirective.getBoundingClientRect();
        if (size === 'viewport') {
            data['outputWidth'] = vpRect.width;
            data['outputHeight'] = vpRect.height;
        }
        data['format'] = 'image/' + format;
        data['quality'] = quality;
        data['url'] = this.data.url;
        return Ng2ImageCropComponent._getCanvasResult(this.boundaryComponent.imageDirective.element.nativeElement, data);
    };
    Ng2ImageCropComponent._getCanvasResult = function (img, data) {
        var points = data.points;
        var left = points[0];
        var top = points[1];
        var width = (points[2] - points[0]);
        var height = (points[3] - points[1]);
        var canvas = document.createElement('canvas');
        var ctx = canvas.getContext('2d');
        var outWidth = width;
        var outHeight = height;
        if (data.outputWidth && data.outputHeight) {
            outWidth = data.outputWidth;
            outHeight = data.outputHeight;
        }
        canvas.width = outWidth;
        canvas.height = outHeight;
        ctx.drawImage(img, left, top, width, height, 0, 0, outWidth, outHeight);
        ctx.fillStyle = '#fff';
        ctx.globalCompositeOperation = 'destination-in';
        ctx.beginPath();
        ctx.arc(outWidth / 2, outHeight / 2, outWidth / 2, 0, Math.PI * 2, true);
        ctx.closePath();
        ctx.fill();
        return canvas.toDataURL(data.format, data.quality);
    };
    Ng2ImageCropComponent.decorators = [
        { type: Component, args: [{
                    selector: 'ng2-image-crop',
                    template: "<div [hidden]=\"!loaded\">\n  <crop-boundary [options]=\"options\" [width]=\"boundaryWidth\" [height]=\"boundaryHeight\"\n    (zoomChanged)=\"onZoomImageChanged($event)\">\n  </crop-boundary>\n\n  <div class=\"slider\" style=\"border-top: none\">\n    <div class=\"slider-main\">\n      <div class=\"slider-main__left\">\n        <button class=\"slider__button\" (click)=\"zoomDecrement()\">\n          <i class=\"icon icon__decrement\"></i>\n        </button>\n      </div>\n      <div class=\"slider-main__center\">\n        <div class=\"slider-bar\">\n          <input class=\"image-zoom\" type=\"range\" step=\"0.01\" [attr.min]=\"minZoom\" [attr.max]=\"maxZoom\"\n            [(ngModel)]=\"zoomValue\"\n            (ngModelChange)=\"onZoomSliderChanged()\" />\n        </div>\n      </div>\n      <div class=\"slider-main__right\">\n        <button class=\"slider__button\" (click)=\"increaseZoom()\">\n          <i class=\"icon icon__increment\"></i>\n        </button>\n      </div>\n    </div>\n  </div>\n</div>",
                    styles: [styles],
                    encapsulation: ViewEncapsulation.None,
                },] },
    ];
    Ng2ImageCropComponent.ctorParameters = [
        { type: ElementRef, },
        { type: Renderer, },
        { type: ChangeDetectorRef, },
    ];
    Ng2ImageCropComponent.propDecorators = {
        'fileItem': [{ type: Input },],
        'cropChanged': [{ type: Output },],
        'boundaryComponent': [{ type: ViewChild, args: [CropBoundaryComponent,] },],
    };
    return Ng2ImageCropComponent;
}());
//# sourceMappingURL=ng2-image-crop.component.js.map