var __extends = (this && this.__extends) || function (d, b) {
    for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p];
    function __() { this.constructor = d; }
    d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
};
import { Component, HostListener, forwardRef } from "@angular/core";
import { NG_VALUE_ACCESSOR } from "@angular/forms";
import * as moment from "moment";
import { DeadlineDialogComponent } from "./deadline.dialog";
import { CustomNgInput } from "../../components/custom_inputs/custom-ng2-input";
import { DialogService } from "../../modules/dialog-module/dialog.module";
var noop = function () {
    var args = [];
    for (var _i = 0; _i < arguments.length; _i++) {
        args[_i - 0] = arguments[_i];
    }
};
var CUSTOM_INPUT_CONTROL_VALUE_ACCESSOR = { provide: NG_VALUE_ACCESSOR, useExisting: forwardRef(function () { return DatetimePickerModalComponent; }), multi: true };
export var DatetimePickerModalComponent = (function (_super) {
    __extends(DatetimePickerModalComponent, _super);
    function DatetimePickerModalComponent(modal) {
        _super.call(this);
        this.modal = modal;
        this.valueView = '';
    }
    DatetimePickerModalComponent.prototype.updateViewValue = function (v) {
        if (v) {
            this.valueView = moment(v).format('dddd, DD MMM, h:mm a');
        }
    };
    DatetimePickerModalComponent.prototype.onDeadlineClick = function () {
        var _this = this;
        this.touched();
        this.modal.open(DeadlineDialogComponent, { deadline: this.value })
            .then(function (deadline) {
            if (deadline) {
                _this.value = moment(moment.utc(deadline)).format();
            }
        })
            .catch(function (err) {
        });
    };
    DatetimePickerModalComponent.decorators = [
        { type: Component, args: [{
                    selector: 'datetimepicker-modal',
                    template: "<input type=\"text\" class=\"form-input datepicker-required\" placeholder=\"Deadline\" [(ngModel)]=\"valueView\" readonly=\"readonly\" />",
                    styles: ["\n\n\n    /*:host.ng-invalid .datepicker-required {\n      border-color: #ef4773 !important;\n    }\n    \n    :host.ng-pristine.ng-invalid .datepicker-required {\n      border-color: #cad3d1 !important;\n    }\n    \n    :host.ng-dirty.ng-invalid .datepicker-required {\n      border-color: #ef4773 !important;\n    }\n    \n    :host.ng-dirty.ng-valid .datepicker-required {\n      border-color: #04be5b !important;\n    }\n    \n    .datetimepicker-modal.ng-untouched.ng-pristine.ng-invalid .form-input.datepicker-required.ng-untouched.ng-pristine.ng-valid {\n      border-color: #ef4773 !important;\n    }*/\n    \n    \n  "],
                    providers: [CUSTOM_INPUT_CONTROL_VALUE_ACCESSOR],
                },] },
    ];
    DatetimePickerModalComponent.ctorParameters = [
        { type: DialogService, },
    ];
    DatetimePickerModalComponent.propDecorators = {
        'onDeadlineClick': [{ type: HostListener, args: ['mousedown',] },],
    };
    return DatetimePickerModalComponent;
}(CustomNgInput));
//# sourceMappingURL=datetimepicker-modal.component.js.map