export declare const CSS_PREFIXES: string[];
export declare function vendorPrefix(prop: string): string;
export declare const CSS_TRANSFORM: string;
export declare const CSS_TRANS_ORG: string;
export declare const CSS_USERSELECT: string;
export declare function fix(v: any, decimalPoints?: number): string;
