import { Pipe, PipeTransform } from '@angular/core';
import * as moment from 'moment';

@Pipe({name: 'shortDate'})

export class ShortDatePipe implements PipeTransform {
  transform(date) {
    return moment(moment.utc(date).toDate()).format('D MMM, h:mm A');
  }
}
