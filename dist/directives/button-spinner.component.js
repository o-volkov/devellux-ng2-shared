import { Component, Input } from '@angular/core';
export var ButtonSpinnerComponent = (function () {
    function ButtonSpinnerComponent() {
    }
    ButtonSpinnerComponent.prototype.requesting = function () {
        if (!this.spinner) {
            return false;
        }
        return !this.spinner.closed;
    };
    ButtonSpinnerComponent.prototype.getSpinnerOpacity = function () {
        return !this.requesting() ? 0 : 1;
    };
    ButtonSpinnerComponent.prototype.getContentOpacity = function () {
        return this.requesting() ? 0 : 1;
    };
    ButtonSpinnerComponent.decorators = [
        { type: Component, args: [{
                    selector: 'button[spinner]',
                    template: "<svg [style.opacity]=\"getSpinnerOpacity()\" x=\"0px\" y=\"0px\" viewBox=\"0 0 40 40\" enable-background=\"new 0 0 40 40\">\n  <path opacity=\"0.2\" fill=\"#000\" d=\"M20.201,5.169c-8.254,0-14.946,6.692-14.946,14.946c0,8.255,6.692,14.946,14.946,14.946\n    s14.946-6.691,14.946-14.946C35.146,11.861,28.455,5.169,20.201,5.169z M20.201,31.749c-6.425,0-11.634-5.208-11.634-11.634\n    c0-6.425,5.209-11.634,11.634-11.634c6.425,0,11.633,5.209,11.633,11.634C31.834,26.541,26.626,31.749,20.201,31.749z\"/>\n  <path fill=\"#000\" d=\"M26.013,10.047l1.654-2.866c-2.198-1.272-4.743-2.012-7.466-2.012h0v3.312h0\n    C22.32,8.481,24.301,9.057,26.013,10.047z\">\n    <animateTransform attributeType=\"xml\"\n      attributeName=\"transform\"\n      type=\"rotate\"\n      from=\"0 20 20\"\n      to=\"360 20 20\"\n      dur=\"0.5s\"\n      repeatCount=\"indefinite\"/>\n    </path>\n  </svg>\n  <div [style.opacity]=\"getContentOpacity()\">\n    <ng-content ></ng-content>\n  </div>\n",
                    styles: ["\n:host{\n  position: relative;\n}\nsvg{\n  position: absolute;\n  top: 0;\n  left: 0;\n  right: 0;\n  bottom: 0;\n  margin: auto;\n  height: 100%;\n}\n\nsvg path,\nsvg rect {\n  fill: currentColor;\n}"],
                },] },
    ];
    ButtonSpinnerComponent.ctorParameters = [];
    ButtonSpinnerComponent.propDecorators = {
        'spinner': [{ type: Input },],
    };
    return ButtonSpinnerComponent;
}());
//# sourceMappingURL=button-spinner.component.js.map