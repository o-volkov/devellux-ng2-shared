import { beforeEachProviders, it, describe, expect, inject } from '@angular/core/testing';
import { PresenceService } from './presence.service';

describe('Presence Service', () => {
  beforeEachProviders(() => [PresenceService]);

  it('should ...',
    inject([PresenceService], (service: PresenceService) => {
      expect(service).toBeTruthy();
    }));
});
