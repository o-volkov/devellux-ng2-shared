import { Pipe } from '@angular/core';
import * as moment from 'moment';
export var UtcToLocal = (function () {
    function UtcToLocal() {
    }
    UtcToLocal.prototype.transform = function (date) {
        return moment.utc(date).toDate();
    };
    UtcToLocal.decorators = [
        { type: Pipe, args: [{
                    name: 'toLocal'
                },] },
    ];
    UtcToLocal.ctorParameters = [];
    return UtcToLocal;
}());
//# sourceMappingURL=to-local.pipe.js.map