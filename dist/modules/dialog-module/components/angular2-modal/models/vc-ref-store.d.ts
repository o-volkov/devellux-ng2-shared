import { ViewContainerRef } from '@angular/core';
export declare const vcRefStore: {
    getVCRef: (key: string) => ViewContainerRef[];
    setVCRef: (key: string, vcRef: ViewContainerRef) => void;
    delVCRef: (key: string, vcRef?: ViewContainerRef) => void;
};
