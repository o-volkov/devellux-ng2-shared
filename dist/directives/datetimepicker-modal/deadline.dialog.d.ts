import { FormControl } from "@angular/forms";
import { ModalComponent, DialogRef } from "../../modules/dialog-module/components/angular2-modal/index";
export declare class DeadlineDialogComponent implements ModalComponent<any> {
    dialog: DialogRef<any>;
    context: any;
    deadline: Date;
    deadlineControl: FormControl;
    constructor(dialog: DialogRef<any>);
    readonly min: string;
    onDeadlineSelected($event: Event): void;
}
