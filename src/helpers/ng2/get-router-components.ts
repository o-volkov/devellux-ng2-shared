export function getRouterComponents(routes) {


  function walkRoute(r, cmpList = new Set()) {
    r.forEach(route => {
      if (route.component) {
        cmpList.add(route.component);
      }

      if (route.children) {
        walkRoute(route.children, cmpList);
      }
    });
    return cmpList;
  }

  return Array.from(walkRoute(routes));
}
