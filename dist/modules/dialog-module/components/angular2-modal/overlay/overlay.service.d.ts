import { ViewContainerRef } from '@angular/core';
import { OverlayRenderer, OverlayConfig } from '../models/tokens';
import { DialogRef } from '../models/dialog-ref';
import { OverlayContext } from '../models/overlay-context';
export declare class Overlay {
    private _modalRenderer;
    defaultViewContainer: ViewContainerRef;
    readonly stackLength: number;
    constructor(_modalRenderer: OverlayRenderer);
    isTopMost(dialogRef: DialogRef<any>): boolean;
    stackPosition(dialogRef: DialogRef<any>): number;
    groupStackLength(dialogRef: DialogRef<any>): number;
    open<T extends OverlayContext>(config: OverlayConfig, group?: any): DialogRef<T>[];
    private createOverlay(renderer, vcRef, config, group);
}
