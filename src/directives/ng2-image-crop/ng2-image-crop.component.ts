import {
  Component,
  Input,
  Output,
  EventEmitter,
  ChangeDetectorRef,
  AfterViewInit,
  ViewEncapsulation,
  ViewChild,
  ElementRef,
  Renderer
} from "@angular/core";
import {FileItem} from "../ng2-file-uploader/index";
import {CropBoundaryComponent} from "./boundary/index";
import * as Utils from "./shared/utils";
import {ImageTransform, ImageTransformOrigin} from "./shared/index";

const styles = `.image-crop-container {
  display: block;
}
.image-crop-image {
  z-index: -1;
  position: absolute;
  top: 0;
  left: 0;
  transform-origin: 0 0;
  max-width: none;
}

.image-crop-boundary {
  display: block;
  position: relative;
  overflow: hidden;
  margin: 0 auto;
  z-index: 1;
}

.image-crop-viewport {
  position: absolute;
  border: 2px solid #fff;
  margin: auto;
  top: 0;
  bottom: 0;
  right: 0;
  left: 0;
  box-shadow:0 0 0 899px rgba(0, 0, 0, 0.5);
  z-index: 0;
}
.image-crop-vp-circle {
  border-radius: 50%;
}
.image-crop-overlay {
  z-index: 1;
  position: absolute;
  cursor: move;
}
.image-zoom-wrap {
  width: 100%;
  margin: 0 auto;
  margin-top: 5px;
  text-align: center;
}

.image-zoom {
  -webkit-appearance: none;/*removes default webkit styles*/
  /*border: 1px solid white; *//*fix for FF unable to apply focus style bug */
  width: 250px;/*required for proper track sizing in FF*/
  max-width: 100%;
}
.image-zoom::-webkit-slider-runnable-track {
  width: 100%;
  height: 3px;
  background: rgba(0, 0, 0, 0.5);
  border: 0;
  border-radius: 3px;
}
.image-zoom::-webkit-slider-thumb {
  -webkit-appearance: none;
  border: none;
  height: 16px;
  width: 16px;
  border-radius: 50%;
  background: #ddd;
  margin-top: -6px;
}
.image-zoom:focus {
  outline: none;
}
/*
.image-zoom:focus::-webkit-slider-runnable-track {
    background: #ccc;
}
*/

.image-zoom::-moz-range-track {
  width: 100%;
  height: 3px;
  background: rgba(0, 0, 0, 0.5);
  border: 0;
  border-radius: 3px;
}
.image-zoom::-moz-range-thumb {
  border: none;
  height: 16px;
  width: 16px;
  border-radius: 50%;
  background: #ddd;
  margin-top: -6px;
}

/*hide the outline behind the border*/
.image-zoom:-moz-focusring{
  outline: 1px solid white;
  outline-offset: -1px;
}

.image-zoom::-ms-track {
  width: 300px;
  height: 5px;
  background: transparent;/*remove bg colour from the track, we'll use ms-fill-lower and ms-fill-upper instead */
  border-color: transparent;/*leave room for the larger thumb to overflow with a transparent border */
  border-width: 6px 0;
  color: transparent;/*remove default tick marks*/
}
.image-zoom::-ms-fill-lower {
  background: rgba(0, 0, 0, 0.5);
  border-radius: 10px;
}
.image-zoom::-ms-fill-upper {
  background: rgba(0, 0, 0, 0.5);
  border-radius: 10px;
}
.image-zoom::-ms-thumb {
  border: none;
  height: 16px;
  width: 16px;
  border-radius: 50%;
  background: #ddd;
}
.image-zoom:focus::-ms-fill-lower {
  background: rgba(0, 0, 0, 0.5);
}
.image-zoom:focus::-ms-fill-upper {
  background: rgba(0, 0, 0, 0.5);
}`;


@Component({
  selector: 'ng2-image-crop',
  template: `<div [hidden]="!loaded">
  <crop-boundary [options]="options" [width]="boundaryWidth" [height]="boundaryHeight"
    (zoomChanged)="onZoomImageChanged($event)">
  </crop-boundary>

  <div class="slider" style="border-top: none">
    <div class="slider-main">
      <div class="slider-main__left">
        <button class="slider__button" (click)="zoomDecrement()">
          <i class="icon icon__decrement"></i>
        </button>
      </div>
      <div class="slider-main__center">
        <div class="slider-bar">
          <input class="image-zoom" type="range" step="0.01" [attr.min]="minZoom" [attr.max]="maxZoom"
            [(ngModel)]="zoomValue"
            (ngModelChange)="onZoomSliderChanged()" />
        </div>
      </div>
      <div class="slider-main__right">
        <button class="slider__button" (click)="increaseZoom()">
          <i class="icon icon__increment"></i>
        </button>
      </div>
    </div>
  </div>
</div>`,
  styles: [styles],
  encapsulation: ViewEncapsulation.None,
})
export class Ng2ImageCropComponent implements AfterViewInit {
  public data:{
    bound?:boolean,
    points?:any,
    url?:string,
  } = {
    bound: false,
    points: [],
    url: '',
  };
  public minZoom = 0;
  public maxZoom = 1.5;

  public boundaryWidth:number = 330;
  public boundaryHeight:number = 175;

  public options:{
    image:{},
    viewport:{ width:number, height:number },
    // boundary: { width: number, height: number },
    mouseWheelZoom:boolean,
    update:Function
  } = {
    image: {},
    viewport: {width: 100, height: 100},
    // boundary: {width: 330, height: 175},
    mouseWheelZoom: true,
    update: () => {
      this.cropChanged.emit(this.getResult());
    }
  };

  private zoomValue:number = 1;
  private loaded:boolean = false;
  @Input() fileItem:FileItem;

  @Output() cropChanged:EventEmitter<string> = new EventEmitter<string>();

  @ViewChild(CropBoundaryComponent) boundaryComponent:CropBoundaryComponent;

  constructor(private element:ElementRef,
              private renderer:Renderer,
              private changeDetectionRef:ChangeDetectorRef) {
    renderer.setElementClass(element.nativeElement, 'image-crop-container', true);
  }

  ngAfterViewInit():void {
    let reader = new FileReader();
    reader.onloadend = () => {
      this.boundaryComponent.image = reader.result;
      this.loaded = true;
      this.boundaryComponent.imageDirective.renderer.listen(
        this.boundaryComponent.imageDirective.element.nativeElement, 'load', () => {
          this.changeDetectionRef.detectChanges();
          setTimeout(() => {
            let points:any[] = [];
            this.data.bound = false;
            this.data.url = reader.result;
            this.data.points = (points || this.data.points).map((p:any) => {
              return parseFloat(`${p}`);
            });

            this._updatePropertiesFromImage();
            this.boundaryComponent._triggerUpdate();
          }, 1);
        });
    };

    this.boundaryComponent.imageDirective.opacity = 0;
    this.changeDetectionRef.detectChanges();

    reader.readAsDataURL(this.fileItem._file);
  }

  zoomDecrement():void {
    if (this.zoomValue > this.minZoom) {
      this.zoomValue -= 0.01;
      this.onZoomSliderChanged();
    }
  }

  increaseZoom():void {
    if (this.zoomValue < this.maxZoom) {
      this.zoomValue += 0.01;
      this.onZoomSliderChanged();
    }
  }

  onZoomSliderChanged():void {
    if (this.loaded && this.boundaryComponent.zoomValue != this.zoomValue) {
      this.boundaryComponent.zoomValue = this.zoomValue;
    }
  }

  onZoomImageChanged(scale:number):void {
    if (this.loaded && this.boundaryComponent.zoomValue != this.zoomValue) {
      this.zoomValue = scale;
    }
  }

  _updatePropertiesFromImage() {
    let initialZoom = 1;
    let minZoom;
    let maxZoom = 1.5;
    let transformReset:ImageTransform = new ImageTransform(0, 0, initialZoom);
    let originReset:ImageTransformOrigin = new ImageTransformOrigin();
    let imgData:any;
    let vpData:any;
    let boundaryData:any;
    let minW:number;
    let minH:number;
    let imgDir = this.boundaryComponent.imageDirective;

    if (!this.boundaryComponent._isVisible() || this.data.bound) {
      return;
    }

    this.data.bound = true;

    imgDir.renderer.setElementStyle(imgDir.element.nativeElement,
      Utils.CSS_TRANSFORM, transformReset.toString());
    imgDir.renderer.setElementStyle(imgDir.element.nativeElement,
      Utils.CSS_TRANS_ORG, originReset.toString());
    imgDir.renderer.setElementStyle(imgDir.element.nativeElement,
      'opacity', '1');

    imgData = imgDir.getBoundingClientRect();
    vpData = this.boundaryComponent.viewportDirective.getBoundingClientRect();
    boundaryData = this.boundaryComponent.getBoundingClientRect();

    minW = vpData.width / imgData.width;
    minH = vpData.height / imgData.height;
    minZoom = Math.max(minW, minH);

    if (minZoom >= maxZoom) {
      this.maxZoom = minZoom + 1;
    }

    initialZoom = Math.max((boundaryData.width / imgData.width),
      (boundaryData.height / imgData.height));
    this.zoomValue = transformReset.scale = initialZoom;
    if (maxZoom < initialZoom) {
      maxZoom = initialZoom;
    }
    this.minZoom = minZoom;
    this.maxZoom = maxZoom;
    this.boundaryComponent.minZoom = minZoom;
    this.boundaryComponent.maxZoom = maxZoom;

    imgDir.renderer.setElementStyle(imgDir.element.nativeElement,
      Utils.CSS_TRANSFORM, transformReset.toString());
    imgDir.renderer.setElementStyle(imgDir.element.nativeElement,
      Utils.CSS_TRANS_ORG, originReset.toString());
    imgDir.opacity = 1;

    if (this.data.points.length) {
      this.boundaryComponent._bindPoints(this.data.points);
    } else {
      this.boundaryComponent._centerImage();
    }

    this.boundaryComponent._zoomValue = initialZoom;

    this.boundaryComponent._triggerUpdate();
    this.onZoomImageChanged(initialZoom);

    this.boundaryComponent._updateCenterPoint();
    this.boundaryComponent._updateOverlay();
  }

  getResult():string {
    let data = this.boundaryComponent._get();
    let size = 'original';
    let format = 'png';
    let quality = 1;
    let vpRect = this.boundaryComponent.viewportDirective.getBoundingClientRect();

    if (size === 'viewport') {
      data['outputWidth'] = vpRect.width;
      data['outputHeight'] = vpRect.height;
    }

    data['format'] = 'image/' + format;
    data['quality'] = quality;
    data['url'] = this.data.url;

    return Ng2ImageCropComponent._getCanvasResult(
      this.boundaryComponent.imageDirective.element.nativeElement, data);
  }

  static _getCanvasResult(img:any, data:any):string {
    let points = data.points;
    let left = points[0];
    let top = points[1];
    let width = (points[2] - points[0]);
    let height = (points[3] - points[1]);
    let canvas = document.createElement('canvas');
    let ctx = canvas.getContext('2d');
    let outWidth = width;
    let outHeight = height;

    if (data.outputWidth && data.outputHeight) {
      outWidth = data.outputWidth;
      outHeight = data.outputHeight;
    }

    canvas.width = outWidth;
    canvas.height = outHeight;

    ctx.drawImage(img, left, top, width, height, 0, 0, outWidth, outHeight);
    ctx.fillStyle = '#fff';
    ctx.globalCompositeOperation = 'destination-in';
    ctx.beginPath();
    ctx.arc(outWidth / 2, outHeight / 2, outWidth / 2, 0, Math.PI * 2, true);
    ctx.closePath();
    ctx.fill();

    return canvas.toDataURL(data.format, data.quality);
  }
}
