var __extends = (this && this.__extends) || function (d, b) {
    for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p];
    function __() { this.constructor = d; }
    d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
};
import { Headers, RequestOptions } from "@angular/http";
import { getCookie } from "../../helpers/cookie/cookie";
export var AppRequestOptions = (function (_super) {
    __extends(AppRequestOptions, _super);
    function AppRequestOptions() {
        _super.apply(this, arguments);
        this.headers = new Headers({
            'Authorization': "Bearer " + getCookie('token'),
            'Content-Type': 'application/json',
            'X-Frontend-Url': window.location.origin,
        });
        this.withCredentials = true;
        this.baseUrl = '';
    }
    AppRequestOptions.isAbsoluteUrl = function (url) {
        var absolutePattern = /^https?:\/\//i;
        return absolutePattern.test(url);
    };
    AppRequestOptions.prototype.prepareUrl = function (url) {
        url = AppRequestOptions.isAbsoluteUrl(url) ? url : this.baseUrl + '/' + url;
        return url.replace(/([^:]\/)\/+/g, '$1');
    };
    AppRequestOptions.prototype.merge = function (options) {
        var result = new AppRequestOptions(options);
        result.baseUrl = this.baseUrl;
        result.url = this.prepareUrl(result.url);
        return result;
    };
    return AppRequestOptions;
}(RequestOptions));
//# sourceMappingURL=request-options.js.map