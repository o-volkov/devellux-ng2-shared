import { PipeTransform } from '@angular/core';
export declare class WherePipe implements PipeTransform {
    transform(input: any, fn: any): any;
}
export declare function getProperty(value: Object, key: string): Array<any>;
