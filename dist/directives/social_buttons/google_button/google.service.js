import { Injectable, OpaqueToken, Inject, Optional } from '@angular/core';
export var GOOGLE_APP_ID = new OpaqueToken('GoogleAppId');
export var GoogleService = (function () {
    function GoogleService(appId) {
        var _this = this;
        this.appId = appId;
        if (appId) {
            window['googleAsyncInit'] = function () {
                gapi.load('auth2', function () {
                    _this.googleAuthInstance = gapi.auth2.init({
                        client_id: appId,
                        cookiepolicy: 'single_host_origin',
                    });
                });
            };
            (function (d, s, id) {
                var js, fjs = d.getElementsByTagName(s)[0];
                if (d.getElementById(id)) {
                    return;
                }
                js = d.createElement(s);
                js.id = id;
                js.async = true;
                js.src = 'https://apis.google.com/js/platform.js?onload=googleAsyncInit';
                fjs.parentNode.insertBefore(js, fjs);
            }(document, 'script', 'google-jssdk'));
        }
    }
    GoogleService.prototype.setGoogleToken = function (token) {
        this.googleAccessToken = token;
    };
    GoogleService.prototype.getToken = function () {
        var _this = this;
        return new Promise(function (resolve, reject) {
            if (!_this.appId) {
                reject('no GOOGLE_APP_ID');
            }
            _this.googleAuthInstance.signIn().then(function (result) {
                _this.setGoogleToken(result.getAuthResponse().id_token);
                resolve(result.getAuthResponse().id_token);
            }, function () {
                reject('login Google cancelled');
            });
        });
    };
    GoogleService.decorators = [
        { type: Injectable },
    ];
    GoogleService.ctorParameters = [
        { type: undefined, decorators: [{ type: Optional }, { type: Inject, args: [GOOGLE_APP_ID,] },] },
    ];
    return GoogleService;
}());
//# sourceMappingURL=google.service.js.map