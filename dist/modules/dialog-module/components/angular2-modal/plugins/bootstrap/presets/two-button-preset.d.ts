import { ResolvedReflectiveProvider } from '@angular/core';
import { Modal, FluentAssignMethod } from '../../../../../components/angular2-modal';
import { MessageModalPresetBuilder } from './message-modal-preset';
import { OneButtonPreset } from './one-button-preset';
export interface TwoButtonPreset extends OneButtonPreset {
    cancelBtn: string;
    cancelBtnClass: string;
}
export declare abstract class AbstractTwoButtonPresetBuilder extends MessageModalPresetBuilder<TwoButtonPreset> {
    okBtn: FluentAssignMethod<string, this>;
    okBtnClass: FluentAssignMethod<string, this>;
    cancelBtn: FluentAssignMethod<string, this>;
    cancelBtnClass: FluentAssignMethod<string, this>;
    constructor(modal: Modal, defaultValues?: TwoButtonPreset, initialSetters?: string[]);
    $$beforeOpen(config: TwoButtonPreset): ResolvedReflectiveProvider[];
}
export declare class TwoButtonPresetBuilder extends AbstractTwoButtonPresetBuilder {
    constructor(modal: Modal, defaultValues?: TwoButtonPreset);
    $$beforeOpen(config: TwoButtonPreset): ResolvedReflectiveProvider[];
}
export interface PromptPreset extends TwoButtonPreset {
    showInput: boolean;
    defaultValue: string;
    placeholder: string;
}
export declare class PromptPresetBuilder extends AbstractTwoButtonPresetBuilder {
    placeholder: FluentAssignMethod<string, this>;
    defaultValue: FluentAssignMethod<string, this>;
    constructor(modal: Modal, defaultValues?: PromptPreset);
    $$beforeOpen(config: PromptPreset): ResolvedReflectiveProvider[];
}
