import { Injectable, EventEmitter } from '@angular/core';
import { Observable } from 'rxjs/Observable';
import { SSEService } from '../../services/see.service';
import {HttpClient} from "../../helpers/http/client";
import {commonResponseHandler} from "../../helpers/common/common";

@Injectable()
export class PresenceService {
  private cache = {};
  private userStatusChangeEE:EventEmitter<{id: string, status: boolean}> = new EventEmitter<{id: string, status: boolean}>();
  public userStatusChange$ = this.userStatusChangeEE.asObservable();

  constructor(private sse: SSEService, private http: HttpClient) {
    this.sse.connect('status')
      .subscribe((res: {'online': boolean, '_id': string}) => {
        this.changeUserStatus(res._id, res.online);
      });
  }

  isUserOnline(userId) {
    return this.getUserStatus(userId);
  }


  changeUserStatus(id, status) {
    this.cache[id] = status;
    this.userStatusChangeEE.emit({
      id, status
    });
  }

  private getUserStatus(userId): any {
    if (userId in this.cache) {
      return Observable.of(this.cache[userId]);
    }

    return this.http.post('/status/subscribe', {_id: userId})
      .map<any>(commonResponseHandler)
      .map(res => res.online)
      .do(status => this.changeUserStatus(userId, status));
  }


  private randomStatusChange() {
    // Random Key
    let id = Object.keys(this.cache)[Math.floor(Math.random() * Object.keys(this.cache).length)];
    if (id) {
      let status = Math.random() >= 0.5;
      this.changeUserStatus(id, status);
    }

    // setTimeout(() => this.randomStatusChange(), 5000);
  }
}
