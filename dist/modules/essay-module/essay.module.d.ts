import { ModuleWithProviders } from "@angular/core";
export declare class DevelluxEssayModule {
    static forRoot(): ModuleWithProviders;
}
export * from "./pipes/essay-constants.pipe";
export * from "./services/essay-constants.service";
