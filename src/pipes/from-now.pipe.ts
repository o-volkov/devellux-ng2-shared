import { Pipe, PipeTransform } from '@angular/core';
import * as moment from 'moment';

@Pipe({name: 'fromNow'})
export class FromNowPipe implements PipeTransform {
  transform(date) {
    // noinspection TypeScriptValidateTypes
    return moment(moment.utc(date).toDate()).fromNow();
  }
}
