export declare class TimezoneTimePipe {
    transform(timezone: any, format?: string): any;
}
